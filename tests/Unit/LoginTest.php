<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;


class LoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPageLoad()
    {
        $response = $this->get("/login");
        $response->assertSee("Login");
    }

    public function testLogin()
    {
        $this->get("/")->assertSee("login");

        /** @var User $user */
        $user = User::query()->first();
        $response = $this->post("/login", ["email" => $user->email, "password" => "secret"]);
        $response->assertRedirect("http://localhost/home");

        $homepage = $this->get("/");
        $homepage->assertSee($user->role);
        $homepage->assertDontSee("login");
    }
}
