<?php

namespace Tests\Unit;

use App\Models\ClassYear;
use App\Models\Cls;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testMethods()
    {
        $user = ClassYear::where("cls", Cls::DAY_CARE)
            ->first()
            ->classYearSections()
            ->first()
            ->classTeacher;

        $this->assertTrue($user->isToddlerClassTeacher());
        $this->assertFalse($user->isJuniorSchoolClassTeacher());

        $user = ClassYear::where("cls", Cls::ONE)
            ->first()
            ->classYearSections()
            ->first()
            ->classTeacher;

        $this->assertFalse($user->isToddlerClassTeacher());
        $this->assertTrue($user->isJuniorSchoolClassTeacher());
    }
}
