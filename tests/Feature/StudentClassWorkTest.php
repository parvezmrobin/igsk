<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;
use App\Models\ClassWork;

class StudentClassWorkTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testClassWork()
    {
        $classWork = ClassWork::first();
        $user = $classWork->subject->classYear->students()->first()->user;
        $this->actingAs($user);

        $response = $this->json('GET', "/student/class-work/$classWork->id?date=$classWork->date");
        $response->assertStatus(200);
        $response->assertJson(["class_work" => ["id" => $classWork->id]]);

        // $this->json("GET", "/student/class-work")->dump();
    }
}
