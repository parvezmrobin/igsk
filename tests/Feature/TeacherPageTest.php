<?php

namespace Tests\Feature;

use App\Models\Cls;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TeacherPageTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTileLoad()
    {
        $user = User::where('role', User::TEACHER)
            ->whereHas("classYearSections.classYear", function ($query) {
                $query->whereIn("cls", [Cls::DAY_CARE, Cls::PRE_PLAY]);
            })->firstOrFail();

        $response = $this->actingAs($user)->get("teacher");
        $response->assertSee("Toddler");
        $response->assertDontSee("Class Work");

        $user = User::where('role', User::TEACHER)
            ->whereDoesntHave("classYearSections.classYear", function ($query) {
                $query->whereIn("cls", [Cls::DAY_CARE, Cls::PRE_PLAY]);
            })->firstOrFail();

        $response = $this->actingAs($user)->get("teacher");
        $response->assertSee("Class Work");
        $response->assertDontSee("Monthly height, weight, cognitive and motor skill");
    }
}
