<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\HomeWork;
use App\Models\User;

class StudentHomeWorkTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHomeWork()
    {
        $homeWork = HomeWork::first();
        $user = $homeWork->section->classYear->students()->first()->user;
        $this->actingAs($user);

        $uri = "/student/home-work/$homeWork->class_year_section_id?date=$homeWork->date";
        $response = $this->json('GET', $uri);
        $response->assertStatus(200);
        $response->assertJson(["home_work" => ["id" => $homeWork->id]]);

//        $this->json("GET", "/student/home-work")->dump();
    }
}
