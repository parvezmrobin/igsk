<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home')->name('home');

Auth::routes();
Route::get("/register/teacher", 'Auth\RegisterController@showTeacherRegistrationFrom');
Route::get("/register/supervisor", 'Auth\RegisterController@showSupervisorRegistrationFrom');
Route::get("/register/admin", 'Auth\RegisterController@showAdminRegistrationFrom');

Route::get('/home', 'HomeController@home')->name('home');
Route::get("/profile","HomeController@profile");
Route::get("/news","PostController@news")->name("news");
Route::get("/notice","PostController@notice")->name("notice");
Route::get("/event","PostController@event")->name("event");
Route::get("/post/{post}","PostController@show")->name("post");
Route::get("/file/{post}","PostController@file")->name("post-file");
Route::get("/contact-us",'ContactController@contact')->name("contact-us");
Route::get("/how-to-apply","AdmissionController@apply")->name("how-to-apply");
Route::get("/age-requirement","AdmissionController@age")->name("age-requirement");

Route::get("teacher", 'UserPagesController@showTeacherPage')->name("Teacher");
Route::get("supervisor", 'UserPagesController@showSupervisorPage')->name("Supervisor");
Route::get("admin", 'UserPagesController@showAdminPage')->name("Admin");
Route::get("student", 'UserPagesController@showStudentPage')->name("Student");
Route::get("teachers", 'HomeController@teacher');
Route::get("user", "UserPagesController@showUserProfile");
Route::post("password/change", "Auth\ChangePasswordController");

/**
 * Toddler routes
 */
Route::get("/teacher/toddler", 'Teacher\ToddlerController@loadPage');
Route::get("/teacher/toddler/report/{student}", 'Teacher\ToddlerController@getReport');
Route::post("/teacher/toddler/report/{student}", 'Teacher\ToddlerController@createReport');
Route::delete("/teacher/toddler/report/{toddler}", 'Teacher\ToddlerController@destroyReport');

/**
 * ClassWork routes
 */
Route::get('/teacher/class-work/download/{classWork}', 'Teacher\ClassWorkController@fileDownload');
Route::get("/teacher/class-work", 'Teacher\ClassWorkController@loadClassWorkPage');
Route::get("/teacher/class-work/{subject}/{date}", 'Teacher\ClassWorkController@getClassWork');
Route::post("/teacher/class-work/{subject}", 'Teacher\ClassWorkController@createClassWork');
Route::post("/teacher/class-work/update/{classWork}", 'Teacher\ClassWorkController@updateClassWork');
Route::delete("/teacher/class-work/{classWork}", 'Teacher\ClassWorkController@destroyClassWork');

/**
 * HomeWork routes
 */
Route::get('/teacher/home-work/download/{homeWork}', 'Teacher\HomeWorkController@fileDownload');
Route::get("/teacher/home-work", 'Teacher\HomeWorkController@loadHomeWorkPage');
Route::get("/teacher/home-work/{section}/{date}", 'Teacher\HomeWorkController@getHomeWork');
Route::post("/teacher/home-work/{section}", 'Teacher\HomeWorkController@createHomeWork');
Route::post("/teacher/home-work/update/{homeWork}", 'Teacher\HomeWorkController@updateHomeWork');
Route::delete("/teacher/home-work/{homeWork}", 'Teacher\HomeWorkController@destroyHomeWork');

/**
 * Assignment routes
 */
Route::get('/teacher/assignment/download/{assignment}', 'Teacher\AssignmentController@fileDownload');
Route::get("/teacher/assignment", 'Teacher\AssignmentController@loadAssignment');
Route::get('/teacher/assignment/{section}/{week}', 'Teacher\AssignmentController@getAssignment');
Route::post('/teacher/assignment/{section}', 'Teacher\AssignmentController@createAssignment');
Route::post('/teacher/assignment/update/{assignment}', 'Teacher\AssignmentController@updateAssignment');
Route::delete('/teacher/{assignment}', 'Teacher\AssignmentController@destroyAssignment');

/**
 * Morality routes
 */
Route::get("/teacher/moral", 'Teacher\MoralityController@loadMorality');
Route::get("/teacher/moral/{student}/{month}", 'Teacher\MoralityController@getMorality');
Route::post("/teacher/moral/{student}", 'Teacher\MoralityController@createMorality');
Route::put("/teacher/moral/{morality}", 'Teacher\MoralityController@updatedMorality');
Route::delete("/teacher/{morality}", 'Teacher\MoralityController@destroyMorality');

/**
 * Assessment routes
 */
Route::get("/teacher/assessment", 'Teacher\AssessmentController@loadAssessment');
Route::get("/teacher/assessment/{subject}", 'Teacher\AssessmentController@getAssessment');
Route::post("/teacher/assessment/{subject}", 'Teacher\AssessmentController@createAssessment');
Route::put("/teacher/assessment/{assessment}", 'Teacher\AssessmentController@updateAssessment');
Route::delete("/teacher/{assessment}", 'Teacher\AssessmentController@destroyAssessment');


/**
 * Student ClassWork routes
 */
Route::get("/student/class-work/download/{classWork}", 'Student\ClassWorkController@fileDownload');
Route::get("/student/class-work", 'Student\ClassWorkController@loadPage');
Route::get("/student/class-work/{subject}", 'Student\ClassWorkController@getClassWork');
/**
 * Student HomeWork routes
 */
Route::get('/student/home-work/download/{homeWork}', 'Student\HomeWorkController@downloadHomeWork');
Route::get("/student/home-work", 'Student\HomeWorkController@loadPage');
Route::get("/student/home-work/{section}", 'Student\HomeWorkController@getHomeWork');

/**
 * Student Assignment routes
 */
Route::get('/student/assignment/download/{assignment}', 'Student\AssignmentController@fileDownload');
Route::get("/student/assignment", 'Student\AssignmentController@loadPage');
Route::get("/student/assignment/{section}", 'Student\AssignmentController@getAssignment');

/**
 * Student Assessment routes
 */
Route::get("/student/assessment", 'Student\AssessmentController@loadPage');
Route::get("/student/assessment/{subject}", 'Student\AssessmentController@getAssessment');

/**
 * Student Morality routes
 */
Route::get("/student/morality", 'Student\MoralityController@loadPage');
Route::get("/student/morality/{month}", 'Student\MoralityController@getMorality');

/**
 * Admin Student History
 */
Route::get("/admin/history/{section}", 'Admin\StudentHistoryController@getStudents');
Route::get("/admin/history", 'Admin\StudentHistoryController@loadPage');

Route::post("/admin/post/delete/{post}", 'PostController@destroy');
Route::get("/admin/posts", 'PostController@index');
Route::get("/admin/post/create", 'PostController@create');
Route::post("/admin/post/create", 'PostController@store');
Route::get("/admin/post/edit/{post}", "PostController@edit");
Route::post("/admin/post/edit/{post}", "PostController@update");

