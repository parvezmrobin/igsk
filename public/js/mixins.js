/**
 * Created by Parvez on 11/27/2017.
 */
const classYearSectionMixin = {
    data: {
        classYearSections: _classYearSections,
        year: undefined,
        class_years: [],
        cls: undefined,
        class_year_sections: [],
        section: undefined
    },
    watch: {
        year: function () {
            const i = _.findIndex(this.classYearSections, ['year', this.year]);
            if (i === -1) {
                this.class_years = [];
            }
            else {
                this.class_years = this.classYearSections[i].class_years;
                if (this.class_years.length) {
                    this.cls = this.class_years[0].id;
                }
            }
        },
        cls: function () {
            const i = _.findIndex(this.class_years, ['id', this.cls]);
            if (i === -1) {
                this.class_year_sections = [];
            }
            else {
                this.class_year_sections = this.class_years[i].class_year_sections;
                if (this.class_year_sections.length) {
                    this.section = this.class_year_sections[0].id;
                }
            }
        },
        section: function () {
            this.loadFromServer();
        }
    },
    methods: {
        loadFromServer: function () {
            console.error("Method unimplemented");
        }
    }
};

const commonMixin = {
    data: {
        quarters: _quarters,
        quarter: undefined,
        already_has: null,
        holidays: _holidays,
        warnings: [],
        errors: []
    },
    methods: {
        isUnavailable: function (error) {
            return error.response.status == 404 || error.response.status == 401;
        },
        isCSRFError: function (error) {
            return error.response.status == 500;
        },
        isOkay: function (response) {
            return response.status == 200;
        },btnProvideClicked: function () {
            console.log("btnProvideClicked");

            if (this.quarter === undefined) {
                this.errors = [
                    "Quarter is not defined"
                ];
                $modalError.modal("show");
                return;
            }
            this.warnings = [];
            if (this.already_has) {
                this.warnings.push("Are you sure to update?");
            }

            const date = new Date(this.date);
            if (_.indexOf(this.holidays, date.getDay()) !== -1) {
                this.warnings.push("Are you sure to provide home work on holiday?");
            }

            if (this.warnings.length) {
                $modalWarning.modal("show");
            } else {
                this.btnProceedClicked();
            }
        },
        btnProceedClicked: function () {
            console.error("Method unimplemented");
        }
    }
};

const classYearSubjectMixin = {
    data: {
        classYearSections: _classYearSections,
        year: undefined,
        class_years: [],
        cls: undefined,
        class_year_subjects: [],
        subject: undefined
    },
    watch: {
        year: function () {
            const i = _.findIndex(this.classYearSections, ['year', this.year]);
            if (i === -1) {
                this.class_years = [];
            }
            else {
                this.class_years = this.classYearSections[i].class_years;
                if (this.class_years.length) {
                    this.cls = this.class_years[0].id;
                }
            }
        },
        cls: function () {
            const i = _.findIndex(this.class_years, ['id', this.cls]);
            if (i === -1) {
                this.class_year_subjects = [];
            }
            else {
                this.class_year_subjects = this.class_years[i].class_year_subjects;
                if (this.class_year_subjects.length) {
                    this.subject = this.class_year_subjects[0].id;
                }
            }
        },
        subject: function () {
            this.loadFromServer();
        }
    },
    methods: {
        loadFromServer: function () {
            console.error("Method unimplemented");
        }
    }
};

