@extends('layouts.app')


@section('header-content')
    @include("includes.snail")
@endsection


@section("content")
   <div class="container">
       <h1 class="text-center">
           <i class="fa fa-address-book"></i>
           Contact Us
       </h1>
       @include("includes.templates.breadcrumb",[
           "links" => [
               "Home" => url("home"),
               "Contact Us" => null,
           ]
       ])

       <h2 class="c">map</h2>

       <div id="map" style="width:100%;height:400px">

       </div>
       <br><br>
       <h2>Contact Us</h2>
       <p>
           2/kha Mujgunni Mahasarak<br>
           Boyra, Khulana - 9000.<br>
           Contact No:01730585311, 01730585312<br>
           E-mail: <a href="mailto:mailbox@khulnaigs.com">mailbox@khulnaigs.com</a>
       </p>
       <br><br>


   </div>
   <div style="position: relative;">
       <div class="wave2"></div>
   </div>
@endsection

@section("script")
    <script>
        function myMap() {
            var mapOptions = {
                center: new google.maps.LatLng(22.826971, 89.538774),
                zoom: 15,
                // mapTypeId: google.maps.MapTypeId.HYBRID
            }
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            var marker = new google.maps.Marker({position: mapOptions.center});

            marker.setMap(map);
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=myMap&key=AIzaSyBezq20FEQ48YlxQQZltgWntzJBVPY7G-Q"></script>
@endsection