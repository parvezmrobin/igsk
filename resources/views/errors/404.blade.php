@extends("layouts.app")

@section("style")
    <style>
        .page-404{
            display: none;
        }
        .page-404 .txt1, .page-404 .txt2{
            color: darkred;
        }
    </style>
@endsection

@section("header-content")

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-404">
                    <p class="txt1">404</p>
                    <p class="txt2">PAGE NOT FOUND</p>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("content")
    <div style="position: relative;">
        <div class="wave2"></div>
    </div>
@endsection

@section("script")
    <script>
        $(document).ready(function () {
            $('.page-404').slideDown('slow');
        })
    </script>
@endsection