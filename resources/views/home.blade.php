@extends('layouts.app')

@section("style")
    <style>
        .team {
            padding-top: 20px;
        }
    </style>
@endsection

@section('slider')
    <div id="slider_wrapper">
        <div id="slider">
            <div id="flexslider">
                <ul class="slides clearfix">
                    <li>
                        <img src="{{asset("images/slide01.jpg")}}" alt="">
                        <div class="flex-caption">
                            <div class="flex-caption_inner container">
                                <div class="txt1">Cherish VALUES and CREATIVITY, <br> believe in EXCELLENCE.</div>
                                {{--<div class="txt2">--}}
                                {{----}}
                                {{--</div>--}}
                                {{--<div class="txt3"><a href="contact-us" class="btn-default btn0">contact us today</a>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="{{asset("images/slide02.jpg")}}" alt="">
                        <div class="flex-caption">
                            <div class="flex-caption_inner container">
                                <div class="txt1">Cherish VALUES and CREATIVITY, <br> believe in EXCELLENCE.</div>
                                {{--<div class="txt2">--}}
                                {{--</div>--}}
                                {{--<div class="txt3"><a href="contact-us" class="btn-default btn0">contact us today</a>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="{{asset("images/slide03.jpg")}}" alt="">
                        <div class="flex-caption">
                            <div class="flex-caption_inner container">
                                <div class="txt1">Cherish VALUES and CREATIVITY, <br> believe in EXCELLENCE.</div>
                                {{--<div class="txt2">--}}
                                {{----}}
                                {{--</div>--}}
                                {{--<div class="txt3"><a href="contact-us" class="btn-default btn0">contact us today</a>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>

    @include('includes.snail')
@endsection

@section('header-content')

    <div class="slogan1 animated" data-animation="fadeIn" data-animation-delay="400">we care about your child</div>

    <div class="slogan2 animated" data-animation="fadeIn"
         data-animation-delay="500">to cherish their visions and unleash their dreams as these are the
        blueprints of your children’s ultimate achievements.
    </div>

    <div class="container" style="padding-top: 50px;" id="team">

        <div class="row">
            <div class="team col-md-3 col-sm-6">
                <div class="team_inner">
                    <a href="{{url("news")}}">
                        <figure>
                            <img src="{{asset("images/banner1.jpg")}}" alt=""
                                 class="img-responsive"><em></em>
                        </figure>
                        <div class="caption">
                            <div class="txt1">News</div>
                            <div class="txt2"></div>
                            <div class="txt3">more</div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="team col-md-3 col-sm-6">
                <div class="team_inner">
                    <a href="{{url("notice")}}">
                        <figure>
                            <img src="{{asset("images/banner2.jpg")}}" alt=""
                                 class="img-responsive"><em></em>
                        </figure>
                        <div class="caption">
                            <div class="txt1">Notices</div>
                            <div class="txt2"></div>
                            <div class="txt3">more</div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="team col-md-3 col-sm-6">
                <div class="team_inner">
                    <a href="{{url("event")}}">
                        <figure>
                            <img src="{{asset("images/banner3.jpg")}}" alt=""
                                 class="img-responsive"><em></em>
                        </figure>
                        <div class="caption">
                            <div class="txt1">Events</div>
                            <div class="txt2"></div>
                            <div class="txt3">more</div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="team col-md-3 col-sm-6">
                <div class="team_inner">
                    <a href="#">
                        <figure>
                            <img src="{{asset("images/banner4.jpg")}}" alt=""
                                 class="img-responsive"><em></em>
                        </figure>
                        <div class="caption">
                            <div class="txt1">Activities</div>
                            <div class="txt2"></div>
                            <div class="txt3">more</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('content')
    <div class="splash_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-push-6 animated" data-animation="fadeInRight" data-animation-delay="0">

                    <div class="thumb1">
                        <div class="thumbnail clearfix">
                            <figure class="" style="display: none">
                                <img src="{{asset("images/logo0.png")}}" alt="" class="img-responsive">
                            </figure>
                        </div>
                    </div>

                </div>
                <div class="col-sm-6 col-sm-pull-6 animated" data-animation="fadeInLeft" data-animation-delay="300">

                    <h1>Innovative methods of teaching</h1>

                    <p>
                        <strong class="text-uppercase">
                            Introducing the first bag less school in Khulna city, where children will enjoy every moment
                            of learning. With the fantastic teachers, they will be curious explorers and extra-ordinary
                            thinkers.
                        </strong>
                    </p>

                    <a href="{{url("contact-us")}}" class="btn-default btn1">contact us today</a>


                </div>
            </div>
        </div>
    </div>

    <div id="parallax1" class="parallax">
        <div class="wave2"></div>
        <div class="bg1 parallax-bg"></div>
        <div class="parallax-content">
            <div class="container">
                <div class="txt1 animated" data-animation="zoomIn" data-animation-delay="0">grow with us</div>
                <div class="txt2 animated" data-animation="zoomIn" data-animation-delay="200">join us today</div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(window).load(function () {
            // flexslider
            $('#flexslider').flexslider({
                animation: "fade",
                slideshow: true,
                slideshowSpeed: 7000,
                animationDuration: 600,
                pauseOnAction: true,
                prevText: "",
                nextText: "",
                controlNav: true,
                directionNav: true
            });


        });
    </script>
@endsection