@extends('layouts.app')


@section('header-content')
    @include("includes.snail")
@endsection

@section("content")
    <div class="container">
        <h1 class="text-center">
            <i class="fa fa-school"></i>
            Profile
        </h1>
        @include("includes.templates.breadcrumb",[
            "links" => [
                "Home" => url("home"),
                "Profile" => null,
            ]
        ])
        <div class="breadcrumbs1_wrapper in-page">
            <div class="container">
                <div class="breadcrumbs1 text-center">
                    @foreach([
                        "Motto" => "#motto",
                        "Vision" => "#vision",
                        "Mission" => "#mission",
                        "Goal" => "#goal",
                        "Objectives" => "#objectives",
                        "Modus Operandi" => "#modus"] as $text => $link)

                            <a href="{{$link}}">{{$text}}</a>
                        @if(!$loop->last)
                             |
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

            <p>
                International Grammar School, Khulna (IGSK) is the name of a long-
                relished dream to harmonize the necessity of an English Medium school
                of international standard in Khulna City and the urgency to provide for
                the unheeded emotional intelligence of our precious children. IGSK
                promises to introduce the students to a stress-free and encouraging
                learning environment while allowing them to think out of the box
                without feeling any hindrances for not going with the flow. Here, every
                student is is given special attention by our team of specially trained

                and groomed teachers to find out their individual specialty, their very
                own interests and the distinct aptitude of them. IGSK aims to develop
                moral values and social responsibilities within each and every
                student through interactive sessions. This would not only make the
                young jewels compatible with the outside world with sufficient
                preparation for a life of usefulness and constructive enterprise but
                also enable them to make substantial contribution to the society while
                living with dignity regardless of the vicissitudes that life throws at
                them. The very first “Bag-less” school in Khulna City, besides not
                letting the students to feel the load of education, wouldn’t require any
                house tutors for academic achievements as well. Students of IGSK
                learn through their spontaneous playing, communicating and everything
                they do in here in whichever way they find interesting.
            </p>
            <h2 id="motto">Motto</h2>
            <ul>
                <li>Cherish values</li>
                <li>Practice creativity</li>
                <li>Believe in excellence</li>
            </ul>
            <br>
            <h2 id="vision">Vision</h2>
            <p>International Grammar School, Khulna (IGSK) envisions to
                facilitate children to develop their capability and enthusiasm for learning in a child centred enabling
                environment.</p>
            <br>
            <h2 id="mission">Mission</h2>
            <p>To develop young people with active and creative minds and a sense of understanding of their role in
                society. We strive for total development of each child: intellectual, social and emotional.</p>
            <p>Children will learn to take responsibility and cooperate others to complete their own actions. They will
                learn to perform with a sense of community while maintaining individuality and creative self-
                expression. Moreover, they will be treated with kindness and respect, and will learn to respect others.
                The school environments will be led children to become active participants in their own learning
                process.</p>
            <br>
            <h2 id="goal">Goal</h2>
                An inspiring school that-
            <ul>
                <li>Provides strong foundation of moral and academic excellence</li>
                <li>Value -laden, committed to practice creativity</li>
                <li>Curriculum relevant and responsive to the changing of times</li>
                <li>Ensures safe learning environment</li>
                <li>Supportive parents, teachers and other members working together for the continuous
                    improvement of school quality performance.</li>
            </ul>
            <br>
            <h2 id="objectives">Objectives</h2>
            <ul>
                <li>Secure and maintain consistently high outcomes in English, Mathematics, Science</li>
                <li>Establish processes to embed the development of growth of mindsets</li>
                <li>Improve the level of participation in extracurricular activities</li>
                <li>Improve the accuracy of teacher child grade profiling to ensure effective intervention at the
                    level of each child</li>
                <li>Improve the quality and consistency of marking and feedback for the child</li>
                <li>Strive for innovative teaching and learning ensuring range of diverse delivery methods</li>
                <li>Strive for innovative teaching and learning ensuring range of diverse delivery methods</li>
                <li>Modify the behavior of the minority of students that create levels of disruption to the learning
                    of others.</li>
            </ul>
            <br>
            <h2 id="modus">Modus Operandi</h2>
            <p>Our premise is that every child is brilliant and they are hungry to learn. This school is committed to
                nurture the natural creativity, curiosity, determination, ambition and sociability of the children. We
                emphasize formal learning, critical thinking, mind broadening, personal enrichment and enticing
                children to enjoy all activities. In the future, the most resilient individuals will be curious, creative and
                world-minded. They will be mobile and flexible. Children naturally possess these qualities and our
                school embraces them and supports the children to enhance them. When they are treated respectfully
                as individuals, asked their own views and presented with fun learning opportunities through doing,
                moving, acting, and interacting, their learning process will be accelerated and they will prosper.</p>
            <br><br>
        </article>

    </div>
    <div style="position: relative;">
        <div class="wave2"></div>
    </div>

@endsection