@extends("layouts.app")

@section("page-title", ucfirst(Auth::user()->role))

@section("style")
    <style>
        .team {
            padding: 10px 0;
        }
    </style>
@endsection

@section("header-content")
    <div class="container">

        @include("includes.templates.breadcrumb", ['links' => ["Home" => route("home"), Auth::user()->role => null]])

        <div class="row">
            <!--Toddlers-->
            @if($toddler)
                <div class="col-md-4 col-lg-3 col-sm-5 col-xs-10 col-xs-offset-1 col-md-offset-0">
                    <div class="team">
                        <div class="team_inner">
                            <a href="{{url("teacher/toddler")}}">
                                <figure><img src="{{asset("images/banner1.jpg")}}" alt=""
                                             class="img-responsive"><em></em></figure>
                                <div class="caption">
                                    <div class="txt1">Toddler</div>
                                    <div class="txt2">Monthly height, weight, cognitive and motor skill report of toddlers.</div>
                                    <div class="txt3">Visit</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            @endif

            @if($juniorSchool)
                <div class="col-md-4 col-lg-3 col-sm-5 col-xs-10 col-xs-offset-1 col-md-offset-0">
                    <div class="team">
                        <div class="team_inner">
                            <a href="{{url("teacher/class-work")}}">
                                <figure><img src="{{asset("images/banner4.jpg")}}" alt=""
                                             class="img-responsive"><em></em></figure>
                                <div class="caption">
                                    <div class="txt1">Class Work</div>
                                    <div class="txt2">Provide today's class work for the junior school students. </div>
                                    <div class="txt3">Provide</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 col-sm-5 col-xs-10 col-xs-offset-1 col-md-offset-0">
                    <div class="team">
                        <div class="team_inner">
                            <a href="{{url("teacher/home-work")}}">
                                <figure><img src="{{asset("images/banner2.jpg")}}" alt=""
                                             class="img-responsive"><em></em></figure>
                                <div class="caption">
                                    <div class="txt1">Home Work</div>
                                    <div class="txt2">Provide this week's home work for the junior school students. </div>
                                    <div class="txt3">Provide</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 col-sm-5 col-xs-10 col-xs-offset-1 col-md-offset-0">
                    <div class="team">
                        <div class="team_inner">
                            <a href="{{url("/teacher/assessment")}}">
                                <figure><img src="{{asset("images/banner3.jpg")}}" alt=""
                                             class="img-responsive"><em></em></figure>
                                <div class="caption">
                                    <div class="txt1">Assessment</div>
                                    <div class="txt2">Assess this week's performance of the junior school students. </div>
                                    <div class="txt3">Assess</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 col-sm-5 col-xs-10 col-xs-offset-1 col-md-offset-0">
                    <div class="team">
                        <div class="team_inner">
                            <a href="{{url("teacher/assignment")}}">
                                <figure><img src="{{asset("images/banner2.jpg")}}" alt=""
                                             class="img-responsive"><em></em></figure>
                                <div class="caption">
                                    <div class="txt1">Assignment</div>
                                    <div class="txt2">Assign this week's assignment for the junior school students. </div>
                                    <div class="txt3">Assign</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-lg-3 col-sm-5 col-xs-10 col-xs-offset-1 col-md-offset-0">
                    <div class="team">
                        <div class="team_inner">
                            <a href="{{url("teacher/moral")}}">
                                <figure><img src="{{asset("images/banner4.jpg")}}" alt=""
                                             class="img-responsive"><em></em></figure>
                                <div class="caption">
                                    <div class="txt1">Morality</div>
                                    <div class="txt2">
                                        Assess this month's morality score of the junior school students.
                                    </div>
                                    <div class="txt3">Assess</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            @endif

            @if($supervisor)
                <div class="col-md-4 col-lg-3 col-sm-5 col-xs-10 col-xs-offset-1 col-md-offset-0">
                    <div class="team">
                        <div class="team_inner">
                            <a href="{{url("admin/posts")}}">
                                <figure><img src="{{asset("images/banner3.jpg")}}" alt=""
                                             class="img-responsive"><em></em></figure>
                                <div class="caption">
                                    <div class="txt1">Post</div>
                                    <div class="txt2">
                                        BREAD(Browse, read, edit, add or delete). news, notices and events.
                                    </div>
                                    <div class="txt3">View</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            @endif

            @if($supervisor)

                <div class="col-md-4 col-lg-3 col-sm-5 col-xs-10 col-xs-offset-1 col-md-offset-0">
                    <div class="team">
                        <div class="team_inner">
                            <a href="{{url("register")}}">
                                <figure><img src="{{asset("images/banner1.jpg")}}" alt=""
                                             class="img-responsive"><em></em></figure>
                                <div class="caption">
                                    <div class="txt1">Registration</div>
                                    <div class="txt2">
                                        Register a new account for student,
                                        teacher{{($admin? ", supervisor or admin" : '')}}.
                                    </div>
                                    <div class="txt3">Register</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-lg-3 col-sm-5 col-xs-10 col-xs-offset-1 col-md-offset-0">
                    <div class="team">
                        <div class="team_inner">
                            <a href="{{url("admin/history")}}">
                                <figure><img src="{{asset("images/banner1.jpg")}}" alt=""
                                             class="img-responsive"><em></em></figure>
                                <div class="caption">
                                    <div class="txt1">History</div>
                                    <div class="txt2">
                                        Look back at the students and teachers of previous years.
                                    </div>
                                    <div class="txt3">View</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
@endsection

@section("content")
    <div style="position: relative;">
        <div class="wave2">

        </div>
    </div>
@endsection

@section("script")
    <script>
        $(document).ready(function () {
            $('.nav6').addClass('active')
        })
    </script>
@endsection