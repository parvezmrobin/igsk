@extends("layouts.app")

@section("page-title", "Class Work")

@section("content")
    <div class="container" id="vm" v-cloak xmlns:v-on="http://www.w3.org/1999/xhtml">

        @php
            $links = [
                "Home" => route("home"),
                ($user = Auth::user())->role => route($user->role),
                "Class Work" => null
            ]
        @endphp
        @include("includes.templates.breadcrumb", ["links" => $links])

        {{--Browsing Section--}}
        <div class="form-horizontal row">

            <div class="col-md-6 ">
                @include("includes.templates.browsing", ["type" => "subject", "toShow" => "subject.subject"])
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="date" class="control-label col-md-4">Date</label>
                    <div class="col-md-8">
                        <input type="date" name="date" id="date" v-model="date"
                               class="form-control">
                    </div>
                </div>
            </div>

        </div>

        <!--File Section-->
        <div class="form-horizontal row">
            <div class="col-md-6" v-show="class_work.class_work.file && class_work.class_work.file.length">
                <div class="form-group">
                    <label for="file-download" class="control-label col-md-4">Download File</label>
                    <div class="col-md-8">
                        <a :href="'{{url("teacher/class-work/download/")}}/' + class_work.id"
                           id="file-download" class="form-control">@{{ class_work.class_work.file }}</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="file-upload" class="control-label col-md-4">Upload File</label>
                    <div class="col-md-8">
                        <input type="file" name="file" id="file-upload" class="form-control">
                    </div>
                </div>
            </div>
        </div>

        <!--Text Section-->
        <div class="row form-horizontal">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="class-work" class="control-label col-md-2">Class Work</label>
                    <div class="col-md-10">
                        <textarea name="class-work" id="class-work" class="form-control" rows="3"
                                  v-model="class_work.class_work.text"></textarea>
                    </div>
                </div>
            </div>
        </div>

        <!--Button-->
        <div class="row form-horizontal">
            <div class="col-xs-12 form-group">
                <div class="col-md-10 col-md-offset-2">
                    <button type="button"
                            class="btn"
                            :class="{'btn-primary': already_has, 'btn-success': !already_has, disabled: (already_has === null)}"
                            v-on:click="btnProvideClicked">
                        @{{ already_has? 'Update' : 'Provide' }}
                    </button>
                </div>
            </div>
        </div>
        @include("includes.alerts.success")
        @include("includes.modals.warning")
        @include("includes.modals.error")
    </div>

    <div style="position: relative;">
        <br>
        <br>
        <div class="wave2"></div>
    </div>

@endsection

@section("script")
    <script src="{{asset("js/vue.js")}}"></script>
    <script src="{{asset("js/axios.js")}}"></script>
    <script src="{{asset("js/lodash.js")}}"></script>

    {{--Variables For Mixins--}}
    <!--suppress JSUnusedLocalSymbols -->
    <script>
        const _classYearSections = {!! json_encode($classYearSubjects) !!};
        const _quarters = {!! json_encode($quarters) !!};
        const _holidays = {!! json_encode(config("app.holidays")) !!};
    </script>

    <script src="{{asset("js/mixins.js")}}"></script>

    <script>
        let $inputs, $modalWarning, $modalError, $fileInput, $alertSuccess;
        $(document).ready(function () {
            $inputs = $("#vm").find("select, input, textarea");
            $modalWarning = $('#modal-warning');
            $modalError = $('#modal-error');
            $fileInput = $('#file-upload');
            $alertSuccess = $('#alert-success');
        });

        const vm = new Vue({
            el: "#vm",
            mixins: [classYearSubjectMixin, commonMixin],
            data: {
                class_work: {
                    class_work: {}
                },
                date: '{{date("Y-m-d")}}',
                success: ''
            },
            watch: {
                date: function () {
                    this.loadFromServer();
                }
            },
            methods: {
                /**
                 * Converts a number to ordinal form
                 */
                ordinal: function (num) {
                    num = Number.parseInt(num)
                    const dividend = num % 10;
                    if (dividend === 1 && num !== 11) {
                        return num + "st";
                    }
                    if (dividend === 2 && num !== 12) {
                        return num + "nd";
                    }
                    if (dividend === 3 && num !== 13) {
                        return num + "rd";
                    }
                    return num + "th";
                },
                /**
                 * Loads a resource from server
                 */
                loadFromServer: function () {
                    /** Before everything remove the file from $fileInput */
                    $fileInput.val("");

                    /** If either subject or date is absent, data can't be fetched */
                    if ((this.subject === undefined) || (this.date === ""))
                        return;

                    $inputs.attr("disabled", true);
                    axios.get('{{url("/teacher/class-work/")}}/' + this.subject + "/" + this.date)
                        .then(response => {
                            this.class_work = response.data.class_work;
                            this.already_has = true;
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                        .catch(error => {
                            if (error.response.status == 404) {
                                this.class_work = {
                                    class_work: {}
                                };
                                this.already_has = false;
                            }
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                },
                /**
                 * Updates server-data
                 */
                btnProceedClicked: function () {
                    $inputs.attr("disabled", true);

                    /**
                     * If the resource already exists, update it
                     */
                    if (this.already_has) {
                        const file = $fileInput[0].files[0];
                        const text = this.class_work.class_work.text;
                        const formData = new FormData();

                        // If file is uploaded
                        if (typeof file !== 'undefined') {
                            formData.append('file', file);
                        }
                        // If text is present
                        if (typeof text !== 'undefined') {
                            formData.append('text', text);
                        }

                        //Request to update ClassWork
                        axios.post("{{url("teacher/class-work/update")}}/" + this.class_work.id, formData)
                            .then(response => {
                                if (response.status == 200) {
                                    //Show alert
                                    this.success = "Successfully Updated.";
                                    $alertSuccess.slideDown()
                                        .delay(2000)
                                        .slideUp();

                                    // Update filename in download anchor
                                    if (typeof file !== 'undefined') {
                                        this.class_work.class_work.file = file.name;
                                    }
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            })
                            .catch(error => {
                                // In case of CSRF error
                                if (error.response.status == 500) {
                                    this.errors = [
                                        "Please reload the page and try again"
                                    ];
                                    $modalError.modal("show");
                                }
                            });
                    }
                    /**
                     * If the resource doesn't exist, create it
                     */
                    else {
                        const file = $fileInput[0].files[0];
                        const text = this.class_work.class_work.text;
                        const formData = new FormData();
                        formData.append('date', this.date);
                        formData.append('quarter', this.quarter);

                        // If file uploaded
                        if (typeof file !== 'undefined') {
                            formData.append('file', file);
                        }
                        // If text is present
                        if (typeof text !== 'undefined') {
                            formData.append('text', text);
                        }

                        // Request to create ClassWork
                        axios.post('{{url("teacher/class-work")}}/' + this.subject, formData)
                            .then(response => {
                                if (this.isOkay(response)) {
                                    this.success = "Successfully Created.";
                                    $alertSuccess.slideDown()
                                        .delay(2000)
                                        .slideUp();

                                    if (typeof file !== 'undefined') {
                                        this.class_work.class_work.file = file.name;
                                    }
                                    this.already_has = true;
                                    this.class_work.id = response.data.id;
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            })
                            .catch(error => {
                                if (this.isCSRFError(error)) {
                                    this.errors = [
                                        "Please reload the page and try again"
                                    ];
                                    $modalError.modal("show");
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            });
                    }
                }
            }
        })
    </script>
@endsection