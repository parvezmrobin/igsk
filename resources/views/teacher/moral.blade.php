@extends("layouts.app")

@section("page-title", "Morality")

@section("content")
    <div class="container" id="vm" v-cloak xmlns:v-on="http://www.w3.org/1999/xhtml">

        @php
            $links = [
                "Home" => route("home"),
                ($user = Auth::user())->role => route($user->role),
                "Moral" => null
            ]
        @endphp
        @include("includes.templates.breadcrumb", ["links" => $links])

        {{--Browsing Section--}}
        <div class="row">
            <div class="col-md-6 form-horizontal">
                @include("includes.templates.browsing", ["type" => "section", "toShow" => "section"])

                <div class="form-group">
                    <label for="student" class="control-label col-md-4">Student</label>
                    <div class="col-md-8">
                        <select name="student" id="student" v-model="student" class="form-control">
                            <option v-for="student in students"
                                    :value="student.id">@{{ student.user.name }}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="month" class="control-label col-md-4">Month</label>
                    <div class="col-md-8">
                        <input type="number" name="month" id="month" v-model="month" class="form-control">
                    </div>
                </div>

            </div>


            <!--Output Box-->
            <div class="col-md-6 form-horizontal">
                <br>
                <div class="form-group">
                    <label for="attentive" class="control-label col-sm-4">Attentive</label>
                    <div class="col-sm-8">
                        <rating v-model="morality.morality.attentive"
                                :show-rating="false"
                                :item-size="30"></rating>
                    </div>
                </div>
                <div class="form-group">
                    <label for="polite" class="control-label col-sm-4">Polite</label>
                    <div class="col-sm-8">
                        <rating v-model="morality.morality.polite" :show-rating="false"
                                :item-size="30"></rating>
                    </div>
                </div>
                <div class="form-group">
                    <label for="friendly" class="control-label col-sm-4">Friendly</label>
                    <div class="col-sm-8">
                        <rating v-model="morality.morality.friendly"
                                :show-rating="false"
                                :item-size="30"></rating>
                    </div>
                </div>
                <div class="form-group">
                    <label for="helpful" class="control-label col-sm-4">Helpful</label>
                    <div class="col-sm-8">
                        <rating v-model="morality.morality.helpful" :show-rating="false"
                                :item-size="30"></rating>
                    </div>
                </div>

                <!--Button-->
                <div class="row form-horizontal">
                    <div class="col-xs-12 form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="button"
                                    class="btn"
                                    :class="{'btn-primary': already_has, 'btn-success': !already_has, disabled: (already_has === null)}"
                                    v-on:click="btnProvideClicked">
                                @{{ already_has? 'Update' : 'Provide' }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include("includes.alerts.success")
        @include("includes.modals.warning")
        @include("includes.modals.error")
    </div>

    <div style="position: relative;">
        <br>
        <br>
        <div class="wave2"></div>
    </div>
@endsection

@section("script")
    <script src="{{asset("js/vue.js")}}"></script>
    <script src="{{asset("js/axios.js")}}"></script>
    <script src="{{asset("js/lodash.js")}}"></script>

    {{--Variables For Mixins--}}
    <!--suppress JSUnusedLocalSymbols -->
    <script>
        const _classYearSections = {!! json_encode($classYearSections) !!};
        const _quarters = {!! json_encode($quarters) !!};
        const _holidays = [];
    </script>

    <script src="{{asset("js/mixins.js")}}"></script>
    <script src="{{asset("js/rate-it.js")}}"></script>

    <script>
        let $inputs, $modalWarning, $modalError, $alertSuccess;
        $(document).ready(function () {
            $inputs = $("#vm").find('select, input, textarea');
            $modalWarning = $('#modal-warning');
            $modalError = $('#modal-error');
            $alertSuccess = $('#alert-success');
        });

        const vm = new Vue({
            el: '#vm',
            mixins: [classYearSectionMixin, commonMixin],
            data: {
                students: [],
                student: undefined,
                morality: {
                    id: undefined,
                    morality: {}
                },
                month: undefined,
                success: ''
            },
            watch: {
                section: function () {
                    this.students = _.find(
                        this.class_year_sections,
                        ['id', this.section]
                    ).students;
                },
                student: function () {
                    this.loadFromServer();
                },
                month: _.debounce(function () {
                    this.loadFromServer();
                }, 500, {trailing: true})
            },
            methods: {
                /** Loads a resource from server */
                loadFromServer: function () {
                    /** If either student or month is absent, data can't be fetched */
                    if (this.student === undefined ||
                        this.month === undefined ||
                        this.month === "" ||
                        this.month < 1) {
                        return;
                    }

                    $inputs.attr("disabled", true);
                    axios.get('{{url("/teacher/moral")}}/' + this.student + "/" + this.month)
                        .then(response => {
                            this.morality = response.data.morality;
                            this.already_has = true;
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                        .catch(error => {
                            if (this.isUnavailable(error)) {
                                this.morality = {
                                    id: undefined,
                                    morality: {}
                                };
                                this.already_has = false;
                            }
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                },
                btnProvideClicked: function () {
                    if (this.quarter === undefined) {
                        this.errors = [
                            "Quarter is not defined"
                        ];
                        $modalError.modal("show");
                        return;
                    }
                    this.warnings = [];
                    if (this.already_has) {
                        this.warnings.push("Are you sure to update morality?");
                    }

                    if (this.warnings.length) {
                        $modalWarning.modal("show");
                    } else {
                        this.btnProceedClicked();
                    }
                },
                btnProceedClicked: function () {
                    $inputs.attr("disabled", true);

                    /** If the resource already exists, update it */
                    if (this.already_has) {
                        axios.put("{{url("teacher/moral")}}/" + this.morality.id,
                            {morality: this.morality.morality})
                            .then(response => {
                                if (this.isOkay(response)) {
                                    //Show alert
                                    this.success = "Successfully Updated.";
                                    $alertSuccess.slideDown()
                                        .delay(2000)
                                        .slideUp();
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            })
                            .catch(error => {
                                // In case of CSRF error
                                if (this.isCSRFError(error)) {
                                    this.errors = [
                                        "Please reload the page and try again"
                                    ];
                                    $modalError.modal("show");
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            });
                    }
                    /** If the resource doesn't exist, create it */
                    else {
                        axios.post('{{url("teacher/moral")}}/' + this.student,
                            {
                                month: this.month,
                                quarter: this.quarter,
                                morality: this.morality.morality
                            })
                            .then(response => {
                                if (this.isOkay(response)) {
                                    //Show alert
                                    this.success = "Successfully Created.";
                                    $alertSuccess.slideDown()
                                        .delay(2000)
                                        .slideUp();

                                    // Update data
                                    this.morality.id = response.data.id;
                                    this.already_has = true;
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            })
                            .catch(() => {
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            })
                    }
                }
            }
        })
    </script>
@endsection