@extends("layouts.app")

@section("page-title", "Assessment")

@section("content")
    <div class="container" id="vm" v-cloak xmlns:v-on="http://www.w3.org/1999/xhtml">

        @php
            $links = [
                "Home" => route("home"),
                ($user = Auth::user())->role => route($user->role),
                "Assessment" => null
            ]
        @endphp
        @include("includes.templates.breadcrumb", ["links" => $links])

        {{--Browsing Section--}}
        <div class="form-horizontal panel panel-default panel-body">
            <div class="col-md-6 ">
                @include("includes.templates.browsing", ["type" => "section", "toShow" => "section"])
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="date" class="control-label col-md-4">Date</label>
                    <div class="col-md-8">
                        <input type="date" name="date" id="date" v-model="date"
                               class="form-control">
                    </div>
                </div>
            </div>

        </div>
        <div class="form-horizontal  panel panel-default panel-body">
            <div class="col-md-6">
                <!--Student-->
                <div class="form-group">
                    <label for="student" class="control-label col-md-4">Student</label>
                    <div class="col-md-8">
                        <select name="student" id="student" v-model="student" class="form-control">
                            <option v-for="student in students"
                                    :value="student.id">@{{ student.user.name }}
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <!--Subject-->
                <div class="form-group">
                    <label for="subject" class="control-label col-md-4">Subject</label>
                    <div class="col-md-8">
                        <select name="subject_teacher" id="subject_teacher" v-model="subject"
                                class="form-control">
                            <option v-for="subject_teacher in subject_teachers"
                                    :value="subject_teacher.class_year_subject.id">
                                {{--SubjectTeacher.ClassYearSubject.Subject.subject — SubjectTeacher.User.name--}}
                                @{{ subject_teacher.class_year_subject.subject.subject + ' — ' +
                                subject_teacher.teacher.name}}
                            </option>
                        </select>
                    </div>
                </div>
            </div>
        </div>


        <!--Output Box-->
        <div class="form-horizontal panel panel-default panel-body">
            <div class="col-xs-12 form-group">
                <label for="rating" class="control-label col-sm-2">Rating</label>
                <div class="col-sm-10">
                    <rating v-model="assessment.rating"
                            :show-rating="false"
                            :item-size="30"></rating>
                </div>
            </div>


            <div class="col-xs-12 form-group">
                <label for="assessment" class="control-label col-md-2">Assessment</label>
                <div class="col-md-10">
                    <textarea name="assessment" id="assessment" class="form-control" rows="3"
                              v-model="assessment.comment"></textarea>
                </div>
            </div>

        </div>
        <!--Button-->
        <div class="row form-horizontal">
            <div class="col-xs-12 form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="button"
                            class="btn"
                            :class="{'btn-primary': already_has, 'btn-success': !already_has, disabled: (already_has === null)}"
                            v-on:click="btnProvideClicked">
                        @{{ already_has? 'Update' : 'Provide' }}
                    </button>
                </div>
            </div>
        </div>

        @include("includes.alerts.success")
        @include("includes.modals.warning")
        @include("includes.modals.error")
    </div>

    <div style="position: relative;">
        <br>
        <br>
        <div class="wave2"></div>
    </div>
@endsection

@section("script")
    <script src="{{asset("js/vue.js")}}"></script>
    <script src="{{asset("js/axios.js")}}"></script>
    <script src="{{asset("js/lodash.js")}}"></script>

    {{--Variables For Mixins--}}
    <!--suppress JSUnusedLocalSymbols -->
    <script>
        const _classYearSections = {!! json_encode($classYearSections) !!};
        const _quarters = {!! json_encode($quarters) !!};
        const _holidays = {!! json_encode(config("app.holidays")) !!};
    </script>

    <script src="{{asset("js/mixins.js")}}"></script>
    <script src="{{asset("js/rate-it.js")}}"></script>

    <script>
        let $inputs, $modalWarning, $modalError, $alertSuccess;
        $(document).ready(function () {
            $inputs = $("#vm").find('select, input, textarea');
            $modalWarning = $('#modal-warning');
            $modalError = $('#modal-error');
            $alertSuccess = $('#alert-success');
        });

        const vm = new Vue({
            el: '#vm',
            mixins: [classYearSectionMixin, commonMixin],
            data: {
                students: [],
                student: undefined,
                subject_teachers: [],
                subject: undefined,
                date: '',
                assessment: {},
                success: ''
            },
            watch: {
                section: function () {
                    const section = _.find(
                        this.class_year_sections,
                        ['id', this.section]
                    );
                    this.students = section.students;

                    this.subject_teachers = section.subject_teachers;
                },
                student: function () {
                    this.loadFromServer();
                },
                date: _.debounce(function () {
                    this.loadFromServer();
                }, 1000, {trailing: true}),
                subject: function () {
                    this.loadFromServer();
                }
            },
            methods: {
                /** Loads a resource from server */

                loadFromServer: function () {
                    /** If either student or date or subject is absent, data can't be fetched */
                    if (this.student === undefined ||
                        this.date === undefined ||
                        this.date === "" ||
                        this.subject === undefined) {
                        return;
                    }

                    $inputs.attr("disabled", true);
                    axios.get('{{url("/teacher/assessment")}}/' + this.subject +
                        "?student=" + this.student + "&date=" + this.date)
                        .then(response => {
                            this.assessment = response.data.assessment;
                            this.already_has = true;
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                        .catch(error => {
                            if (this.isUnavailable(error)) {
                                this.assessment = {
                                    id: undefined,
                                    comment: undefined,
                                    rating: undefined
                                };
                                this.already_has = false;
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            }
                        })
                },
                btnProvideClicked: function () {
                    this.errors = [];
                    if (this.quarter === undefined) {
                        this.errors.push("Quarter is not defined");
                    }
                    if (this.assessment.comment === undefined || this.assessment.comment === '') {
                        this.errors.push("Comment is not defined");
                    }
                    if (this.assessment.rating === undefined) {
                        this.errors.push("Rating is not defined");
                    }
                    if (this.errors.length) {
                        $modalError.modal("show");
                        return;
                    }

                    this.warnings = [];
                    if (this.already_has) {
                        this.warnings.push("Are you sure to update assessment?");
                    }

                    if (this.warnings.length) {
                        $modalWarning.modal("show");
                    } else {
                        this.btnProceedClicked();
                    }
                }, btnProceedClicked: function () {
                    $inputs.attr("disabled", true);

                    /** If the resource already exists, update it */
                    if (this.already_has) {
                        axios.put("{{url("teacher/assessment")}}/" + this.assessment.id,
                            {
                                comment: this.assessment.comment,
                                rating: this.assessment.rating
                            })
                            .then(response => {
                                if (this.isOkay(response)) {
                                    //Show alert
                                    this.success = "Successfully Updated.";
                                    $alertSuccess.slideDown()
                                        .delay(2000)
                                        .slideUp();
                                }

                                //Enable inputs
                                $inputs.attr("disabled", false);
                            })
                            .catch(error => {
                                // In case of CSRF error
                                if (this.isCSRFError(error)) {
                                    this.errors = [
                                        "Please reload the page and try again"
                                    ];
                                    $modalError.modal("show");
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            });
                    }
                    /** If the resource doesn't exist, create it */
                    else {
                        axios.post('{{url("teacher/assessment")}}/' + this.subject,
                            {
                                date: this.date,
                                quarter: this.quarter,
                                comment: this.assessment.comment,
                                rating: this.assessment.rating,
                                student: this.student
                            })
                            .then(response => {
                                if (this.isOkay(response)) {
                                    //Show alert
                                    this.success = "Successfully Created.";
                                    $alertSuccess.slideDown()
                                        .delay(2000)
                                        .slideUp();

                                    // Update data
                                    this.assessment.id = response.data.id;
                                    this.already_has = true;
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            })
                            .catch(() => {
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            })
                    }
                }
            }
        })
    </script>
@endsection