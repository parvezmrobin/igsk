@extends("layouts.app")

@section("page-title", "Home Work")

@section("content")
    <div class="container" id="vm" v-cloak xmlns:v-on="http://www.w3.org/1999/xhtml">

        @php
            $links = [
                "Home" => route("home"),
                ($user = Auth::user())->role => route($user->role),
                "Home Work" => null
            ]
        @endphp
        @include("includes.templates.breadcrumb", ["links" => $links])

        {{--Browsing Section--}}
        <div class="row form-horizontal">
            <div class="col-md-6">
                @include("includes.templates.browsing", ["type" => "section", "toShow" => "section"])
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="date" class="control-label col-md-4">Date</label>
                    <div class="col-md-8">
                        <input type="date" name="date" id="date" v-model="date"
                               class="form-control">
                    </div>
                </div>
            </div>
        </div>

        <!--File Section-->
        <div class="form-horizontal row">
            <!--File Download-->
            <div class="col-md-6" v-show="home_work.home_work.file && home_work.home_work.file.length">
                <div class="form-group">
                    <label for="file-download" class="control-label col-md-4">Download File</label>
                    <div class="col-md-8">
                        <a :href="'{{url("teacher/home-work/download/")}}/' + home_work.id"
                           id="file-download" class="form-control">@{{ home_work.home_work.file }}</a>
                    </div>
                </div>
            </div>

            <!--File Upload-->
            <div class="col-md-6">
                <div class="form-group">
                    <label for="file-upload" class="control-label col-md-4">Upload File</label>
                    <div class="col-md-8">
                        <input type="file" name="file" id="file-upload" class="form-control">
                    </div>
                </div>
            </div>
        </div>

        <!--Text Section-->
        <div class="row form-horizontal">
            <div class="col-xs-12 form-group">
                <label for="home-work" class="control-label col-md-2">Home Work</label>
                <div class="col-md-10">
                    <textarea name="home-work" id="home-work" class="form-control" rows="3"
                              v-model="home_work.home_work.text"></textarea>
                </div>
            </div>
        </div>

        <!--Button-->
        <div class="row form-horizontal">
            <div class="col-xs-12 form-group">
                <div class="col-md-10 col-md-offset-2">
                    <button type="button"
                            class="btn"
                            :class="{'btn-primary': already_has, 'btn-success': !already_has, disabled: (already_has === null)}"
                            v-on:click="btnProvideClicked">
                        @{{ already_has? 'Update' : 'Provide' }}
                    </button>
                </div>
            </div>
        </div>
        @include("includes.alerts.success")
        @include("includes.modals.warning")
        @include("includes.modals.error")
    </div>
    <div style="position: relative;">
        <br>
        <br>
        <div class="wave2"></div>
    </div>

@endsection

@section("script")
    <script src="{{asset("js/vue.js")}}"></script>
    <script src="{{asset("js/axios.js")}}"></script>
    <script src="{{asset("js/lodash.js")}}"></script>

    {{--Variables For Mixins--}}
    <!--suppress JSUnusedLocalSymbols -->
    <script>
        const _classYearSections = {!! json_encode($classYearSections) !!};
        const _quarters = {!! json_encode($quarters) !!};
        const _holidays = {!! json_encode(config("app.holidays")) !!};
    </script>

    <script src="{{asset("js/mixins.js")}}"></script>

    <script>
        let $inputs, $modalWarning, $modalError, $fileInput, $alertSuccess;
        $(document).ready(function () {
            $inputs = $("#vm").find("select, input, textarea");
            $modalWarning = $('#modal-warning');
            $modalError = $('#modal-error');
            $fileInput = $('#file-upload');
            $alertSuccess = $('#alert-success');
        });

        const vm = new Vue({
            el: "#vm",
            mixins: [classYearSectionMixin, commonMixin],
            data: {
                home_work: {
                    home_work: {}
                },
                date: '{{date("Y-m-d")}}',
                success: ''
            },
            watch: {
                date: function () {
                    this.loadFromServer();
                }
            },
            methods: {
                /** Loads a resource from server */
                loadFromServer: function () {
                    /** Before everything remove the file from $fileInput */
                    $fileInput.val("");

                    /** If either section or date is absent, data can't be fetched */
                    if (this.section === undefined || this.date === "")
                        return;

                    $inputs.attr("disabled", true);
                    axios.get('{{url("/teacher/home-work/")}}/' + this.section + "/" + this.date)
                        .then(response => {
                            this.home_work = response.data.home_work;
                            this.already_has = true;
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                        .catch(error => {
                            console.log(error);
                            if (this.isUnavailable(error)) {
                                this.home_work = {
                                    home_work: {}
                                };
                                this.already_has = false;
                            }
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                },
                btnProceedClicked: function () {
                    $inputs.attr("disabled", true);

                    /** If the resource already exists, update it */
                    if (this.already_has) {
                        const file = $fileInput[0].files[0];
                        const text = this.home_work.home_work.text;
                        const formData = new FormData();

                        // If file is uploaded
                        if (typeof file !== 'undefined') {
                            formData.append('file', file);
                        }
                        // If text is present
                        if (typeof text !== 'undefined') {
                            formData.append('text', text);
                        }

                        axios.post("{{url("teacher/home-work/update")}}/" + this.home_work.id, formData)
                            .then(response => {
                                if (this.isOkay(response)) {
                                    //Show alert
                                    this.success = "Successfully Updated.";
                                    $alertSuccess.slideDown()
                                        .delay(2000)
                                        .slideUp();

                                    // Update filename in download anchor
                                    if (typeof file !== 'undefined') {
                                        this.home_work.home_work.file = file.name;
                                    }
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            })
                            .catch(error => {
                                // In case of CSRF error
                                if (this.isCSRFError(error)) {
                                    this.errors = [
                                        "Please reload the page and try again"
                                    ];
                                    $modalError.modal("show");
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            });
                    }
                    /** If the resource doesn't exist, create it */
                    else {
                        const file = $fileInput[0].files[0];
                        const text = this.home_work.home_work.text;
                        const formData = new FormData();
                        formData.append('date', this.date);
                        formData.append('quarter', this.quarter);

                        // If file uploaded
                        if (typeof file !== 'undefined') {
                            formData.append('file', file);
                        }
                        // If text is present
                        if (typeof text !== 'undefined') {
                            formData.append('text', text);
                        }

                        // Request to create HomeWork
                        axios.post('{{url("teacher/home-work")}}/' + this.section, formData)
                            .then(response => {
                                if (this.isOkay(response)) {
                                    //Show alert
                                    this.success = "Successfully Created.";
                                    $alertSuccess.slideDown()
                                        .delay(2000)
                                        .slideUp();

                                    // Update filename in download anchor
                                    if (typeof file !== 'undefined') {
                                        this.home_work.home_work.file = file.name;
                                    }

                                    // Update data
                                    this.home_work.id = response.data.id;
                                    this.already_has = true;
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            })
                            .catch(error => {
                                if (this.isCSRFError(error)) {
                                    this.errors = [
                                        "Please reload the page and try again"
                                    ];
                                    $modalError.modal("show");
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            });
                    }
                }
            }
        })
    </script>
@endsection