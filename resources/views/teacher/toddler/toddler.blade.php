@extends("layouts.app")

@section("page-title", "Toddler")

@section('style')
    <style>
        .btn-remove {
            display: inline-block;
            color: #a5a5a5;
            font-size: 12px;
            line-height: 20px;
            font-weight: 700;
            padding: 4px 20px;
            text-decoration: none;
            border-radius: 15px;
            margin: 0;
            -moz-border-radius: 15px;
            -webkit-border-radius: 15px;
            background: #fff;
            border: 1px solid #dedede;
            text-transform: uppercase;
            transition: all 0.3s ease-out;
            -moz-transition: all 0.3s ease-out;
            -webkit-transition: all 0.3s ease-out;
            -o-transition: all 0.3s ease-out;

        }

        .btn-remove:focus, .btn-remove:hover {
            color: #fff;
            background: crimson;
            border-color: darkred;
        }
    </style>
@endsection

@section("content")
    <div class="container" id="vm" v-cloak xmlns:v-on="http://www.w3.org/1999/xhtml">
        @php
            $links = [
                "Home" => route("home"),
                ($user = Auth::user())->role => route($user->role),
                "Toddler" => null
            ]
        @endphp
        @include("includes.templates.breadcrumb", ["links" => $links])

        {{--Browsing Section--}}
        <div class="row">
            <div class="col-md-4 form-horizontal">
                <br><br>
                <div class="form-group">
                    <label for="year" class="control-label col-sm-4">Year</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="year" id="year" v-model="year">
                            <option v-for="year in years" :value="year">@{{year}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cls" class="control-label col-sm-4">Class</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="cls" id="cls" v-model="cls">
                            <option v-for="class_year in class_years" :value="class_year.id">
                                @{{class_year.cls}}
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="section" class="control-label col-sm-4">Section</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="section" id="section" v-model="section">
                            <option v-for="class_year_section in class_year_sections" :value="class_year_section.id">
                                @{{class_year_section.section}}
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="student" class="control-label col-sm-4">Student</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="student" id="student" v-model="student">
                            <option v-for="student in students" :value="student.id">
                                @{{student.user.name}}
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-8 table-responsive">
                <table class="table table-hover table-striped" v-show="student">
                    <thead>
                    <tr class="text-success">
                        <th>Month</th>
                        <th>Height <em>(cm)</em></th>
                        <th>Weight <em>(kg)</em></th>
                        <th>Cognitive <em>(max 5)</em></th>
                        <th>Motor Skill <em>(max 5)</em></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="report in reports">
                        <td>@{{ ordinal(report.month) }}</td>
                        <td>@{{ report.height }}</td>
                        <td>@{{ report.weight }}</td>
                        <td>@{{ report.cognitive }}</td>
                        <td>@{{ report.motor_skill }}</td>
                        <td>
                            <button type="button" class="btn-remove" :id="'remove' + report.id"
                                    v-on:click="deleteReport(report.id)">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                    <tr style="border-top: 2px groove #3c763d;">
                        <td>
                            <input type="number" name="month" id="month"
                                   class="form-control" placeholder="Month" v-model="report.month">
                        </td>
                        <td>
                            <input type="text" name="height" id="height"
                                   class="form-control" placeholder="Height" v-model="report.height">
                        </td>
                        <td>
                            <input type="text" name="weight" id="weight"
                                   class="form-control" placeholder="Weight" v-model="report.weight">
                        </td>
                        <td>
                            <input type="number" name="cognitive" id="cognitive"
                                   class="form-control" placeholder="Cognitive" v-model="report.cognitive">
                        </td>
                        <td>
                            <input type="number" name="motor_skill" id="motor_skill"
                                   class="form-control" placeholder="Motor Skill" v-model="report.motor_skill">
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <button type="button" class="btn btn-success" v-on:click="createReport">Add</button>
                        </td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @include("includes.modals.error")
    </div>
    <br>
    <div style="position: relative;">
        <div class="wave2"></div>
    </div>
@endsection

@section("script")
    <script src="{{asset("js/vue.js")}}"></script>
    <script src="{{asset("js/axios.js")}}"></script>
    <script src="{{asset("js/lodash.js")}}"></script>

    <script>
        let $inputs, $modalError;
        $(document).ready(function () {
            $inputs = $("#vm").find("select, input");
            $modalError = $("#modal-error");
        });

        const vm = new Vue({
            el: "#vm",
            data: {
                classYearSections: {!! json_encode($classYearSections) !!},
                year: undefined,
                cls: undefined,
                section: undefined,
                class_years: [],
                class_year_sections: [],
                students: [],
                student: undefined,
                reports: [],
                report: {
                    month: undefined,
                    height: undefined,
                    weight: undefined,
                    cognitive: undefined,
                    motor_skill: undefined
                },
                errors: []
            },
            computed: {
                years: function () {
                    return _.map(this.classYearSections, 'year')
                }
            },
            watch: {
                year: function () {
                    const i = _.findIndex(this.classYearSections, ['year', this.year]);
                    if (i === -1)
                        this.class_years = [];
                    else
                        this.class_years = this.classYearSections[i].class_years;
                },
                cls: function () {
                    const i = _.findIndex(this.class_years, ['id', this.cls]);
                    if (i === -1)
                        this.class_year_sections = [];
                    else
                        this.class_year_sections = this.class_years[i].class_year_sections;
                },
                section: function () {
                    const i = _.findIndex(this.class_year_sections, ['id', this.section]);
                    if (i === -1)
                        this.students = [];
                    else
                        this.students = this.class_year_sections[i].students;
                },
                student: function () {
                    $inputs.attr("disabled", true);

                    axios.get('{{url("/teacher/toddler/report/")}}/' + this.student)
                        .then((response) => {
                            this.reports = response.data.toddler;
                            $inputs.attr("disabled", false);
                        });
                }
            },
            methods: {
                ordinal: function (num) {
                    num = Number.parseInt(num)
                    const dividend = num % 10;
                    if (dividend === 1 && num !== 11) {
                        return num + "st";
                    }
                    if (dividend === 2 && num !== 12) {
                        return num + "nd";
                    }
                    if (dividend === 3 && num !== 13) {
                        return num + "rd";
                    }
                    return num + "th";
                },
                createReport: function () {
                    if (!this.student || !this.report.month || !this.report.height || !this.report.weight || !this.report.cognitive || !this.report.motor_skill) {
                        return;
                    }

                    $inputs.attr("disabled", true);
                    axios.post('{{url("/teacher/toddler/report")}}/' + this.student, this.report)
                        .then(response => {
                            this.report.id = response.data.id;
                            this.reports.splice(this.reports.length, 0, this.report);
                            this.report = {
                                month: undefined,
                                height: undefined,
                                weight: undefined,
                                cognitive: undefined,
                                motor_skill: undefined
                            };

                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                        .catch(() => {
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        });
                },
                deleteReport: function (id) {
                    $button = $('#remove' + id).attr("disabled", true);
                    $i = $button.find('i');
                    $i.removeClass('fa-trash-o').addClass('fa-spinner');

                    axios.delete('{{url("/teacher/toddler/report")}}/' + id)
                        .then(response => {
                            const i = _.findIndex(this.reports, ['id', id]);
                            this.reports.splice(i, 1);
                            $i.removeClass('fa-spinner').addClass('fa-trash-o');
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                        .catch(error => {
                            if (error.response.status == 500) {
                                this.errors = [
                                    "Please reload the page and try again"
                                ];

                                $modalError.modal("show");
                            }
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        });

                }
            }
        })
    </script>
@endsection