@extends("layouts.register")

@section("inputs")
    <input type="hidden" name="role" value="{{\App\Models\User::STUDENT}}">

    <div class="form-group">
        <label for="year" class="col-md-4 control-label">Year</label>

        <div class="col-md-6">
            <select id="year" v-model="year" class="form-control">
                <option v-for="year in classYearSections" :value="year.year">
                    @{{ year.year }}
                </option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="cls" class="col-md-4 control-label">Class</label>

        <div class="col-md-6">
            <select id="cls" v-model="cls" class="form-control">
                <option v-for="cls in class_years" :value="cls.id">
                    @{{ cls.cls }}
                </option>
            </select>
        </div>
    </div>

    <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
        <label for="section" class="col-md-4 control-label">Section</label>

        <div class="col-md-6">
            <select name="section" id="section" v-model="section"
                    class="form-control" required>
                <option v-for="section in class_year_sections" :value="section.id"
                        :selected="section.id === '{{ old('name') }}'">
                    @{{ section.section }}
                </option>
            </select>

            @if ($errors->has('section'))
                <span class="help-block">
                    <strong>{{ $errors->first('section') }}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection

@section("script")
    <script src="{{asset("js/vue.js")}}"></script>
    <script src="{{asset("js/lodash.js")}}"></script>
    <script>
        const _classYearSections =
                {!! json_encode($classYearSections) !!}
        const _quarters = undefined;
        const _holidays = undefined;
    </script>
    <script src="{{asset("js/mixins.js")}}"></script>

    <script>
        const vm = new Vue({
            el: '#vm',
            mixins: [classYearSectionMixin],
            methods: {
                loadFromServer: function () {
                }
            }
        })
    </script>
@endsection
