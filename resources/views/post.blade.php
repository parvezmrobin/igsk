@extends('layouts.app')

@section('header-content')
    @include("includes.snail")
@endsection

@section("content")
    <div class="container">
        <article class="row">
            <h1 class="text-center">{{$post->title}}</h1>
            @if(!is_null($post->image))
                <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <img src="{{asset($post->image)}}"  alt="Image couldn't be loaded"
                         class="img img-responsive img-thumbnail">
                </div>
            @endif
            <div class="clearfix"></div>
            <br><br>
            <div class="col-lg-8 col-lg-offset-2">
                <p>{!! $post->post["text"] !!}</p>
            </div>
        </article>
        <br><br>
    </div>
    <div style="position: relative;">
        <div class="wave2"></div>
    </div>
@endsection