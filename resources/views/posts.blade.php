@extends('layouts.app')

@section("style")
    <style>
        .post{
            margin: 10px 0;
        }

        .post + hr {
            border-top-width: 3px;
        }
    </style>
 @endsection

@section('header-content')
    @include("includes.snail")
@endsection

@section('content')
    <div class="container">
        <h1 class="text-center">
            <i class="fa fa-{{$icon}}"></i>
            {{$pageTitle}}
        </h1>
        @include("includes.templates.breadcrumb",[
           "links" => [
               "Home" => url("home"),
               $pageTitle=> null,
           ]
       ])

        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                @each("includes.templates.post-preview", $posts, 'post')

                @if(!count($posts))
                    <h2 class="c text-center">No {{$pageTitle}} to show</h2>
                    <br><br>
                @endif
            </div>
        </div>
    </div>
    <div style="position: relative;">
        <div class="wave2"></div>
    </div>
@endsection