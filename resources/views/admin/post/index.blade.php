{{--
 - IGSK
 - User: Parvez M Robin
 - Mail: parvezmrobin@gmail.com
 - Date: 6/20/2018
 - Time: 7:59 AM
 --}}

@extends("layouts.app")

@section("style")
    <style>
        .form-horizontal label {
            padding-top: 18px;
            padding-bottom: 18px;

            font-weight: 400;
            font-size: 16px;
        }
    </style>
@endsection


@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="{{url("admin/post/create")}}" class="btn1 btn-default">Create Post</a>
            </div>
        </div>
        <hr class="blue">
        <div class="row">
            <form action="{{url("admin/posts")}}" method="get" class="form-horizontal">
                <div class="col-md-3 col-sm-6 form-inline">
                    <div class="checkbox">
                        <label for="event">
                            <input type="checkbox" name="event" id="event" {{$hasEvent? 'checked': ''}}>
                            Event
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 form-inline">
                    <div class="checkbox">
                        <label for="news">
                            <input type="checkbox" name="news" id="news" {{$hasNews? 'checked': ''}}>
                            News
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 form-inline">
                    <div class="checkbox">
                        <label for="notice">
                            <input type="checkbox" name="notice" id="notice" {{$hasNotice? 'checked': ''}}>
                            Notice
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 form-inline">
                    <button type="submit" class="btn btn-primary" style="float: right;">Refresh</button>
                </div>
            </form>
        </div>
        <hr class="blue">

        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Type</th>
                        <th>Last Update</th>
                        <th>Last Updated By</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td>
                                <a href="{{url("post/" . $post->id)}}" target="_blank">{{$post->title}}</a>
                            </td>
                            <td>{{$types[$post->type]}}</td>
                            <td>{{$post->updated_at->diffForHumans()}}</td>
                            <td>{{$post->user->name}}</td>
                            <td>
                                <a href="{{url("admin/post/edit/" . $post->id)}}">Edit</a>
                            </td>
                            <td>
                                <a href="#" class="text-danger" onclick="submitDeleteForm({{$post->id}})">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{$posts->links()}}
            </div>

            <form action="{{url("admin/post/delete")}}" hidden method="post" id="delete-form">
                {{csrf_field()}}
            </form>
        </div>
    </div>

    @include("includes.wave")
@endsection

@section("script")
    <script>
        function submitDeleteForm(postId) {
            const $frm = $('#delete-form');
            $frm.attr('action', $frm.attr('action') + '/' + postId);
            $frm.submit()
        }
    </script>
@endsection