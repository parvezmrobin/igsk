{{--
 - IGSK
 - User: Parvez M Robin
 - Mail: parvezmrobin@gmail.com
 - Date: 6/8/2018
 - Time: 11:45 PM
 --}}

@extends("layouts.app")


@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="text-center">Create Post</h1>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="{{url("/admin/post/" . (isset($post)? "edit/$post->id": "create"))}}"
                      method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="title" class="control-label col-md-2">Title</label>
                        <div class="col-md-10">
                            <input type="text" name="title" id="title" class="form-control"
                                   value="{{isset($post)? $post->title: old("title")}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="type" class="control-label col-md-2">Type</label>
                        <div class="col-md-10">
                            <select name="type" id="type" class="form-control">
                                <option {{(isset($post) && $post->type == App\Models\Post::EVENT)? 'selected': ''}}
                                        value="{{App\Models\Post::EVENT}}">
                                    Event
                                </option>
                                <option {{(isset($post) && $post->type == App\Models\Post::NEWS)? 'selected': ''}}
                                        value="{{App\Models\Post::NEWS}}">
                                    News
                                </option>
                                <option {{(isset($post) && $post->type == App\Models\Post::NOTICE)? 'selected': ''}}
                                        value="{{App\Models\Post::NOTICE}}">
                                    Notice
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="post" class="sr-only"></label>
                        <div class="col-md-10 col-md-offset-2">
                            <textarea name="post" id="post" cols="30"
                                      rows="10">{{isset($post)? $post->post["text"]: old("post")}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image" class="col-md-2 control-label">
                            {{isset($post)? "Update": "Add"}} Image
                        </label>
                        <div class="col-md-10">
                            <input type="file" name="image" id="image" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="file" class="col-md-2 control-label">
                            {{isset($post)? "Update": "Add"}} File
                        </label>
                        <div class="col-md-10">
                            <input type="file" name="file" id="file" class="form-control">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary" style="float: right;">
                        {{isset($post)? "Update": "Create"}}
                    </button>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>

    @include("includes.wave")
@endsection

@section("script")

    <script src="{{asset("js/tinymce/tinymce.min.js")}}"></script>

    <script>
        tinymce.init({
            selector: '#post',
            plugins: "link",
            setup: function (editor) {
                editor.on('load', function () {
                    $('.mce-branding').remove();
                })
            }
        })

    </script>
@endsection