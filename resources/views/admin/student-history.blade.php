@extends("layouts.app")

@section("style")
    <style>
        td {
            line-height: 1.5;
            font-size: 16px;
        }
    </style>
@endsection

@section("page-title", "Student History")

@section("content")

    <div class="container" id="vm" v-cloak>
        @include("includes.templates.breadcrumb", [
            "links" => [
                "Home" => url("home"),
                "Admin" => url("admin"),
                "Students" => null
            ]
        ])

        <div class="row">
            <!--region Browse-->
            <div class="col-lg-6 col-md-8 col-lg-offset-3 col-md-offset-2">
                <div class="form-horizontal">
                    <vue-select label="Year" :list="classYearSections" list-id="year"
                                css="bootstrap" v-model="year"></vue-select>
                    <vue-select label="Class" name="cls" :list="class_years" list-item="cls"
                                css="bootstrap" v-model="cls"></vue-select>
                    <vue-select label="Section" :list="class_year_sections" list-item="section"
                                css="bootstrap" v-model="section"></vue-select>
                </div>
            </div>
            <!--endregion-->
        </div>

        <div class="row">
            <div class="col-md-6" v-if="subject_teachers.length || class_teacher">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title"><h3>Teachers</h3></div>
                    </div>
                    <div class="panel-body">
                        <div class="lead" v-show="class_teacher">
                            Class Teacher: <b>@{{ class_teacher.name }}</b>
                        </div>
                        <br>
                        <div v-show="subject_teachers && subject_teachers.length" class="table-responsive">
                            <h6>Subject Teachers</h6>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Subject</th>
                                    <th>Teacher</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="st in subject_teachers">
                                    <td>@{{ st.subject.subject.subject }}:</td>
                                    <td><b>@{{ st.teacher.name }}</b></td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" v-if="students.length">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title"><h3>Students</h3></div>
                    </div>
                    <div class="panel-body table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="std in students">
                                <td><b>@{{ std.user.name }}</b></td>
                                <td>@{{ std.user.email }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @endsection

        @section("script")
            <script src="{{asset("js/vue.js")}}"></script>
            <script src="{{asset("js/vue-selectify.js")}}"></script>
            <script src="{{asset("js/axios.js")}}"></script>
            <script src="{{asset("js/lodash.js")}}"></script>

            <!--suppress JSUnusedLocalSymbols -->
            <script>
                const _classYearSections =
                        {!! json_encode($classYearSections) !!}
                const _quarters = undefined;
                const _holidays = undefined;
            </script>

            <script src="{{asset("js/mixins.js")}}"></script>

            <script>
                const vm = new Vue({
                    el: '#vm',
                    mixins: [classYearSectionMixin],
                    data: {
                        students: [],
                        class_teacher: undefined,
                        subject_teachers: []
                    },
                    methods: {
                        loadFromServer: function () {
                            if (this.section === undefined)
                                return;

                            axios.get("{{url("/admin/history")}}/" + this.section)
                                .then(response => {
                                    if (response.status == 200) {
                                        this.students = response.data.students;
                                        this.class_teacher = response.data.class_teacher;
                                        this.subject_teachers = response.data.subject_teachers;
                                    }
                                })
                        }
                    }
                })
            </script>
@endsection