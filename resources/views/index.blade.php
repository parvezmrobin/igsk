@extends("layouts.app")

@section("style")
    <style>
        .const {
            white-space: nowrap;
            border-radius: .2em;
            padding:10px;
        }
    </style>
@endsection
@section("slider")
    @include("includes.snail")
@endsection

@section("header-content")
    <div class="slogan1" style="text-shadow: 20px 20px 20px #000;">
        <i class="fa fa-cogs"></i>
        <span class="bg-warning text-primary const">Under Construction!</span>
    </div>
@endsection

@section("content")
    <div style="position: relative;">
        <div class="wave2"></div>
    </div>
@endsection