@extends("layouts.app")

@section("style")
    <style>
        .fancy {
            font-family: 'Finger Paint', fantasy;
            font-weight: bold;
        }

        h4{
            color: chocolate;
        }

        .teacher{
            margin: 10px 0;
        }

        .teacher + hr {
            border-top-width: 3px;
        }

        li.subject {
            color: cornflowerblue;
            font-size: 1.2em;
            line-height: 1.5;
            list-style: none;
        }

        a:hover, a:focus {
            color: #2a6496;
            text-decoration: underline;
        }
    </style>
@endsection

@section("header-content")
    @include("includes.snail")
@endsection

@section("content")
    <div class="container">
        <h1 class="text-center">Teachers</h1>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h2 class="c text-center">Class Teachers</h2>
                @each("includes.templates.class-teacher", $classTeachers, 'teacher')
            </div>

            {{--<div class="col-md-6">--}}
                {{--<h2 class="c">Subject Teachers</h2>--}}
                {{--@each("includes.templates.subject-teacher", $subjectTeachers, 'teacher')--}}
            {{--</div>--}}
        </div>
    </div>

@endsection