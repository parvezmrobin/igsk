<div class="snail1_wrapper">
    <div class="tree1 animated" data-animation="fadeIn" data-animation-delay="200">
        <img src="{{asset("images/tree1.png")}}" alt="" class="img-responsive">
    </div>
    <div class="snail1 animated" data-animation="bounceInLeft" data-animation-delay="300">
        <img src="{{asset("images/snail1.png")}}" alt="" class="img-responsive">
    </div>
</div>