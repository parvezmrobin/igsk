<div class="breadcrumbs1_wrapper">
    <div class="container">
        <div class="breadcrumbs1 text-center">
            @foreach($links as $text => $link)
                @if(is_null($link))
                    {{$text}}
                @else
                    <a href="{{$link}}">{{$text}}</a>
                    <span></span>
                @endif
            @endforeach
        </div>
        <hr class="blue" style="border-top-width: 2px;">
    </div>
</div>