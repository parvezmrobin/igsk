<div class="form-group">
    <label for="year" class="control-label col-md-4">Year</label>
    <div class="col-md-8">
        <select name="year" id="year" class="form-control" v-model="year">
            <option :value="year.year" v-for="year in classYearSections">@{{ year.year }}</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="quarter" class="control-label col-md-4">Quarter</label>
    <div class="col-md-8">
        <select name="quarter" id="quarter" class="form-control" v-model="quarter">
            <option :value="quarter.id" v-for="quarter in quarters">@{{ quarter.quarter }}</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="cls" class="control-label col-md-4">Class</label>
    <div class="col-md-8">
        <select name="cls" id="cls" class="form-control" v-model="cls">
            <option :value="cls.id" v-for="cls in class_years">@{{ cls.cls }}</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="{{$type}}" class="control-label col-md-4">{{ucfirst($type)}}</label>
    <div class="col-md-8">
        <select name="{{$type}}" id="{{$type}}" class="form-control" v-model="{{$type}}">
            <option :value="{{$type}}.id" v-for="{{$type}} in class_year_{{$type}}s">
                {{ <?= e($type); ?>.<?= e($toShow); ?> }}
            </option>
        </select>
    </div>
</div>
