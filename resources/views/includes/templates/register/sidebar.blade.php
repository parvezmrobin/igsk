<ul class="nav nav-pills nav-stacked">
    <li id="std-reg" {{strcasecmp($for, "student")? "" : "class=active" }}>
        <a href="{{url("register")}}">Student</a>
    </li>
    <li id="tea-reg" {{strcasecmp($for, "teacher")? "" : "class=active" }}>
        <a href="{{url("register/teacher")}}">Teacher</a>
    </li>
    <li id="adm-reg" {{strcasecmp($for, "supervisor")? "" : "class=active" }} >
        <a href="{{url("register/supervisor")}}">Supervisor</a>
    </li>
    <li id="adm-reg" {{strcasecmp($for, "admin")? "" : "class=active" }}>
        <a href="{{url("register/admin")}}">Admin</a>
    </li>
</ul>