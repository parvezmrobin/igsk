<a href="{{url("post/$post->id")}}">
    <div class="row post">

        @if(!is_null($post->image))
            <div class="col-md-4">
                <img src="{{asset($post->image)}}" alt="" class="img img-circle img-responsive">
            </div>
        @endif

        <div class="col-sm-8">
            <h4 class="c">
                <strong class="text-uppercase text-primary">
                    {{$post->title}}
                </strong>
            </h4>
            <p>{!! substr(strip_tags($post->post["text"], "<br>"),0, 300) !!}</p>
            <span class="text-primary">See more...</span>
            <strong style="float: right;">{{$post->created_at->diffForHumans()}}</strong>

        </div>

    </div>
</a>
<hr>
