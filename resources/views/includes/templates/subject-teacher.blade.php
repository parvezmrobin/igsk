@php
    if(!isset($teacher->img)) $teacher->img = null;
@endphp


<div class="row teacher">

    <img src="{{asset("images/user.png")}}" alt="" class="img img-circle col-sm-4">

    <div class="col-sm-8">
        <h4 class="c">
            <strong class="text-uppercase">
                {{$teacher->name}}
            </strong>
        </h4>
        <ul>
            <li>Class <span class="fancy">{{$teacher->cls}}</span></li>
            <li>Section <span class="fancy">{{$teacher->section}}</span></li>
            <li>Email <a class="fancy" href="mailto:{{$teacher->email}}">{{$teacher->email}}</a></li>
            <li class="subject">{{$teacher->subject}}</li>
        </ul>
    </div>

</div>
<hr>
