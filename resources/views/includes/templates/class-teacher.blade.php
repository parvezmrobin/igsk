@php
    if(!isset($teacher->img)) $teacher->img = "http://canadianglobal.ca/wp-content/uploads/2015/09/Businessman-512.png";
@endphp


<div class="row teacher">
    <img src="{{asset("images/user.png")}}" alt="" class="img img-circle col-sm-4">
    <div class="col-sm-8">
        <h4 class="c">
            <strong class="text-uppercase">
                {{$teacher->name}}
            </strong>
        </h4>
        <ul>
            <li>
                Class
                @foreach($teacher->classYearSections as $classYearSection)
                    @if($loop->first)
                        <span class="fancy">
                            {{$classYearSection->classYear->cls . (($loop->remaining>1)? ', ': '')}}
                        </span>
                    @elseif($loop->last)
                        and <span class="fancy">{{$classYearSection->classYear->cls}}</span>
                    @else
                        <span class="fancy">
                            {{$classYearSection->classYear->cls . (($loop->remaining>1)? ', ': '')}}
                        </span>
                    @endif
                @endforeach
            </li>

            <li>Email <a class="fancy" href="mailto:{{$teacher->email}}">{{$teacher->email}}</a></li>
        </ul>
    </div>
</div>
<hr>
