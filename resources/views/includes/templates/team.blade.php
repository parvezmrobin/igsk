<div class="col-md-4 col-lg-3 col-sm-5 col-xs-10 col-xs-offset-1 col-md-offset-0">
    <div class="team">
        <div class="team_inner">
            <a href="{{url($url)}}">
                <figure>
                    @php
                        if (!isset($img)) $img = "images/banner1.jpg";
                        if (!isset($imgTitle)) $imgTitle = null;
                    @endphp

                    <img src="{{asset($img)}}" alt="{{$imgTitle}}" class="img-responsive">
                    <em>{{$imgTitle}}</em>
                </figure>
                <div class="caption">
                    <div class="txt1">{{$title}}</div>
                    <div class="txt2">
                        @isset($detail)
                            {{$detail}}
                        @endisset
                    </div>
                    <div class="txt3">
                   <span>
                       @if(isset($btnText))
                           {{$btnText}}
                       @else
                           Visit
                       @endif
                   </span>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>