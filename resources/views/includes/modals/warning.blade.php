<!-- Modal -->
<div id="modal-warning" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-warning">Warning!</h4>
            </div>
            <div class="modal-body bg-warning">
                <ul>
                    <li v-for="warning in warnings">@{{ warning }}</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="btnProceedClicked">Proceed</button>
            </div>
        </div>

    </div>
</div>