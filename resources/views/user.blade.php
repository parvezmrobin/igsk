@extends("layouts.app")


@section("header-content")
    <div class="container">
        <div class="row col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
            <div class="col-md-12">
                @if(count($errors))
                    <div class="alert alert-danger alert-dismissable fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li><strong>{{$error}}</strong></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session("info"))
                    <div class="alert alert-info alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{session("info")}}</strong>
                    </div>
                @endif
            </div>

            <div class="col-md-4 pull-right">
                <img src="{{asset($user->image)}}" alt="{{$user->name}}"
                     class="img img-responsive thumbnail ">
            </div>
            <div class="col-md-8">
                <div class="well">
                    <h3>{{$user->name}}</h3>
                    <hr class="blue">
                    <p>
                        Email: <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                    </p>
                    <p><kbd>{{ucfirst($user->role)}}</kbd></p>
                </div>

                <button type="button" class="btn btn-primary"
                        data-toggle="modal" data-target="#pass-cng-modal">Change Password
                </button>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="pass-cng-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-primary" style="border-top-left-radius: 6px; border-top-right-radius: 6px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title ">Change Password</h4>
                </div>
                <div class="modal-body">
                    <form id="password-cng" method="post" class="form-horizontal" action="{{url("password/change")}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="control-label col-md-4" for="old">Old Password</label>
                            <div class="col-md-8">
                                <input type="password" name="old" id="old" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="new">New Password</label>
                            <div class="col-md-8">
                                <input type="password" name="new" id="new" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="new_confirmation">
                                Confirm Password
                            </label>
                            <div class="col-md-8">
                                <input type="password" name="new_confirmation" id="new_confirmation"
                                       class="form-control" required>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input form="password-cng" type="submit" name="submit" id="submit" class="btn btn-primary">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

@endsection

@section("content")
    <div style="position: relative;">
        <div class="wave2"></div>
    </div>
@endsection
