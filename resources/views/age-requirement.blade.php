@extends('layouts.app')

@section('header-content')
    @include("includes.snail")
@endsection

@section('content')
    <div class="container">
        <h1 class="text-center">
            <i class="fa fa-child"></i>
            Age Requirements
        </h1>
        @include("includes.templates.breadcrumb",[
            "links" => [
                "Home" => url("home"),
                "Age Requirement" => null,
            ]
        ])

        <p> IGSK considers age placement screening, school records, achievement level, and emotional and physical maturity.
            Moreover, the school reserves the right to place a student considering their respective aged peer groups
            to serve them suitably.
            {{--make the final placement decision keeping in mind that children are best served--}}
            {{--when placed in their respective aged peer groups.--}}
        </p>
        <br>

        <p>
           The following table gives an overview of 'aged peer groups' in different school systems.
        </p>
        <br>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Minimum age before August 31 ( Age cutoff date)</th>
                <th>IGSK classes</th>
                <th>US grade equivalent</th>
                <th>British Class Equivalent</th>
                <th>Bangladesh Grade Equivalent</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>3+ years</td>
                <td>Play Group</td>
                <td>Pre-K3</td>
                <td>Nursery</td>
                <td>Playgroup</td>
            </tr>
            <tr>
                <td>4+ years</td>
                <td>Kinder I</td>
                <td>Pre-K4</td>
                <td>Reception</td>
                <td>Nursery</td>
            </tr>
            <tr>
                <td>5+ years</td>
                <td>Kinder II</td>
                <td>Kindergarten</td>
                <td>Year 1</td>
                <td>Kindergarten 1</td>
            </tr>
            <tr>
                <td>6+ years</td>
                <td>Grade 1</td>
                <td>Grade 1</td>
                <td>Year 2</td>
                <td>Kindergarten 2</td>
            </tr>
            <tr>
                <td>7+ years</td>
                <td>Grade 2</td>
                <td>Grade 2</td>
                <td>Year 3</td>
                <td>Class I</td>
            </tr>
            <tr>
                <td>8+ years</td>
                <td>Grade 3</td>
                <td>Grade 3</td>
                <td>Year 4</td>
                <td>Class II</td>
            </tr>
            <tr>
                <td>9+ years</td>
                <td>Grade 4</td>
                <td>Grade 4</td>
                <td>Year 5</td>
                <td>Class III</td>
            </tr>
            <tr>
                <td>10+ years</td>
                <td>Grade 5</td>
                <td>Grade 5</td>
                <td>Year 6</td>
                <td>Class IV</td>
            </tr>
            </tbody>

        </table>
    </div>
    <br><br>
    <div style="position: relative;">
        <div class="wave2"></div>
    </div>
@endsection
