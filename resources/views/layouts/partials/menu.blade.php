<div class="navbar navbar_ navbar-default">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
            data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <div class="navbar-collapse navbar-collapse_ collapse">
        <ul class="nav navbar-nav sf-menu clearfix">
            <li class="nav1"><a href="{{url("/home")}}">Home</a></li>
            <li class="nav2 sub-menu sub-menu-1"><a href="#">About Us</a>
                <ul>
                    <li class="disabled">
                        <a href="{{url("profile")}}">Profile</a>
                    </li>
                    <li><a href="{{url("contact-us")}}">Contact Us</a></li>
                </ul>


            </li>


            <li class="nav3 sub-menu sub-menu-1"><a href="#">People</a>
                <ul>
                    <li class="disabled">
                        <a href="{{url("administrators")}}">Administrators</a>
                    </li>
                    <li><a href="{{url("teachers")}}">Teachers</a></li>
                    <li class="disabled"><a href="#">Staff</a></li>
                </ul>
            </li>
            <li class="nav4 sub-menu sub-menu-1"><a href="#">Admission</a>
                <ul>
                    <li>
                        <a href="{{url("how-to-apply")}}">How to apply</a>
                    </li>
                    <li>
                        <a href="{{url("age-requirement")}}">Age Requirement</a>
                    </li>
                </ul>
            </li>

            @if(!Auth::check())
                <li class="nav5"><a href="{{url("login")}}">Log in</a></li>
            @else
                @php
                    $user = Auth::user();
                    $firstName = $user->first_name
                @endphp
                <li class="nav5 sub-menu sub-menu-1"><a href="#">Hi {{$firstName}}!</a>
                    <ul>
                        @if($user->isInRole('student'))
                            <li><a href="{{route("Student")}}">Student</a></li>
                        @elseif($user->isInRole('teacher'))
                            <li><a href="{{route("Teacher")}}">Teacher</a></li>
                        @elseif($user->isInRole('supervisor'))
                            <li><a href="{{route("Supervisor")}}">Supervisor</a></li>
                        @elseif($user->isInRole('admin'))
                            <li><a href="{{route("Admin")}}">Admin</a></li>
                        @endif
                        <li><a href="{{url("user")}}">Account</a></li>
                        <li><a href="https://khulnaigs.com:2096">Mail</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</div>