@extends('layouts.app')

@section("page-title", "Register")

@section('content')
    <div class="container" {{strcasecmp($for, "student")? "" : "id=vm v-cloak"}}>
        <div class="row">
            <div class="col-sm-4 col-md-2">
                @include("includes.templates.register.sidebar", ["for" => $for])
            </div>
            <div class="col-sm-8 col-md-10">
                @if($name)
                    <div class="alert alert-success alert-dismissible col-md-8 col-md-offset-2">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        Registration of <strong>{{$name}}</strong> was successful.
                    </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    @include("includes.templates.register.user-inputs")

                    @yield("inputs")

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Register
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div style="position: relative;">
        <br>
        <br>
        <div class="wave2"></div>
    </div>
@endsection

