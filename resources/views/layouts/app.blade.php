<!DOCTYPE html>
<html lang="en">
<head>
    <title>International Grammar School, Khulna</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Your description">
    <meta name="keywords" content="Your keywords">
    <meta name="author" content="Your name">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{asset("images/favicon.png")}}" type="image/png">
    <link rel="shortcut icon" href="{{asset("images/favicon.png")}}" type="image/png"/>

    <link href="{{asset("css/bootstrap.css")}}" rel="stylesheet">
    <link href="{{asset("css/fontawesome-all.css")}}" rel="stylesheet">
    <link href="{{asset("css/flexslider.css")}}" rel="stylesheet">
    <link href="{{asset("css/animate.css")}}" rel="stylesheet">
    <link href="{{asset("css/style.css")}}" rel="stylesheet">

    @yield('style')

    <script src="{{asset("js/jquery.js")}}"></script>
    <script src="{{asset("js/jquery-migrate-1.2.1.min.js")}}"></script>
    <script src="{{asset("js/jquery.easing.1.3.js")}}"></script>
    <script src="{{asset("js/superfish.js")}}"></script>

    <script src="{{asset("js/jquery.parallax-1.1.3.resize.js")}}"></script>

{{--    <script src="{{asset("js/SmoothScroll.js")}}"></script>--}}

    <script src="{{asset("js/jquery.appear.js")}}"></script>

    <script src="{{asset("js/jquery.flexslider.js")}}"></script>

    <script src="{{asset("js/jquery.caroufredsel.js")}}"></script>
    <script src="{{asset("js/jquery.touchSwipe.min.js")}}"></script>

    <script src="{{asset("js/jquery.ui.totop.js")}}"></script>

    <script src="{{asset("js/script.js")}}"></script>

    <script>

    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="front">

<div id="main">

    <div class="top_wrapper">
        <div class="top_bg">
            <img src="{{asset("images/sky2.jpg")}}" alt="" class="sky2">
            <div class="grass1"></div>
            <div class="sun1"></div>
            <div class="green2"></div>
        </div>
        <div class="top_content">
            <div class="container">
                <header>
                    <div class="logo_wrapper">
                        <a href="/" class="logo" style="text-decoration: none;">
                            <h1 style="color: aliceblue; text-shadow: 1px 1px 2px #000;">
                                International Grammar School
                            </h1>
                        </a>
                    </div>
                </header>
                <div class="menu_wrapper">
                    @include("layouts.partials.menu")
                </div>

                @yield('slider')



                <div class="page_title">
                    @yield('page-title')
                </div>
                @yield('header-content')

            </div>
        </div>
        <div class="wave1"></div>
    </div>

    @yield('content')

    <div class="bot1_wrapper">

        <div class="container">

            <div class="logo2_wrapper">
                <a href="/" class="logo2" style="text-decoration: none">
                    <h2>International Grammar School</h2>
                </a>
            </div>

            <div class="row" style="display: none">
                <div class="col-sm-6">
                    <div class="bot1_title">Office:</div>
                    <p>
                        2/kha Mujgunni Mahasarak<br>
                        Boyra, Khulana - 9000.<br>
                        Contact No:01730585311, 01730585312<br>
                        E-mail: <a href="mailto:mailbox@khulnaigs.com">mailbox@khulnaigs.com</a>
                    </p>
                </div>

                <div class="col-sm-6" style="text-align: right;">
                    <div class="bot1_title">follow us:</div>
                    <div class="social_wrapper row">
                        <ul class="social clearfix">
                            <li><a href="#"><img src="{{asset("images/social_ic1.png")}}" alt=""></a></li>
                            {{--<li><a href="#"><img src="{{asset("images/social_ic2.png")}}" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="{{asset("images/social_ic3.png")}}" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="{{asset("images/social_ic4.png")}}" alt=""></a></li>--}}
                            <li><a href="https://www.facebook.com/khulnaigs" target="_blank"><img src="{{asset("images/social_ic5.png")}}" alt=""></a></li>
                            {{--<li><a href="#"><img src="{{asset("images/social_ic6.png")}}" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="{{asset("images/social_ic7.png")}}" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="{{asset("images/social_ic8.png")}}" alt=""></a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="bot2_wrapper" style="display: none;">
        <div class="container">
            <span style="display: inline-block;">
                Copyright IGS, Khulna © {{date("Y")}}. All rights reserved.
            </span>
            <span style="display: inline-block;">
                Developed by
                <a href="https://parvezmrobin.github.io/" title="Personal Homepage" target="_blank"><b>Parvez M Robin</b></a>
            &amp;
            <a href="https://www.linkedin.com/in/nzoishie/" title="LinkedIn Profile" target="_blank"><b>Naz Zarreen Oishie</b></a>.
            </span>
        </div>
    </div>


</div>
<script src="{{asset("js/bootstrap.min.js")}}"></script>

@yield('script')
</body>
</html>
