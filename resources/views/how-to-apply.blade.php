@extends('layouts.app')

@section('header-content')
    @include("includes.snail")
@endsection

@section('content')
    <div class="container">
        <h1 class="text-center">
            <i class="fa fa-edit"></i>
            How To Apply
        </h1>
        @include("includes.templates.breadcrumb",[
            "links" => [
                "Home" => url("home"),
                "How To Apply" => null,
            ]
        ])
        <b>Applications are submitted to our front desk admission officer.</b>
        <br><br>
        All records submitted with the application must be in English. Translated documents must be
        verified/attested by an official translator. Parents may not translate records.
        <br>
        The following documents must be completed and submitted to the office:
        <ul>
            <li>A completed application form.</li>
            <li>A completed health form.</li>
            <li>A copy of immunization records.</li>
            <li>A complete set of academic transcripts or report cards from previous school if applicable.</li>
            <li>Three passport size photographs.</li>
            <li>A copy of both parentsʼ Bangladeshi National ID Card / Passport.</li>
            <li>Speech/language, psycho-educational or special needs evaluations and or records (if
                applicable)</li>

        </ul>
        <br><br>
        <h2 style="color: red">Placement Testing</h2>
        All students applying to IGSK are required to complete a placement testing.
        The IGSK authority will notify the schedule for the placement test. All rights to change/reschedule/cancel the placement test wii
        be preserved by the authority.
        <br><br><br><br>

    </div>
    <div style="position: relative;">
        <div class="wave2"></div>
    </div>
@endsection