{{--
 - IGSK
 - User: Parvez M Robin
 - Mail: parvezmrobin@gmail.com
 - Date: 6/8/2018
 - Time: 3:35 PM
 --}}

@extends("layouts.app")

@section("header-content")
    @include("includes.snail")
@endsection

@section("content")
    <div class="container" id="vm" v-cloak="">
        <h1 class="text-center">
            <i class="fa fa-certificate"></i>
            Morality
        </h1>

        @include("includes.templates.breadcrumb", [
            "links" => [
                "Home" => url("home"),
                "Student" => url("student"),
                "Morality" => null
            ]
        ])

        <div class="row">
            <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3">
                <!--region Browse-->
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="month" class="control-label col-sm-4">Month</label>
                        <div class="col-sm-8">
                            <input type="number" name="month" id="month" class="form-control" v-model="month">
                        </div>
                    </div>
                </div>
                <!--endregion-->
            </div>

            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">

                <!--region Morality-->
                <div class="jumbotron" v-show="morality.id">
                    <div class="form-horizontal">
                        <div v-for="(rating, moral) in morality.morality" class="form-group">
                            <label for="morality" class="col-sm-4 control-label">
                                @{{ capitalize(moral) }}
                            </label>
                            <div class="col-md-8">
                                <rating :rating="rating" :show-rating="false" :item-size="30"></rating>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="alert alert-info lead" v-show="status">
                    @{{ status }}
                </div>
                <!--endregion-->
            </div>
        </div>
    </div>

    @include("includes.wave")
@endsection

@section("script")
    <script src="{{asset("js/vue.js")}}"></script>
    <script src="{{asset("js/axios.js")}}"></script>
    <script src="{{asset("js/lodash.js")}}"></script>
    <script src="{{asset("js/rate-it.js")}}"></script>

    <script>
        let $inputs;
        $(document).ready(function () {
            $inputs = $('#vm').find('input, select');
        });

        (function () {
            new Vue({
                el: '#vm',
                data: {
                    month: undefined,
                    morality: {
                        id: undefined,
                        morality: undefined
                    },
                    status: undefined
                },
                watch: {
                    month: _.debounce(function () {
                        this.loadFromServer()
                    }, 500, {trailing: true})
                },
                methods: {
                    loadFromServer: function () {
                        $inputs.attr("disabled", true);
                        axios.get("{{url("student/morality")}}/" + this.month)
                            .then(response => {
                                if (response.status === 200) {
                                    this.morality = response.data.morality;
                                    this.status = undefined;
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            })
                            .catch(error => {
                                if (error.response.status === 404) {
                                    this.morality = {
                                        id: undefined,
                                        morality: undefined
                                    };
                                    this.status = 'No morality available';
                                }
                                //Enable inputs
                                $inputs.attr("disabled", false);
                            })
                    },
                    capitalize: function (str) {
                        return str[0].toUpperCase() + str.substr(1);
                    }
                }
            })
        })()
    </script>
@endsection