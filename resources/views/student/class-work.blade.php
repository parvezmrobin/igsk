@extends("layouts.app")

@section("header-content")
    @include("includes.snail")
@endsection

@section("content")
    <div class="container" id="vm" v-cloak>
        <h1 class="text-center">
            <i class="fa fa-book"></i>
            Class Work
        </h1>

        @include("includes.templates.breadcrumb", [
            "links" => [
                "Home" => url("home"),
                "Student" => url("student"),
                "Class Work" => null
            ]
        ])

        <div class="row">

            <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3">
                <!--Browse Start-->
                <div class="form-horizontal">
                    <vue-select label="Year" name="year" :list="classYearSections" list-id="year"
                                css="bootstrap" v-model="year"></vue-select>
                    <vue-select label="Class" name="cls" :list="class_years" list-item="cls"
                                css="bootstrap" v-model="cls"></vue-select>
                    <vue-select label="Subject" name="subject" :list="class_year_subjects"
                                list-item="subject.subject"
                                css="bootstrap" v-model="subject"></vue-select>

                    <div class="form-group">
                        <label for="date" class="control-label col-sm-4">Date</label>
                        <div class="col-sm-8">
                            <input type="date" name="date" id="date" class="form-control" v-model="date">
                        </div>
                    </div>
                </div>
                <!--Browse Ends-->
            </div>

            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                <!--Class Work Starts-->
                <div class="well" v-show="class_work.id">
                    <div v-show="class_work.class_work.file && class_work.class_work.file.length">
                        <div class="row">
                            <label for="download" class="control-label col-sm-4">Download</label>
                            <div class="col-sm-8">
                                <a id="download"
                                   :href="'{{url("/student/class-work/download")}}/' + class_work.id">
                                    @{{ class_work.class_work.file }}
                                </a>
                            </div>
                        </div>

                        <hr class="blue">
                    </div>
                    <p v-show="class_work.class_work.text && class_work.class_work.text.length"
                       v-html="class_work.class_work.text">
                    </p>
                </div>
                <div class="alert alert-info lead" v-show="status.length">
                    @{{ status }}
                </div>
                <!--Class Work Ends-->
            </div>

        </div>
    </div>

    <div style="position: relative;">
        <br>
        <br>
        <div class="wave2"></div>
    </div>
@endsection

@section("script")
    <script src="{{asset("js/vue.js")}}"></script>
    <script src="{{asset("js/axios.js")}}"></script>
    <script src="{{asset("js/vue-selectify.js")}}"></script>
    <script src="{{asset("js/lodash.js")}}"></script>

    <!--suppress JSUnusedLocalSymbols -->
    <script>
        const _classYearSections =
                {!! json_encode($classYearSubjects) !!}
        const _quarters = undefined;
        const _holidays = undefined;
    </script>

    <script src="{{asset("js/mixins.js")}}"></script>

    <script>
        let $inputs;
        $(document).ready(function () {
            $inputs = $('#vm').find('input, select');
        });

        const vm = new Vue({
            el: '#vm',
            mixins: [classYearSubjectMixin],
            data: {
                class_work: {
                    id: undefined,
                    class_work: {
                        file: undefined,
                        text: undefined
                    }
                },
                date: undefined,
                status: ''
            },
            watch: {
                date: function () {
                    this.loadFromServer();
                }
            },
            methods: {
                loadFromServer: function () {
                    if (this.date === undefined ||
                        this.date === '' ||
                        this.subject === undefined)
                        return;

                    $inputs.attr("disabled", true);
                    axios.get("{{url("student/class-work")}}/" + this.subject + "?date=" + this.date)
                        .then(response => {
                            if (response.status == 200) {
                                this.class_work = response.data.class_work;
                                this.status = '';
                            }
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                        .catch(error => {
                            if (error.response.status == 404) {
                                this.class_work = {
                                    id: undefined,
                                    class_work: {
                                        file: undefined,
                                        text: undefined
                                    }
                                };
                                this.status = 'No classwork provided.';
                            }
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                }
            }
        })
    </script>
@endsection