@extends("layouts.app")

@section("page-title", "Student")

@section("style")
    <style>
        .team {
            padding: 10px 0;
        }
    </style>
@endsection

@section("header-content")
    <div class="container">

        @include("includes.templates.breadcrumb", ['links' => ["Home" => route("home"), "Student" => null]])
        <div class="row">
            @includeWhen($dayCare, "includes.templates.team", [
                'url' => '#',
                'title' => 'Day Care',
            ])

            @includeWhen ($prePlay || $junior, "includes.templates.team", [
                'url' => url("student/home-work"),
                'title' => 'Home Work'
            ])

            @includeWhen($junior, "includes.templates.team", [
                'url' => url("student/class-work"),
                'title' => 'Class Work'
            ])
            @includeWhen($junior, "includes.templates.team", [
                'url' => url("student/assessment"),
                'title' => 'Assessment'
            ])
            @includeWhen($junior, "includes.templates.team", [
                'url' => url('student/assignment'),
                'title' => 'Assignment'
            ])
            @includeWhen($junior, "includes.templates.team", [
                'url' => url('student/morality'),
                'title' => 'Morality'
            ])
        </div>
    </div>
@endsection

@section("content")
    @include("includes.wave")
@endsection

@section("script")
    <script>
        $(document).ready(function () {
            $('.nav6').addClass('active')
        })
    </script>
@endsection