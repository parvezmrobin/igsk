{{--
 - Author: Parvez M Robin
 - Email: parvezmrobin@gmail.com
 - Date: 2/9/2018
 - Time: 9:41 PM
 --}}
@extends("layouts.app")

@section("header-content")
    @include("includes.snail")
@endsection

@section("content")
    <div class="container" id="vm" v-cloak>
        <h1 class="text-center">
            <i class="fa fa-check-square"></i>
            Assessment
        </h1>

        @include("includes.templates.breadcrumb", [
            "links" => [
                "Home" => url("home"),
                "Student" => url("student"),
                "Assessment" => null
            ]
        ])

        <div class="row">

            <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3">
                <!--Browse Start-->
                <div class="form-horizontal">
                    <vue-select label="Year" name="year" :list="classYearSections" list-id="year"
                                css="bootstrap" v-model="year"></vue-select>
                    <vue-select label="Class" name="cls" :list="class_years" list-item="cls"
                                css="bootstrap" v-model="cls"></vue-select>
                    <vue-select label="Subject" name="subject" :list="class_year_subjects"
                                list-item="subject.subject" list-id="subject_id"
                                css="bootstrap" v-model="subject"></vue-select>

                    <div class="form-group">
                        <label for="date" class="control-label col-sm-4">Date</label>
                        <div class="col-sm-8">
                            <input type="date" name="date" id="date" class="form-control" v-model="date">
                        </div>
                    </div>
                </div>
                <!--Browse Ends-->
            </div>

            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                <!--Assessment Starts-->
                <div class="jumbotron" v-show="assessment.id">
                    <div v-show="assessment.comment && assessment.comment.length">
                        <p class="lead">
                            <b>Assessed as </b>@{{ assessment.comment }}
                        </p>

                        <hr class="blue">
                    </div>
                    <div v-show="assessment.rating" class="row">
                        <label for="rating" class="control-label col-xs-2">Rating</label>
                        <div class="col-xs-10">
                            <rating v-model="assessment.rating"
                                    :show-rating="false"
                                    :item-size="30"></rating>
                        </div>
                    </div>
                </div>
                <div class="alert alert-info text-center lead" v-show="status.length">
                    @{{ status }}
                </div>
                <!--Assessment Ends-->
            </div>

        </div>

    </div>

    @include("includes.wave")
@endsection

@section("script")
    <script src="{{asset("js/vue.js")}}"></script>
    <script src="{{asset("js/axios.js")}}"></script>
    <script src="{{asset("js/vue-selectify.js")}}"></script>
    <script src="{{asset("js/lodash.js")}}"></script>

    <!--suppress JSUnusedLocalSymbols -->
    <script>
        const _classYearSections =
                {!! json_encode($classYearSubjects) !!}
        const _quarters = undefined;
        const _holidays = {!! json_encode(config("app.holydays")) !!};
    </script>

    <script src="{{asset("js/mixins.js")}}"></script>
    <script src="{{asset("js/rate-it.js")}}"></script>

    <script>
        let $inputs;
        $(document).ready(function () {
            $inputs = $('#vm').find('input, select');
        });

        const vm = new Vue({
            el: '#vm',
            mixins: [classYearSubjectMixin, commonMixin],
            data: {
                assessment: {
                    id: undefined,
                    comment: undefined,
                    rating: undefined
                },
                date: (() => {
                    const date = new Date();
                    const m = date.getMonth();
                    const y = date.getFullYear();
                    const d = date.getDate();
                    return y + "-" + ((m < 9) ? '0' : '') + (m + 1) + "-" + ((d < 10) ? '0' : '') + d;
                })(),
                status: ''
            },
            watch: {
                date: function () {
                    this.loadFromServer();
                }
            },
            methods: {
                loadFromServer: function () {
                    if (this.subject === undefined ||
                        this.date === undefined ||
                        this.date === '' ||
                        this.assessment === undefined)
                        return;

                    $inputs.attr("disabled", true);
                    axios.get("{{url("student/assessment")}}/" + this.subject + "?date=" + this.date)
                        .then(response => {
                            if (response.status == 200) {
                                this.assessment = response.data.assessment;
                                this.status = '';
                            }
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                        .catch(error => {
                            if (error.response.status == 404) {
                                this.assessment = {
                                    id: undefined,
                                    comment: undefined,
                                    rating: undefined
                                };
                                this.status = 'No assessment given';
                            }
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                }
            }
        })
    </script>
@endsection