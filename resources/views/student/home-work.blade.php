@extends("layouts.app")

@section("header-content")
    @include("includes.snail")
@endsection

@section("content")
    <div class="container" id="vm" v-cloak>
        <h1 class="text-center">
            <i class="fa fa-pencil"></i>
            Home Work
        </h1>

        @include("includes.templates.breadcrumb", [
            "links" => [
                "Home" => url("home"),
                "Student" => url("student"),
                "Home Work" => null
            ]
        ])

        <div class="row">
            <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3">
                <!--region Browse-->
                <div class="form-horizontal">
                    <vue-select label="Year" name="year" :list="classYearSections" list-id="year"
                                css="bootstrap" v-model="year"></vue-select>
                    <vue-select label="Class" name="cls" :list="class_years" list-item="cls"
                                css="bootstrap" v-model="cls"></vue-select>
                    <vue-select label="Section" name="section" :list="class_year_sections" list-item="section"
                                css="bootstrap" v-model="section"></vue-select>

                    <div class="form-group">
                        <label for="date" class="control-label col-sm-4">Date</label>
                        <div class="col-sm-8">
                            <input type="date" name="date" id="date" class="form-control" v-model="date">
                        </div>
                    </div>
                </div>
                <!--endregion-->
            </div>

            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">

                <!--region Home Work-->
                <div class="well" v-show="home_work.id">
                    <div v-show="home_work.home_work.file && home_work.home_work.file.length">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="download" class="col-sm-4 control-label">Download</label>
                                <div class="col-md-8">
                                    <p class="form-control-static">
                                        <a id="download"
                                           :href="'{{url("/student/home-work/download")}}/' + home_work.id">
                                            @{{ home_work.home_work.file }}
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <hr class="blue">
                    </div>

                    <p v-show="home_work.home_work.text && home_work.home_work.text.length">
                        @{{home_work.home_work.text}}
                    </p>
                </div>
                <div class="alert alert-info lead" v-show="status.length">
                    @{{ status }}
                </div>
                <!--endregion-->
            </div>
        </div>
    </div>

    <div style="position: relative;">
        <br>
        <br>
        <div class="wave2"></div>
    </div>
@endsection

@section("script")
    <script src="{{asset("js/vue.js")}}"></script>
    <script src="{{asset("js/axios.js")}}"></script>
    <script src="{{asset("js/vue-selectify.js")}}"></script>
    <script src="{{asset("js/lodash.js")}}"></script>

    <!--suppress JSUnusedLocalSymbols -->
    <script>
        const _classYearSections =
                {!! json_encode($classYearSections) !!}
        const _quarters = undefined;
        const _holidays = undefined;
    </script>

    <script src="{{asset("js/mixins.js")}}"></script>

    <script>
        let $inputs;
        $(document).ready(function () {
            $inputs = $('#vm').find('input, select');
        });

        const vm = new Vue({
            el: '#vm',
            mixins: [classYearSectionMixin],
            data: {
                home_work: {
                    id: undefined,
                    home_work: {
                        file: undefined,
                        text: undefined
                    }
                },
                date: undefined,
                status: ''
            },
            watch: {
                date: function () {
                    this.loadFromServer();
                }
            },
            methods: {
                loadFromServer: function () {
                    if (this.date === undefined ||
                        this.date === '' ||
                        this.section === undefined)
                        return;

                    $inputs.attr("disabled", true);
                    axios.get("{{url("student/home-work")}}/" + this.section + "?date=" + this.date)
                        .then(response => {
                            if (response.status == 200) {
                                this.home_work = response.data.home_work;
                                this.status = '';
                            }
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                        .catch(error => {
                            if (error.response.status == 404) {
                                this.home_work = {
                                    id: undefined,
                                    home_work: {
                                        file: undefined,
                                        text: undefined
                                    }
                                };
                                this.status = 'No homework given';
                            }
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                }
            }
        })
    </script>
@endsection