{{--
 - Author: Parvez M Robin
 - Email: parvezmrobin@gmail.com
 - Date: 2/9/2018
 - Time: 9:41 PM
 --}}
@extends("layouts.app")

@section("header-content")
    @include("includes.snail")
@endsection

@section("content")
    <div class="container" id="vm" v-cloak>
        <h1 class="text-center">
            <i class="fa fa-envelope"></i>
            Assignment
        </h1>

        @include("includes.templates.breadcrumb", [
            "links" => [
                "Home" => url("home"),
                "Student" => url("student"),
                "Assignment" => null
            ]
        ])

        <div class="row">

            <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3">
                <!--Browse Start-->
                <div class="form-horizontal">
                    <vue-select label="Year" name="year" :list="classYearSections" list-id="year"
                                css="bootstrap" v-model="year"></vue-select>
                    <vue-select label="Class" name="cls" :list="class_years" list-item="cls"
                                css="bootstrap" v-model="cls"></vue-select>
                    <vue-select label="Section" name="section" :list="class_year_sections"
                                css="bootstrap" v-model="section"></vue-select>

                    <div class="form-group">
                        <label for="week" class="control-label col-sm-4">Week</label>
                        <div class="col-sm-8">
                            <input type="number" name="week" id="week" class="form-control" v-model="week">
                        </div>
                    </div>
                </div>
                <!--Browse Ends-->
            </div>

            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                <!--Assignment Starts-->
                <div class="well" v-show="assignment.id">
                    <div v-show="assignment.assignment.file && assignment.assignment.file.length">
                        <div class="row">
                            <label for="download" class="control-label col-sm-4">Download</label>
                            <div class="col-sm-8">
                                <a id="download"
                                   :href="'{{url("/student/assignment/download")}}/' + assignment.id">
                                    @{{ assignment.assignment.file }}
                                </a>
                            </div>
                        </div>

                        <hr class="blue">
                    </div>
                    <p v-show="assignment.assignment.text && assignment.assignment.text.length">
                        @{{assignment.assignment.text}}
                    </p>
                </div>
                <div class="alert alert-info lead" v-show="status.length">
                    @{{ status }}
                </div>
                <!--Assignment Ends-->
            </div>

        </div>

    </div>
    <div style="position: relative;">
        <br>
        <br>
        <div class="wave2"></div>
    </div>
@endsection

@section("script")
    <script src="{{asset("js/vue.js")}}"></script>
    <script src="{{asset("js/axios.js")}}"></script>
    <script src="{{asset("js/vue-selectify.js")}}"></script>
    <script src="{{asset("js/lodash.js")}}"></script>

    <!--suppress JSUnusedLocalSymbols -->
    <script>
        const _classYearSections =
                {!! json_encode($classYearSections) !!}
        const _quarters = undefined;
        const _holidays = undefined;
    </script>

    <script src="{{asset("js/mixins.js")}}"></script>

    <script>
        let $inputs;
        $(document).ready(function () {
            $inputs = $('#vm').find('input, select');
        });

        const vm = new Vue({
            el: '#vm',
            mixins: [classYearSectionMixin],
            data: {
                assignment: {
                    id: undefined,
                    assignment: {
                        file: undefined,
                        text: undefined
                    }
                },
                week: undefined,
                status: ''
            },
            watch: {
                week: function () {
                    this.loadFromServer();
                }
            },
            methods: {
                loadFromServer: function () {
                    if (this.week === undefined ||
                        this.week === '' ||
                        this.assignment === undefined)
                        return;

                    $inputs.attr("disabled", true);
                    axios.get("{{url("student/assignment")}}/" + this.section + "?week=" + this.week)
                        .then(response => {
                            if (response.status == 200) {
                                this.assignment = response.data.assignment;
                                this.status = '';
                            }
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                        .catch(error => {
                            if (error.response.status == 404) {
                                this.assignment = {
                                    id: undefined,
                                    assignment: {
                                        file: undefined,
                                        text: undefined
                                    }
                                };
                                this.status = 'No assignment given';
                            }
                            //Enable inputs
                            $inputs.attr("disabled", false);
                        })
                }
            }
        })
    </script>
@endsection