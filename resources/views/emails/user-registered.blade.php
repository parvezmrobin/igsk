<div style="font-family: Verdana, Cambria, Arial, sans-serif; ">
    <h2>Welcome, {{$user->name}}!</h2>
    <p>
        You are now a part of
        <kbd style='font-family: Menlo, Monaco, Consolas, "Courier New", monospace;padding: 2px 4px;
  font-size: 90%;
  color: #fff;
  background-color: #333;
  border-radius: 3px;
  box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.25);'>International Grammar School, Khulna</kbd>
        family.
        <a href="{{url("/")}}">Click here</a> to start you journey.
    </p>
</div>