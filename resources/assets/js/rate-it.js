import {HeartRating} from 'vue-rate-it';

Vue.component('rating', HeartRating);