<?php

namespace App\Http\Requests;

use App\Models\User;
use App\Models\Post;
use Auth;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var User $user */
        $user = Auth::user();
        return $user->isInRole([User::SUPERVISOR, User::ADMIN]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod("GET")) {
            return [];
        }

        $types = [Post::EVENT, Post::NEWS, Post::NOTICE];
        return [
            "title" => "required|string|max:255",
            "image" => "sometimes|image",
            "post" => "required|string|max:65535",
            "type" => "required|in:" . implode(",", $types)
        ];
    }
}
