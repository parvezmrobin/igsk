<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotFoundResponse;
use App\Models\Assignment;
use App\Models\ClassYearSection;
use App\Models\Quarter;
use App\Models\User;
use App\Models\Year;
use App\Repositories\SectionRepository;
use Auth;
use DB;
use Illuminate\Http\JsonResponse as Json;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Storage;

class AssignmentController extends Controller
{
    use Authorization;
    use NotFoundResponse;
    use SetTextFile;

    function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * @param Request $request
     * @return View
     */
    public function loadAssignment(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isInRole(User::TEACHER)) {
            $classYearSections = SectionRepository::taughtSections($user);
        } elseif ($user->isInRole(User::SUPERVISOR)) {
            $classYearSections = SectionRepository::supervisedSections($user);
        } elseif ($user->isInRole(User::ADMIN)) {
            $classYearSections = Year::query()->with("classYears.classYearSections")->get();
        } else {
            return $this->notFoundResponse($request);
        }

        return view(
            "teacher.assignment",
            [
                "classYearSections" => $classYearSections,
                'quarters' => Quarter::all()
            ]
        );
    }

    /**
     * @param Request $request
     * @param ClassYearSection $section
     * @param $week
     * @return Json
     */
    public function getAssignment(Request $request, ClassYearSection $section, $week)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $section)) {
            return $this->unauthorizedResponse($request);
        }

        $assignment = $section->getAssignment($week);

        return (is_null($assignment)) ?
            $this->notFoundResponse($request) :
            new Json(["assignment" => $assignment]);
    }

    public function createAssignment(Request $request, ClassYearSection $section)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $section)) {
            return $this->unauthorizedResponse($request);
        }

        /** Validate */
        $this->validate(
            $request,
            [
                'text' => 'sometimes|required_without:file|string',
                'file' => 'sometimes|required_without:text|file',
                'week' => 'required|integer',
                'quarter' => 'required|integer'
            ]);

        $assignment = $this->performCreation($request, $section);
        return new Json(["id" => $assignment->id]);
    }

    public function updateAssignment(Request $request, Assignment $assignment)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $assignment->section)) {
            return $this->unauthorizedResponse($request);
        }

        /** Validate */
        $this->validate(
            $request,
            [
                'text' => 'sometimes|required_without:file|string',
                'file' => 'sometimes|required_without:text|file',
            ]);

        $this->performUpdate($request, $assignment);
        return new Json();
    }

    /**
     * @param Request $request
     * @param Assignment $assignment
     * @return Json
     */
    public function destroyAssignment(Request $request, Assignment $assignment)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $assignment->section)) {
            return $this->unauthorizedResponse($request);
        }

        try {
            $assignment->delete();
        } catch (\Exception $e) {
            return $this->notFoundResponse($request);
        }
        return new Json();
    }

    /**
     * @param Request $request
     * @param Assignment $assignment
     * @return Json|\Illuminate\Http\Response
     */
    public function fileDownload(Request $request, Assignment $assignment)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $assignment->section)) {
            return $this->unauthorizedResponse($request);
        }

        $file = Storage::files("teacher/assignment/$assignment->id/")[0];
        return response()->download(storage_path("app/$file"));
    }

    /**
     * @param Request $request
     * @param ClassYearSection $section
     * @return Assignment
     */
    protected function performCreation(Request $request, ClassYearSection $section)
    {
        /** Start a transaction as creation and updating is interrelated */
        try {
            DB::beginTransaction();
        } catch (\Exception $e) {
        }

        /**
         * Creating the Assignment object
         * @var Assignment $assignment
         */
        $assignment = $section->assignments()->create(
            [
                'week' => $request->get('week'),
                'quarter_id' => $request->get('quarter'),
                'assignment' => [],
            ]);

        $this->setText($request, $assignment, 'assignment');
        $this->setFile($request, $assignment, 'assignment');
        $assignment->update();
        /** Commit the transaction */
        DB::commit();

        return $assignment;
    }

    /**
     * @param Request $request
     * @param Assignment $assignment
     */
    protected function performUpdate(Request $request, Assignment $assignment)
    {
        $this->setText($request, $assignment, 'assignment');
        $this->setFile($request, $assignment, 'assignment');
        $assignment->update();
    }
}
