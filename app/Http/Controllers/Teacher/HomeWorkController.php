<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\NotFoundResponse;
use App\Models\ClassYearSection;
use App\Models\HomeWork;
use App\Models\Quarter;
use App\Models\User;
use App\Repositories\SectionRepository;
use Auth;
use DB;
use Illuminate\Http\JsonResponse as Json;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class HomeWorkController extends Controller
{
    use Authorization;
    use NotFoundResponse;
    use SetTextFile;

    function __construct()
    {
        $this->middleware("auth");
    }

    public function loadHomeWorkPage(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isInRole(User::TEACHER)) {
            $classYearSections = SectionRepository::taughtSections($user);
        } elseif ($user->isInRole(User::SUPERVISOR)) {
            $classYearSections = SectionRepository::supervisedSections($user);
        } elseif ($user->isInRole(User::ADMIN)) {
            $classYearSections = SectionRepository::allSections();
        } else {
            return $this->notFoundResponse($request);
        }

        return view(
            "teacher.home-work",
            ["classYearSections" => $classYearSections, 'quarters' => Quarter::all()]
        );
    }

    public function getHomeWork(Request $request, ClassYearSection $section, $date)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $section)) {
            return $this->unauthorizedResponse($request);
        }

        $homeWork = $section->getHomeWork($date);

        return (is_null($homeWork)) ?
            $this->notFoundResponse($request) :
            new Json(["home_work" => $homeWork]);
    }

    public function createHomeWork(Request $request, ClassYearSection $section)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $section)) {
            return $this->unauthorizedResponse($request);
        }

        /** Validate */
        $this->validate(
            $request,
            [
                'text' => 'sometimes|required_without:file|string',
                'file' => 'sometimes|required_without:text|file',
                'date' => 'required|date',
                'quarter' => 'required|integer'
            ]);

        $homeWork = $this->performCreation($request, $section);
        return new Json(["id" => $homeWork->id]);
    }

    public function updateHomeWork(Request $request, HomeWork $homeWork)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $homeWork->section)) {
            return $this->unauthorizedResponse($request);
        }

        /** Validating request */
        $this->validate(
            $request,
            [
                'text' => 'sometimes|required_without:file|string',
                'file' => 'sometimes|required_without:text|file',
            ]);

        $this->performUpdate($request, $homeWork);
        return new Json();
    }

    /**
     * @param Request $request
     * @param HomeWork $homeWork
     * @return Json
     */
    public function destroyHomeWork(Request $request, HomeWork $homeWork)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $homeWork->section)) {
            return $this->unauthorizedResponse($request);
        }

        try {
            $homeWork->delete();
        } catch (\Exception $e) {
            return $this->notFoundResponse($request);
        }
        return new Json();
    }

    /**
     * @param Request $request
     * @param HomeWork $homeWork
     * @return Json|\Illuminate\Http\Response
     */
    public function fileDownload(Request $request, HomeWork $homeWork)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $homeWork->section)) {
            return $this->unauthorizedResponse($request);
        }

        $file = Storage::files("teacher/home-work/$homeWork->id/")[0];
        return response()->download(storage_path("app/$file"));
    }

    /**
     * @param Request $request
     * @param ClassYearSection $section
     * @return HomeWork
     */
    protected function performCreation(Request $request, ClassYearSection $section)
    {
        /** Start a transaction as creation and updating is interrelated */
        try {
            DB::beginTransaction();
        } catch (\Exception $e) {
        }

        /**
         * Creating the HomeWork object
         * @var HomeWork $homeWork
         */
        $homeWork = $section->homeWorks()->create(
            [
                'date' => $request->get('date'),
                'quarter_id' => $request->get('quarter'),
                'home_work' => []
            ]);

        $this->setText($request, $homeWork, 'home_work');
        $this->setFile($request, $homeWork, 'home_work');
        $homeWork->update();
        /** Commit the transaction */
        DB::commit();

        return $homeWork;
    }

    /**
     * @param Request $request
     * @param HomeWork $homeWork
     */
    protected function performUpdate(Request $request, HomeWork $homeWork)
    {
        $this->setText($request, $homeWork, 'home_work');
        $this->setFile($request, $homeWork, 'home_work');
        $homeWork->update();
    }
}
