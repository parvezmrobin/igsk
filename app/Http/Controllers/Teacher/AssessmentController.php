<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotFoundResponse;
use App\Models\Assessment;
use App\Models\ClassYearSubject;
use App\Models\Quarter;
use App\Models\Student;
use App\Models\User;
use App\Repositories\TeacherRepository;
use Illuminate\Http\JsonResponse as Json;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AssessmentController extends Controller
{
    use Authorization;
    use NotFoundResponse;

    function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * @param Request $request
     * @return Json|\Illuminate\View\View
     */
    public function loadAssessment(Request $request)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();

        if ($user->isInRole(User::TEACHER)) {
            $classYearSections = TeacherRepository::subordinateTeachers($user);
        } elseif ($user->isInRole(User::SUPERVISOR)) {
            $classYearSections = TeacherRepository::supervisedTeachers($user);
        } elseif ($user->isInRole(User::ADMIN)) {
            $classYearSections = TeacherRepository::allTeachers();
        } else {
            return $this->notFoundResponse($request);
        }

        return view(
            "teacher.assessment",
            [
                "classYearSections" => $classYearSections,
                'quarters' => Quarter::all()
            ]
        );
    }

    /**
     * @param Request $request
     * @param ClassYearSubject $subject
     * @return Json
     */
    public function getAssessment(Request $request, ClassYearSubject $subject)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $subject)) {
            return $this->unauthorizedResponse($request);
        }

        /** Validate */
        $this->validate(
            $request,
            [
                "date" => "required|date",
                "student" => "required|integer"
            ]);

        /** @var Assessment $assessment */
        new Assessment();
        $assessment = $subject->getAssessmentOfStudent(
            $request->get("date"),
            $request->get("student")
        );

        return (is_null($assessment)) ?
            $this->notFoundResponse($request) :
            new Json(["assessment" => $assessment]);
    }

    /**
     * @param Request $request
     * @param ClassYearSubject $subject
     * @return Json
     */
    public function createAssessment(Request $request, ClassYearSubject $subject)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        $student = (new Student)->find($request->request->get("student"));
        if (!$this->authorized($user, $student->section)) {
            return $this->unauthorizedResponse($request);
        }

        /** Validate */
        $studentHasSubject = $this->validateCreation($request, $subject, $student);
        if (!$studentHasSubject) {
            return new Json(
                ["errors" => ["invalid combination of student and subject"]],
                422);
        }

        /** @var Assessment $assessment */
        $assessment = $subject->assessments()->create(
            [
                'date' => $request->request->get("date"),
                'quarter_id' => $request->request->get("quarter"),
                'comment' => $request->request->get("comment"),
                'rating' => $request->request->get("rating"),
                'student_id' => $student->id
            ]);

        return new Json(["id" => $assessment->id]);
    }

    /**
     * @param Request $request
     * @param ClassYearSubject $subject
     * @param $student
     * @return bool
     */
    public function validateCreation(Request $request, ClassYearSubject $subject, $student)
    {
        $this->validate(
            $request,
            [
                "date" => "required|date",
                "quarter" => "required|integer",
                "comment" => "required|max:255",
                "rating" => "required|integer",
                "student" => "required|integer"
            ]);

        $studentHasSubject = $student->classYearSection->class_year_id === $subject->class_year_id;
        return $studentHasSubject;
    }

    /**
     * @param Request $request
     * @param Assessment $assessment
     * @return Json
     */
    public function updateAssessment(Request $request, Assessment $assessment)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $assessment->subject)) {
            return $this->unauthorizedResponse($request);
        }

        /** Validate */
        $this->validate(
            $request,
            [
                "comment" => "required|max:255",
                "rating" => "required|integer",
            ]);


        $assessment->comment = $request->get("comment");
        $assessment->rating = $request->get("rating");
        $assessment->update();

        return new Json();
    }

    /**
     * @param Request $request
     * @param Assessment $assignment
     * @return Json
     */
    public function destroyAssessment(Request $request, Assessment $assignment)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $assignment->subject)) {
            return $this->unauthorizedResponse($request);
        }

        try {
            $assignment->delete();
        } catch (\Exception $e) {
            return $this->notFoundResponse($request);
        }
        return new Json();
    }
}
