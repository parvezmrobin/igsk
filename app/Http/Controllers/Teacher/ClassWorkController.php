<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotFoundResponse;
use App\Models\ClassWork;
use App\Models\ClassYearSubject;
use App\Models\Quarter;
use App\Models\User;
use App\Repositories\SubjectRepository;
use Auth;
use Illuminate\Http\Request;
use DB;
use Storage;
use Illuminate\Http\JsonResponse as Json;

class ClassWorkController extends Controller
{
    use Authorization;
    use NotFoundResponse;
    use SetTextFile;

    function __construct()
    {
        $this->middleware("auth");
    }


    /**
     * @param Request $request
     * @return Json|\Illuminate\View\View
     */
    public function loadClassWorkPage(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isInRole(User::TEACHER)) {
            $classYearSubjects = SubjectRepository::taughtSubjects($user);
        } elseif ($user->isInRole(User::SUPERVISOR)) {
            $classYearSubjects = SubjectRepository::supervisedSubjects($user);
        } elseif ($user->isInRole(User::ADMIN)) {
            $classYearSubjects = SubjectRepository::allSubjects();
        } else {
            return $this->notFoundResponse($request);
        }

        return view(
            "teacher.class-work",
            ["classYearSubjects" => $classYearSubjects, 'quarters' => Quarter::all()]
        );
    }

    public function getClassWork(Request $request, ClassYearSubject $subject, $date)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $subject)) {
            return new Json(["errors" => [Json::$statusTexts[401]]], 401);
        }

        $classWork = $subject->getClassWork($date);

        return (is_null($classWork)) ?
            $this->notFoundResponse($request) :
            new Json(["class_work" => $classWork]);
    }

    public function createClassWork(Request $request, ClassYearSubject $subject)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$this->authorized($user, $subject)) {
            return $this->unauthorizedResponse($request);
        }

        /** Validate */
        $this->validate(
            $request,
            [
                'text' => 'sometimes|required_without:file|string',
                'file' => 'sometimes|required_without:text|file',
                'date' => 'required|date',
                'quarter' => 'required|integer'
            ]);

        $classWork = $this->performCreation($request, $subject);
        return new Json(["id" => $classWork->id]);
    }

    public function updateClassWork(Request $request, ClassWork $classWork)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $classWork->subject)) {
            return $this->unauthorizedResponse($request);
        }

        /** Validate */
        $this->validate(
            $request,
            [
                'text' => 'sometimes|required_without:file|string',
                'file' => 'sometimes|required_without:text|file',
            ]);

        $this->performUpdate($request, $classWork);
        return new Json();
    }

    public function destroyClassWork(Request $request, ClassWork $classWork)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $classWork->subject)) {
            return $this->unauthorizedResponse($request);
        }

        try {
            $classWork->delete();
        } catch (\Exception $e) {
            return $this->notFoundResponse($request);
        }
        return new Json();
    }

    /**
     * @param Request $request
     * @param ClassWork $classWork
     * @return Json|\Illuminate\Http\Response
     */
    public function fileDownload(Request $request, ClassWork $classWork)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$this->authorized($user, $classWork->subject)) {
            return $this->unauthorizedResponse($request);
        }

        $file = Storage::files("teacher/class-work/$classWork->id/")[0];
        return response()->download(storage_path("app/$file"));
    }

    /**
     * @param Request $request
     * @param ClassYearSubject $subject
     * @return ClassWork
     */
    protected function performCreation(Request $request, ClassYearSubject $subject)
    {
        try {
            DB::beginTransaction();
        } catch (\Exception $e) {
        }
        /**
         * Creating the ClassWork object
         * @var ClassWork $classWork
         */
        $classWork = $subject->classWorks()->create(
            [
                'date' => $request->get('date'),
                'quarter_id' => $request->get('quarter'),
                'class_work' => []
            ]);

        $this->setText($request, $classWork, 'class_work');
        $this->setFile($request, $classWork, 'class_work');
        $classWork->update();
        DB::commit();

        return $classWork;
    }

    /**
     * @param Request $request
     * @param ClassWork $classWork
     */
    protected function performUpdate(Request $request, ClassWork $classWork)
    {
        $this->setText($request, $classWork, 'class_work');
        $this->setFile($request, $classWork, 'class_work');
        $classWork->update();
    }
}
