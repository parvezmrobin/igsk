<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\NotFoundResponse;
use App\Models\Student;
use App\Models\Toddler;
use App\Models\User;
use App\Repositories\StudentRepository;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\JsonResponse as Json;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

class ToddlerController extends Controller
{
    use Authorization;
    use NotFoundResponse;

    function __construct()
    {
        $this->middleware("auth");
    }

    public function loadPage(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isInRole(User::TEACHER)) {
            $classYearSections = StudentRepository::taughtStudents($user);
        } elseif ($user->isInRole(User::SUPERVISOR)) {
            $classYearSections = StudentRepository::supervisedStudents($user);
        } elseif ($user->isInRole(User::ADMIN)) {
            $classYearSections = StudentRepository::allStudents();
        } else {
            return $this->notFoundResponse($request);
        }

        return view(
            "teacher.toddler.toddler",
            ["classYearSections" => $classYearSections]
        );
    }

    public function getReport(Request $request, Student $student)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();

        if (!$this->authorized($user, $student->section)) {
            return $this->unauthorizedResponse($request);
        }

        return new Json(["toddler" => $student->toddlers]);
    }

    public function createReport(Request $request, Student $student)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$this->authorized($user, $student->section)) {
            return $this->unauthorizedResponse($request);
        }

        /** Validate */
        $this->validate(
            $request,
            [
                'month' => 'required|integer',
                'height' => 'required|numeric',
                'weight' => 'required|numeric',
                'cognitive' => 'required|integer',
                'motor_skill' => 'required|integer',
            ]);


        /** @var Toddler $toddler */
        $attributes = $request->only(['month', 'height', 'weight', 'cognitive', 'motor_skill']);
        $toddler = $student->toddlers()->create($attributes);

        return new Json(["id" => $toddler->id]);
    }

    public function destroyReport(Request $request, Toddler $toddler)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$this->authorized($user, $toddler->student->section)) {
            return $this->unauthorizedResponse($request);
        }

        try {
            $toddler->delete();
        } catch (\Exception $e) {
            return $this->notFoundResponse($request);
        }
        return new Json(["status" => ["removed" => true]]);
    }
}
