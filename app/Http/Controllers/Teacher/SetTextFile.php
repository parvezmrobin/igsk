<?php
/**
 * User: Parvez
 * Date: 1/2/2018
 * Time: 9:34 PM
 */

namespace App\Http\Controllers\Teacher;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Storage;

trait SetTextFile
{
    /**
     * @param Request $request
     * @param Model $model
     * @param null $var Name of array attribute
     */
    protected function setText(Request $request, Model $model, $var = null)
    {
        /** Putting the text, if any */
        if ($request->has('text')) {
            if (is_null($var)) {
                $var = Str::snake(get_class($model));
            }
            $array = $model->$var;
            $array["text"] = $request->get('text');
            $model->$var = $array;
        }
    }

    /**
     * @param Request $request
     * @param $model
     * @param null $var Name of array attribute
     */
    protected function setFile(Request $request, Model $model, $var = null)
    {
        if ($request->hasFile("file")) {
            if (is_null($var)) {
                /** @var string $var snake_case of ClassName */
                $var = get_class($model);
                $lastSlashPos = mb_strrpos($var, "\\");
                $var = Str::snake(Str::substr($var, $lastSlashPos + 1));
            }

            $array = $model->$var;
            $file = $request->file("file");
            $array['file'] = $file->getClientOriginalName();;
            $model->$var = $array;

            /** @var string $folderName kebab-case of ClassName */
            $folderName = str_replace('_', '-', $var);
            /** Delete existing file and save new one */
            Storage::deleteDirectory("teacher/$folderName/$model->id");
            $file->storeAs("teacher/$folderName/$model->id/", $file->getClientOriginalName());
        }
    }
}