<?php
/**
 * User: Parvez
 * Date: 11/27/2017
 * Time: 8:54 PM
 */

namespace App\Http\Controllers\Teacher;

use App\Models\IAssociatable;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse as Json;

trait Authorization
{
    protected function authorized(User $user, IAssociatable $association)
    {
        if ($user->isInRole(User::STUDENT)) {
            return false;
        }
        if ($user->isInRole(User::TEACHER) && !$association->isClassTeacher($user)) {
            return false;
        }

        // TODO: Subject Teacher access

        if ($user->isInRole(User::SUPERVISOR) && !$association->isSupervisor($user)) {
            return false;
        }

        return true;
    }

    protected function unauthorizedResponse(Request $request)
    {
        if ($request->expectsJson()) {
            return new Json(["errors" => [Json::$statusTexts[401]]], 401);
        } else {
            return view("errors.404");
        }
    }
}