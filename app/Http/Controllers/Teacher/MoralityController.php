<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\NotFoundResponse;
use App\Models\Morality;
use App\Models\Quarter;
use App\Models\Student;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\StudentRepository;
use Illuminate\Http\JsonResponse as Json;

class MoralityController extends Controller
{
    use Authorization;
    use NotFoundResponse;

    function __construct()
    {
        $this->middleware("auth");
    }


    public function loadMorality(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isInRole(User::TEACHER)) {
            $classYearSections = StudentRepository::taughtStudents($user);
        } elseif ($user->isInRole(User::SUPERVISOR)) {
            $classYearSections = StudentRepository::supervisedStudents($user);
        } elseif ($user->isInRole(User::ADMIN)) {
            $classYearSections = StudentRepository::allStudents();
        } else {
            return $this->notFoundResponse($request);
        }

        return view(
            "teacher.moral",
            [
                "classYearSections" => $classYearSections,
                "quarters" => Quarter::all(),
            ]);
    }

    public function getMorality(Request $request, Student $student, $month)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $student->section)) {
            return $this->unauthorizedResponse($request);
        }

        $morality = $student->getMorality($month);
        return (is_null($morality)) ?
            $this->notFoundResponse($request) :
            new Json(["morality" => $morality]);
    }

    public function createMorality(Request $request, Student $student)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $student->section)) {
            return $this->unauthorizedResponse($request);
        }

        $this->validate($request, [
            'month' => 'required|integer',
            'quarter' => 'required|integer',
            'morality' => 'required',
        ]);

        /** @var Morality $morality */
        $morality = $student->moralities()->create(
            [
                'month' => $request->get('month'),
                'quarter_id' => $request->get('quarter'),
                'morality' => $request->get('morality'),
            ]);

        return new Json(["id" => $morality->id]);
    }

    public function updatedMorality(Request $request, Morality $morality)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $morality->student->section)) {
            return $this->unauthorizedResponse($request);
        }

        /** Validate */
        $this->validate($request, ['morality' => 'required']);

        $morality->morality = $request->get("morality");
        $morality->update();

        return new Json();
    }

    /**
     * @param Request $request
     * @param Morality $morality
     * @return Json|\Illuminate\View\View
     */
    public function destroyMorality(Request $request, Morality $morality)
    {
        /**
         * Authorize
         * @var User $user
         */
        $user = Auth::user();
        if (!$this->authorized($user, $morality->student->section)) {
            return $this->unauthorizedResponse($request);
        }

        try {
            $morality->delete();
        } catch (\Exception $e) {
            return $this->notFoundResponse($request);
        }
        return new Json();
    }
}
