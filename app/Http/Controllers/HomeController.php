<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use NotFoundResponse;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('home');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("index");
    }

    public function teacher()
    {
        return view("teachers", [
            "classTeachers" => UserRepository::getClassTeachers(),
            "subjectTeachers" => UserRepository::getSubjectTeachers(),
        ]);
    }

    public function profile()
    {
        return view("profile");
    }
}
