<?php

namespace App\Http\Controllers\Student;

use App\Models\ClassYearSection;
use App\Models\HomeWork;
use App\Models\Year;
use App\Repositories\SectionRepository;
use Auth;
use App\Models\User;
use App\Http\Controllers\NotFoundResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse as Json;
use Storage;

class HomeWorkController extends Controller
{
    use NotFoundResponse;

    function __construct()
    {
        $this->middleware("auth");
    }

    public function loadPage(Request $request)
    {
        /**
         * @var Year $classYearSection Contains year-class-section hierarchically
         */
        /** @var User $user */
        $user = Auth::User();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }
        $classYearSections = SectionRepository::sectionsOfStudent($user);
        return (is_null($classYearSections)) ?
            $this->notFoundResponse($request) :
            view("student.home-work", ["classYearSections" => $classYearSections]);
    }

    public function getHomeWork(Request $request, ClassYearSection $section)
    {
        /**
         * @var HomeWork $homeWork Contains corresponding home-work
         */
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }
        $this->validate($request, ['date' => 'required|date']);
        $homework = $section->getHomeWork($request->get("date"));

        return (is_null($homework)) ?
            $this->notFoundResponse($request) :
            new Json(['home_work' => $homework]);
    }


    /**
     * @param Request $request
     * @param $homeWork
     * @return Json|\Illuminate\View\View
     */
    public function downloadHomeWork(Request $request, $homeWork)
    {
        /**
         * downloads the home-work file, if any
         */

        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }
        $file = Storage::files("teacher/home-work/$homeWork/")[0];
        return response()->download(storage_path("app/$file"));
    }
}
