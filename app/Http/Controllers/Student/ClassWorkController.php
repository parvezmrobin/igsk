<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotFoundResponse;
use App\Models\ClassWork;
use App\Models\ClassYearSubject;
use App\Models\User;
use App\Repositories\SubjectRepository;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse as Json;
use Illuminate\Http\Request;
use Storage;

class ClassWorkController extends Controller
{
    use NotFoundResponse;

    /**
     * ClassWorkController constructor.
     */
    public function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * load the HTML page
     * @param Request $request
     * @return Json|\Illuminate\View\View
     */
    public function loadPage(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }

        /**
         * @var Model $classYearSubject Contains year-class-subject hierarchically
         */
        $classYearSubjects = SubjectRepository::subjectsOfStudent($user);

        return (is_null($classYearSubjects)) ?
            $this->notFoundResponse($request) :
            view("student.class-work", ["classYearSubjects" => $classYearSubjects]);
    }

    /**
     * get ClassWork in json
     * @param Request $request
     * @param ClassYearSubject $subject
     * @return Json
     */
    public function getClassWork(Request $request, ClassYearSubject $subject)
    {
        /**
         * @var ClassWork $classWork Contains corresponding class-work
         */
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }

        $this->validate($request, ['date' => 'required|date']);

        $classWork = $subject->getClassWork($request->get("date"));
        if (is_null($classWork)) {
            return $this->notFoundResponse($request);
        }
        $cw = $classWork->class_work;
        $cw["text"] = nl2br($cw["text"]);
        $classWork->setAttribute('class_work', $cw);
        return new Json(["class_work" => $classWork]);
    }

    /**
     * @param Request $request
     * @param ClassWork $classWork
     * @return Json|\Illuminate\View\View
     */
    public function fileDownload(Request $request, $classWork)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }

        $file = Storage::files("teacher/class-work/$classWork/")[0];
        return response()->download(storage_path("app/$file"));
    }
}
