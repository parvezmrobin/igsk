<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotFoundResponse;
use App\Models\Assessment;
use App\Models\ClassYearSubject;
use App\Models\User;
use App\Repositories\SubjectRepository;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse as Json;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AssessmentController extends Controller
{
    use NotFoundResponse;

    function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * @param Request $request
     * @return View
     */
    public function loadPage(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }

        /**
         * @var Model $classYearSubject Contains year-class-subject hierarchically
         */
        $classYearSubjects = SubjectRepository::subjectsOfStudent($user);

        return (is_null($classYearSubjects)) ?
            $this->notFoundResponse($request) :
            view("student.assessment", compact("classYearSubjects"));
    }

    /**
     * @param Request $request
     * @param ClassYearSubject $subject
     * @return Json
     */
    public function getAssessment(Request $request, ClassYearSubject $subject)
    {
        /**
         * @var Assessment $assessment Contains corresponding assessment
         */
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }

        $this->validate($request, ['date' => 'required|date']);

        $assessment = $subject->getAssessmentOfUser($request->get("date"), $user);
        return (is_null($assessment)) ?
            $this->notFoundResponse($request) :
            new Json(["assessment" => $assessment]);
    }
}
