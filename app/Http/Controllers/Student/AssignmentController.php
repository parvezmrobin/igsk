<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\NotFoundResponse;
use App\Models\Assignment;
use App\Models\ClassYearSection;
use App\Models\User;
use App\Repositories\SectionRepository;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse as Json;
use Storage;

class AssignmentController extends Controller
{
    use NotFoundResponse;

    /**
     * ClassWorkController constructor.
     */
    public function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * load the HTML page
     * @param Request $request
     * @return Json|\Illuminate\View\View
     */
    public function loadPage(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }

        /**
         * @var Model $classYearSubject Contains year-class-subject hierarchically
         */
        $classYearSections = SectionRepository::sectionsOfStudent($user);

        return (is_null($classYearSections)) ?
            $this->notFoundResponse($request) :
            view("student.assignment", ["classYearSections" => $classYearSections]);
    }

    /**
     * get assignment in json
     * @param Request $request
     * @param ClassYearSection $section
     * @return Json
     */
    public function getAssignment(Request $request, ClassYearSection $section)
    {
        /**
         * @var Assignment $assignment Contains corresponding assignment
         */
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }

        $this->validate($request, ['week' => 'required|integer']);

        $assignment = $section->getAssignment($request->get("week"));
        return (is_null($assignment)) ?
            $this->notFoundResponse($request) :
            new Json(["assignment" => $assignment]);
    }

    /**
     * @param Request $request
     * @param $assignment
     * @return Json|\Illuminate\View\View
     */
    public function fileDownload(Request $request, $assignment)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }

        $file = Storage::files("teacher/assignment/$assignment/")[0];
        return response()->download(storage_path("app/$file"));
    }
}
