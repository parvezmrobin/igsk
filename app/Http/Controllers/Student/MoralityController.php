<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotFoundResponse;
use App\Models\Morality;
use App\Models\User;
use Auth;
use Illuminate\Http\JsonResponse as Json;
use Illuminate\Http\Request;
use View;

class MoralityController extends Controller
{
    use NotFoundResponse;

    function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * @param Request $request
     * @return View | Json
     */
    public function loadPage(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }

        return view("student.morality");
    }

    /**
     * @param Request $request
     * @param $month
     * @return Json
     */
    public function getMorality(Request $request, $month)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->notFoundResponse($request);
        }

        /**
         * @var Morality $morality Contains corresponding morality
         */
        $morality = (new Morality)->getMorality($user, $month);
        return (is_null($morality)) ?
            $this->notFoundResponse($request) :
            new Json(["morality" => $morality]);
    }
}
