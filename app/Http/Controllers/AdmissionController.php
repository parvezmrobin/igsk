<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdmissionController extends Controller
{
    public function apply()
    {
        return view("how-to-apply");
    }
    public function age()
    {
        return view("age-requirement");
    }
}
