<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotFoundResponse;
use App\Models\ClassYearSection;
use App\Models\User;
use App\Repositories\SectionRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StudentHistoryController extends Controller
{
    use NotFoundResponse;

    function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function loadPage(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        if (!$user->isInRole(User::ADMIN)) {
            return $this->notFoundResponse($request);
        }

        return view(
            "admin.student-history",
            [
                "classYearSections" => SectionRepository::allSections()
            ]);
    }

    /**
     * @param Request $request
     * @param ClassYearSection $section
     * @return JsonResponse|\Illuminate\View\View
     */
    public function getStudents(Request $request, ClassYearSection $section)
    {
        /** @var User $user */
        $user = $request->user();
        if (!$user->isInRole(User::ADMIN)) {
            return $this->notFoundResponse($request);
        }

        $section->load(
            [
                "classTeacher",
                "subjectTeachers.teacher",
                "subjectTeachers.subject.subject",
                "students.user"
            ]);

        return new JsonResponse(
            [
                "class_teacher" => $section->classTeacher,
                "subject_teachers" => $section->subjectTeachers,
                "students" => $section->students,
            ]);
    }
}
