<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Teacher\Authorization;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Storage;

class UserPagesController extends Controller
{
    use Authorization;

    function __construct()
    {
        $this->middleware("auth");
    }

    public function showTeacherPage(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::TEACHER)) {
            return $this->unauthorizedResponse($request);
        }

        $toddler = $user->isToddlerClassTeacher();
        $juniorSchool = $user->isJuniorSchoolClassTeacher();

        return view("teacher.index", [
            "toddler" => $toddler,
            "juniorSchool" => $juniorSchool,
            "supervisor" => false,
            "admin" => false,
        ]);
    }

    public function showSupervisorPage(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::SUPERVISOR)) {
            return $this->unauthorizedResponse($request);
        }

        return view("teacher.index", [
            "toddler" => true,
            "juniorSchool" => true,
            "supervisor" => true,
            "admin" => false,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function showAdminPage(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::ADMIN)) {
            return $this->unauthorizedResponse($request);
        }

        return view("teacher.index", [
            "toddler" => true,
            "juniorSchool" => true,
            "supervisor" => true,
            "admin" => true,
        ]);
    }

    public function showStudentPage(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::STUDENT)) {
            return $this->unauthorizedResponse($request);
        }
        return view("student.index", [
            'dayCare' => $user->isDayCared,
            'prePlay' => $user->isPrePlayed,
            'junior' => $user->isJunior,
        ]);
    }

    public function showUserProfile(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        $user->image = "storage/user/$user->id";
        if (!Storage::exists($user->image)) {
            $user->image = "storage/user/default.png";
        }

        return view("user", compact("user"));
    }
}
