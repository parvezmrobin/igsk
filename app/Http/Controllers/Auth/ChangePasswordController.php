<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Http\Request;

class ChangePasswordController extends Controller
{
    public function __invoke(Request $request)
    {
        $user = $request->user();
        $rules = [
            "old" => "required",
            "new" => "required|string|min:6|confirmed",
        ];
        $messages = [
            "new.min" => "Password must be at least 6 characters.",
            "new.confirmed" => "Password and confirm password ain't same.",
        ];
        $this->validate($request, $rules, $messages);

        $oldPassword = $request->get("old");
        if (!Hash::check($oldPassword, $user->password)) {
            return response()->redirectTo("/user")
                ->withErrors(["old" => "Old password is incorrect."]);
        }

        $user->updatePassword($request->get("new"));
        return redirect("/user")
            ->with("info", "Password successfully updated.");
    }
}
