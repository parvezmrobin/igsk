<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\UserRegistrationConfirmation;
use App\Models\User;
use App\Repositories\SectionRepository;
use Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use Mail;
use Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers {
        register as protected traitRegister;
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @inheritdoc
     */
    public function showRegistrationForm(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if ($user->isInRole([User::STUDENT, User::TEACHER,])) {
            return view("errors.404");
        }
        $classYearSections = SectionRepository::sectionsOfRecentYear();
        $for = "student";
        $name = $request->get('name');
        return view(
            'auth.register.student',
            compact("classYearSections", "for", 'name')
        );
    }

    public function showTeacherRegistrationFrom(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!($user->isInRole(User::ADMIN) || $user->isInRole(User::SUPERVISOR))) {
            return view("errors.404");
        }
        $name = $request->get('name');

        return view("auth.register.teacher")->with("for", "teacher")->with("name", $name);
    }

    public function showSupervisorRegistrationFrom(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::ADMIN)) {
            return view("errors.404");
        }
        $name = $request->get('name');

        return view("auth.register.supervisor")->with("for", "supervisor")->with("name", $name);
    }

    public function showAdminRegistrationFrom(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isInRole(User::ADMIN)) {
            return view("errors.404");
        }
        $name = $request->get('name');

        return view("auth.register.admin")->with("for", "admin")->with("name", $name);
    }

    public function register(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($this->isUnauthorizedToCreateRole($role = $request->get('role'))) {
            if ($request->expectsJson()) {
                return response()->json(
                    [
                        "errors" => [
                            "User{role: $user->role} cannot create new user of role: " . $role
                        ]
                    ],
                    401);
            }
            throw new UnauthorizedException(
                "User{role: $user->role} cannot create new user of role: " . $request->get('role'));
        }

//        return $this->traitRegister($request);
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

//        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect(url($request->url()) . "?name=$user->name");
    }

    /**
     * @param string $role
     * @return bool
     */
    protected function isUnauthorizedToCreateRole($role)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isInRole(User::STUDENT)) {
            return true;
        } elseif ($user->isInRole(User::TEACHER)) {
            if (($role === User::TEACHER) || ($role === User::SUPERVISOR) || ($role === User::ADMIN)) {
                return true;
            }
            return false;
        } elseif ($user->isInRole(User::SUPERVISOR)) {
            if (($role === User::SUPERVISOR) || ($role === User::ADMIN)) {
                return true;
            }
            return false;
        } elseif ($user->isInRole(User::ADMIN)) {
            return false;
        }

        return true;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make(
            $data,
            [
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'role' => 'required|string|max:31|exists:roles',
                'section' => 'sometimes|required|integer|exists:class_year_sections,id'
            ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return (new User)->create(
            [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'role' => $data['role'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'info' => '[]',
            ]);
    }

    /**
     * @inheritdoc
     */
    protected function registered(Request $request, $user)
    {
        $role = $request->get("role");
        if ($role === User::STUDENT) {
            $this->createStudent($user, $request->get("section"));
        }

        Mail::to($user)->send(new UserRegistrationConfirmation($user));
    }

    public function createStudent(User $user, $section)
    {
        return $user->students()->create(
            [
                "class_year_section_id" => $section
            ]);
    }
}
