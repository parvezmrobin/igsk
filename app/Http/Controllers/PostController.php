<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use Illuminate\Filesystem\FilesystemManager;

class PostController extends Controller
{
    use NotFoundResponse;

    public function __construct()
    {
        $a = ";fdljk";
    }


    public function index(PostRequest $request)
    {
        if ($request->query->count() === 0) {
            $hasEvent = $hasNews = $hasNotice = 1;
            $posts = Post::getPosts();
        } else {
            $types = [];
            if ($request->has("event")) {
                $hasEvent = 1;
                $types[] = Post::EVENT;
            } else {
                $hasEvent = 0;
            }
            if ($request->has("news")) {
                $hasNews = 1;
                $types[] = Post::NEWS;
            } else {
                $hasNews = 0;
            }
            if ($request->has("notice")) {
                $hasNotice = 1;
                $types[] = Post::NOTICE;
            } else {
                $hasNotice = 0;
            }

            $posts = Post::getPostOfTypes($types);
        }

        $types = [
            Post::EVENT => "Event",
            Post::NEWS => "News",
            Post::NOTICE => "Notice",
        ];
        return view(
            "admin.post.index",
            compact("posts", "types", "hasEvent", "hasNews", "hasNotice")
        );
    }

    protected function retrievePost($type)
    {
        return Post::getPostOfTypes($type);
    }

    public function event()
    {
        $posts = $this->retrievePost(Post::EVENT);
        return view("posts", ["posts" => $posts, "pageTitle" => 'Events', "icon" => "calendar-alt"]);
    }

    public function news()
    {
        $posts = $this->retrievePost(Post::NEWS);
        return view("posts", ["posts" => $posts, "pageTitle" => "News", "icon" => "newspaper"]);
    }

    public function notice()
    {
        $posts = $this->retrievePost(Post::NOTICE);
        return view("posts", ["posts" => $posts, "pageTitle" => "Notices", "icon" => "bell"]);
    }

    public function show(Post $post)
    {
        return view("post", ["post" => $post]);
    }

    public function create(PostRequest $request)
    {
        return view("admin.post.create");
    }

    public function store(PostRequest $request)
    {
        $request->validate();
        $post = Post::store($request);

        return response()->redirectTo("post/$post->id");
    }

    public function edit(PostRequest $request, Post $post)
    {
        return view("admin.post.create", compact("post"));
    }

    public function update(PostRequest $request, Post $post)
    {
        $request->validate();
        Post::edit($request, $post);

        return response()->redirectTo("post/$post->id");
    }

    public function file(PostRequest $request, Post $post)
    {
        /** @var FilesystemManager $fs */
        $fs = app("filesystem");
        $filePath = $fs->allFiles("public/post/$post->id/file")[0];
        return response()->download($filePath);
    }

    public function destroy(PostRequest $request, Post $post)
    {
        try {
            /** @var FilesystemManager $fs */
            $fs = app("filesystem");
            $fs->deleteDirectory("public/post/$post->id/");

            $post->delete();
        } catch (\Exception $e) {
        }
        return response()->redirectTo("admin/posts");
    }
}
