<?php
/**
 * User: Parvez
 * Date: 12/4/2017
 * Time: 11:07 PM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse as Json;
use Illuminate\View\View;

trait NotFoundResponse
{
    /**
     * @param Request $request
     * @return Json|View
     */
    public function notFoundResponse(Request $request)
    {
        if ($request->expectsJson()) {
            return new Json(["errors" => [Json::$statusTexts[404]]], 404);
        }

        return view("errors.404");
    }
}