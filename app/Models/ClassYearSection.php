<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string id
 * @property string class_year_id
 * @property string teacher_id
 * @property ClassYear classYear
 * @property Student[] students
 * @property User classTeacher
 * @property mixed subjectTeachers
 */
class ClassYearSection extends Model implements IAssociatable
{
    public function classYear()
    {
        return $this->belongsTo(ClassYear::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class, 'section', 'section');
    }

    public function classTeacher()
    {
        return $this->belongsTO(User::class, 'teacher_id');
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function subjectTeachers()
    {
        return $this->hasMany(SubjectTeacher::class);
    }

    public function homeWorks()
    {
        return $this->hasMany(HomeWork::class);
    }

    /**
     * @param $date
     * @return HomeWork|null
     */
    public function getHomeWork($date) {
        return $this->homeWorks()
            ->whereDate("date", $date)
            ->first();
    }

    public function assignments()
    {
        return $this->hasMany(Assignment::class);
    }

    /**
     * @param $week
     * @return Assignment|null
     */
    public function getAssignment($week)
    {
        return $this->assignments()
            ->where("week", $week)
            ->first();
    }

    public function prePlays()
    {
        return $this->hasMany(PrePlay::class);
    }

    public function subjects()
    {
        return $this->hasMany(ClassYearSubject::class, 'class_year_id', 'class_year_id');
    }

    public function isClassTeacher(User $user)
    {
        return $this->teacher_id == $user->id;
    }

    public function isSupervisor(User $user)
    {
        return $this->classYear->classs->supervisor_id == $user->id;
    }
}
