<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string id
 * @property array morality
 * @property Student student
 */
class Morality extends Model
{
    const ATTENTIVE = "attentive";
    const POLITE = "polite";
    const FRIENDLY = "friendly";
    const HELPFUL = "helpful";

    protected $casts = [
        "morality" => "array"
    ];

    protected $fillable = [
        'quarter_id', 'month', 'morality'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function getMoralitiesOfUser(User $user, $year = null)
    {
        $year = is_null($year) ? (new Year)->latestYear() : $year;

        return $this->whereHas("student", function ($query) use ($user, $year) {
            /** @var Builder $query */
            $query->where("user_id", $user->id)
                ->whereHas(
                    "classYearSection.classYear",
                    function ($query) use ($year) {
                        /** @var Builder $query */
                        $query->where("year", $year->year);
                    });
        });
    }

    /**
     * @param User $user
     * @param $month
     * @param null $year
     * @return Morality|Builder|Model|null
     */
    public function getMorality(User $user, $month, $year = null)
    {
        return $this->getMoralitiesOfUser($user, $year)
            ->where("month", $month)
            ->first();
    }
}
