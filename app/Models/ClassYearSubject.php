<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string id
 * @property string class_year_id
 * @property mixed subject_id
 */
class ClassYearSubject extends Model implements IAssociatable
{
    /**
     * @param $date
     * @param $studentId
     * @return Assessment|null
     */
    public function getAssessmentOfStudent($date, $studentId)
    {
        return $this->assessments()
            ->where("date", $date)
            ->where("student_id", $studentId)
            ->first();
    }

    /**
     * @param $date
     * @param User $user
     * @return Assessment|null
     */
    public function getAssessmentOfUser($date, User $user)
    {
        return $this->assessments()
            ->where("date", $date)
            ->whereHas("student", function (Builder $query) use ($user) {
                $query->where("user_id", $user->id)
                    ->whereHas("classYearSection", function (Builder $query) {
                        $query->where("class_year_id", $this->class_year_id);
                    });
            })
            ->first();
    }

    public function classYear()
    {
        return $this->belongsTo(ClassYear::class);
    }

    public function classYearSections()
    {
        return $this->hasMany(ClassYearSection::class, 'class_year_id', 'class_year_id');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function assessments()
    {
        return $this->hasMany(Assessment::class, 'subject_id');
    }

    public function subjectTeachers()
    {
        return $this->hasMany(SubjectTeacher::class);
    }

    public function classWorks()
    {
        return $this->hasMany(ClassWork::class, 'subject_id');
    }

    /**
     * @param $date
     * @return ClassWork|null
     */
    public function getClassWork($date)
    {
        $classWork = $this->classWorks()
            ->whereDate("date", $date)
            ->first();
        return $classWork;
    }

    public function grades()
    {
        return $this->hasMany(Grade::class, 'subject_id');
    }

    public function isClassTeacher(User $user)
    {
        return !!static::where("id", $this->id)
            ->whereHas(
                "classYear.classYearSections",
                function (Builder $query) use ($user) {
                    return $query->where("teacher_id", $user->id);
                })->count();
    }

    public function isSupervisor(User $user)
    {
        return !!static::where("id", $this->id)
            ->whereHas(
                "classYear.cls",
                function ($query) use ($user) {
                    /**
                     * @var Builder $query
                     */
                    return $query->where("supervisor_id", $user->id);
                }
            )->count();
    }
}
