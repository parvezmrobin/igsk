<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public function classYearSections()
    {
        return $this->hasMany(ClassYearSection::class);
    }
}
