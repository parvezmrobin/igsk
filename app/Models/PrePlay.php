<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrePlay extends Model
{
    protected $casts = [
        "homework" => 'array'
    ];
}
