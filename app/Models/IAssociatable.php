<?php
/**
 * User: Parvez
 * Date: 11/27/2017
 * Time: 10:28 PM
 */

namespace App\Models;


interface IAssociatable
{
    public function isClassTeacher(User $user);

    public function isSupervisor(User $user);
}