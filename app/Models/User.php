<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;


/**
 * @property Collection classYearSections
 * @property Collection supervisedClasses
 * @property string id
 * @property string first_name
 * @property string last_name
 * @property string name
 * @property string role
 * @property string email
 * @property bool isDayCared
 * @property bool isPrePlayed
 * @property bool isJunior
 * @property string password
 */
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    const STUDENT = "Student";
    const TEACHER = "Teacher";
    const SUPERVISOR = "Supervisor";
    const ADMIN = "Admin";

    protected $casts = [
        "info" => "array",
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['name'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'role', 'info'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    /**
     * @param string|array $roles
     * @return bool
     */
    public function isInRole($roles)
    {
        foreach ((array)$roles as $role) {
             if (!strcasecmp($this->role, $role)){
                 return true;
             }
        }
        return false;
    }

    public function teacher()
    {
        return $this->hasOne(Teacher::class, "id");
    }

    public function classYearSections()
    {
        return $this->hasMany(ClassYearSection::class, "teacher_id");
    }

    /**
     * Supervised classes by the user
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function supervisedClasses()
    {
        return $this->hasMany(Cls::class, "supervisor_id");
    }

    /**
     * Determine if the user is a toddler Class Teacher
     * @return bool
     */
    public function isToddlerClassTeacher()
    {
        return !!(new ClassYearSection)->where("teacher_id", $this->id)
            ->join("class_years", "class_year_id", "class_years.id")
            ->whereIn("cls", ["Day Care", "Pre Play"])
            ->count();
    }

    /**
     * Determine if the user is a junior school Class Teacher
     * @return bool
     */
    public function isJuniorSchoolClassTeacher()
    {
        return !!(new ClassYearSection)->where("teacher_id", $this->id)
            ->join("class_years", "class_year_id", "class_years.id")
            ->whereNotIn("cls", ["Day Care", "Pre Play"])
            ->count();
    }

    /**
     * @inheritdoc
     */
    public function __get($key)
    {
        if ($key === "isDayCared") {
            return $this->isDayCared();
        } elseif ($key === "isPrePlayed") {
            return $this->isPrePlayed();
        } elseif ($key === "isJunior") {
            return $this->isJunior();
        }
        return parent::__get($key);
    }

    /**
     * @return bool
     */
    public function isDayCared()
    {
        return !!$this->students()->whereHas(
            "classYearSection.classYear",
            function ($query) {
                /** @var Builder $query */
                return $query->where("cls", Student::DAY_CARE);
            }
        )->count();
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    /**
     * @return bool
     */
    public function isPrePlayed()
    {
        return !!$this->students()->whereHas(
            "classYearSection.classYear",
            function ($query) {
                /** @var Builder $query */
                return $query->where("cls", Student::PRE_PLAY);
            }
        )->count();
    }

    /**
     * @return bool
     */
    public function isJunior()
    {
        return !!$this->students()->whereHas(
            "classYearSection.classYear",
            function ($query) {
                /** @var Builder $query */
                return $query->whereIn(
                    "cls",
                    [
                        Student::PLAY,
                        Student::NURSERY,
                        Student::KG,
                        Student::ONE,
                        Student::TWO,
                        Student::THREE,
                        Student::FOUR,
                        Student::FIVE,
                        Student::SIX,
                    ]);
            }
        )->count();
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function updatePassword($newPassword)
    {
        return $this->forceFill([
            'password' => bcrypt($newPassword),
            'remember_token' => Str::random(60),
        ])->save();
    }
}
