<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string supervisor_id
 */
class Cls extends Model
{
    protected $primaryKey = 'cls';
    protected $keyType = "string";
    protected $table = "clses";

    const PRE_PLAY = "Pre Play";
    const DAY_CARE = "Day Care";
    const NURSERY = "Nursery";
    const KG = "KG";
    const PLAY = "Play";
    const ONE = "One";
    const TWO = "Two";
    const THREE = "Three";
    const FIVE = "Five";

    public function classYears()
    {
        return $this->hasMany(ClassYear::class, 'cls');
    }

    public function supervisor()
    {
        return $this->belongsTo(User::class, 'supervisor_id');
    }
}
