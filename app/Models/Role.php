<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/** @property string role */
class Role extends Model
{
    protected $primaryKey = 'role';
    protected $keyType = 'string';
}
