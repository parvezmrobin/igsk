<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string id
 * @property string assessment
 * @property ClassYearSubject subject
 * @property string comment
 * @property string rating
 */
class Assessment extends Model
{
    protected $fillable = ['date', 'quarter_id', 'student_id', 'comment', 'rating'];

    public function subject()
    {
        return $this->belongsTo(ClassYearSubject::class, 'subject_id');
    }

    public function student()
    {
        return $this->belongsTo(Student::class, "student_id");
    }
}
