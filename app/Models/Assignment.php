<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property array assignment
 * @property string id
 * @property ClassYearSection $section
 */
class Assignment extends Model
{
    protected $fillable = ['id', 'quarter_id', 'week', 'assignment'];

    protected $casts = [
        "assignment" => 'array'
    ];

    public function section()
    {
        return $this->belongsTo(ClassYearSection::class, 'class_year_section_id');
    }
}
