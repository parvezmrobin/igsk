<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubjectTeacher extends Model
{
    public function classYearSubject()
    {
        return $this->belongsTo(ClassYearSubject::class);
    }

    public function subject()
    {
        return $this->classYearSubject();
    }

    public function classYearSection()
    {
        return $this->belongsTo(ClassYearSection::class);
    }

    public function teacher()
    {
        return $this->belongsTo(User::class, 'subject_teacher_id');
    }
}
