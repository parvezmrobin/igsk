<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $primaryKey = 'section';

    public function classYearSections()
    {
        return $this->hasMany(ClassYearSection::class, 'section', 'section');
    }
}
