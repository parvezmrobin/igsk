<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    const ENGLISH = "English";
    const BANGLA = "Bangla";
    const MATH = "Math";
    const SCIENCE = "Science";
    const GEOGRAPHY = "Geography";
    const RELIGIOUS_STUDY = "Religious Stydy";
    const ARTS_CRAFTS = "Art and Cratfts";
    const LIBRARY = "Library";
    const LINGUISTIC = "Linguistic";
    const LITERATURE = "Literature";
    const VASHA = "Bangla Vasha";
    const SAHITYO = "Bangla Sahityo";
    const SOCIAL_STUDIES = "Social Studies";
    const HISTORY = "History";
    const COMPUTER = "Computer";


    public function classYearSubjects()
    {
        return $this->hasMany(ClassYearSubject::class);
    }
}
