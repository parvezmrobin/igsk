<?php

namespace App\Models;

use App\Http\Requests\PostRequest;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\FilesystemManager;

/**
 * @property string title
 * @property string image
 * @property int user_id
 * @property array post
 * @property int type
 * @property int id
 */
class Post extends Model
{
    const EVENT = 1;
    const NEWS = 2;
    const NOTICE = 3;

    protected $casts = [
        'post' => 'array'
    ];

    public static function store(PostRequest $request)
    {
        $post = new Post();
        self::assignAttributes($request, $post);
        $post->save();

        if ($request->hasFile("image")) {
            self::assignImageAttribute($request, $post);
            $post->update();
        }
        if ($request->hasFile("file")) {
            self::assignFileAttribute($request, $post);
            $post->update();
        }

        return $post;
    }

    /**
     * @param PostRequest $request
     * @param Post $post
     */
    public static function edit(PostRequest $request, Post $post)
    {
        self::assignAttributes($request, $post);

        if ($request->hasFile("image")) {
            static::assignImageAttribute($request, $post);
        }
        if ($request->hasFile("file")) {
            self::assignFileAttribute($request, $post);
            $post->update();
        }

        $post->update();
    }

    /**
     * @param PostRequest $request
     * @param Post $post
     */
    protected static function assignAttributes(PostRequest $request, Post $post)
    {
        $post->title = $request->get("title");
        $postArray = $post->post;
        $postArray["text"] = $request->get("post");
        $post->post = $postArray;
        $post->type = $request->get("type");
        $post->user_id = Auth::id();
    }

    /**
     * @param PostRequest $request
     * @param Post $post
     * @return void
     */
    protected static function assignImageAttribute(PostRequest $request, Post $post)
    {
        $fileName = static::storeFile($request, $post, "image");
        $post->image = "storage/post/$post->id/image/$fileName";
    }

    /**
     * @param PostRequest $request
     * @param Post $post
     * @return void
     */
    protected static function assignFileAttribute(PostRequest $request, Post $post)
    {
        $fileName = static::storeFile($request, $post);
        $postArray = $post->post;
        $postArray["file"] = "storage/post/$post->id/file/$fileName";
        $post->post = $postArray;
    }

    /**
     * @param PostRequest $request
     * @param $post
     * @param string $input
     * @return string
     */
    protected static function storeFile(PostRequest $request, Post $post, $input = "file")
    {
        /** @var FilesystemManager $fs */
        $fs = app("filesystem");

        $fs->deleteDirectory("public/post/$post->id/$input");
        $fs->makeDirectory("public/post/$post->id/$input");
        $image = $request->file($input);
        $fileName = $image->getClientOriginalName();
        $image->storeAs("public/post/$post->id/$input", $fileName);

        return $fileName;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getPostOfTypes($type)
    {
        return (new Post)
            ->whereIn("type", (array)$type)
            ->with("user")
            ->latest()
            ->paginate();
    }

    public static function getPosts()
    {
        return (new Post)
            ->with("user")
            ->latest()
            ->paginate();
    }
}
