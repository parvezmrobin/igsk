<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string id
 * @property Student student
 */
class Toddler extends Model
{
    protected $fillable = ['month', 'height', 'weight', 'cognitive', 'motor_skill'];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
