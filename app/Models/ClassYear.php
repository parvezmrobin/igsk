<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property Cls classs
 */
class ClassYear extends Model
{
    public function year()
    {
        return $this->belongsTo(Year::class, 'year', 'year');
    }

    public function classs()
    {
        return $this->belongsTo(Cls::class, 'cls', 'cls');
    }

    public function classYearSections()
    {
        return $this->hasMany(ClassYearSection::class);
    }

    public function classYearSubjects()
    {
        return $this->hasMany(ClassYearSubject::class);
    }

    public function classTeachers()
    {
        return $this->belongsToMany(User::class, 'class_year_sections', 'class_year_id', 'teacher_id');
    }

    public function students()
    {
        return $this->hasManyThrough(Student::class, ClassYearSection::class);
    }
}
