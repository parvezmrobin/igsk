<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/** @property string year */
class Year extends Model
{
    protected $primaryKey = 'year';
    protected $keyType = 'varchar';

    public function classYears()
    {
        return $this->hasMany(ClassYear::class, 'year');
    }

    public function latestYear()
    {
        return $this->latest("year")->first();
    }
}
