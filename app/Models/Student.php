<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property ClassYearSection classYearSection
 * @property ClassYearSection section
 * @property string id
 * @property Collection toddlers
 */
class Student extends Model
{
    const DAY_CARE = "Day Care";
    const PRE_PLAY = "Pre Play";
    const PLAY = "Play";
    const NURSERY = "Nursery";
    const KG = "KG";
    const ONE = "One";
    const TWO = "Two";
    const THREE = "Three";
    const FOUR = "Four";
    const FIVE = "Five";
    const SIX = "Six";

    protected $fillable = [
        "class_year_section_id"
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Alias of classYearSection
     * @param $value
     * @return ClassYearSection
     */
    public function getSectionAttribute($value)
    {
        return $this->classYearSection;
    }

    public function classYearSection()
    {
        return $this->belongsTo(ClassYearSection::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function toddlers()
    {
        return $this->hasMany(Toddler::class);
    }

    /**
     * @param $month
     * @return Morality|null
     */
    public function getMorality($month)
    {
        return $this->moralities()->where("month", $month)->first();
    }

    public function moralities()
    {
        return $this->hasMany(Morality::class);
    }

    public function assessments()
    {
        return $this->hasMany(Assessment::class);
    }

    public function grades()
    {
        return $this->hasMany(Grade::class);
    }

    public function teachers()
    {
        return $this->hasMany(SubjectTeacher::class, 'class_year_section_id', 'class_year_section_id');
    }

    public function homeWork()
    {
        return $this->hasMany(HomeWork::class, 'class_year_section_id', 'class_year_section_id');
    }

    public function assignments()
    {
        return $this->hasMany(Assignment::class, 'class_year_section_id', 'class_year_section_id');
    }

    public function prePlays()
    {
        return $this->hasMany(PrePlay::class, 'class_year_section_id', 'class_year_section_id');
    }
}
