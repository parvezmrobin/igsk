<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quarter extends Model
{
    const FIRST = "First Quarter";
    const SECOND = "Second Quarter";

    public function homeWorks()
    {
        return $this->hasMany(HomeWork::class);
    }

    public function grades()
    {
        return $this->hasMany(Grade::class);
    }

    public function assessments()
    {
        return $this->hasMany(Assessment::class);
    }

    public function assignment()
    {
        return $this->hasMany(Assignment::class);
    }

    public function moralities()
    {
        return $this->hasMany(Morality::class);
    }

    public function classWorks()
    {
        return $this->hasMany(ClassWork::class);
    }
}
