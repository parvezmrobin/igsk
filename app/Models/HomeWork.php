<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property ClassYearSection section
 * @property array home_work
 * @property string id
 * @property string class_year_section_id
 * @property Carbon $date
 */
class HomeWork extends Model
{
    protected $fillable = ['date', 'quarter_id', 'home_work'];

    protected $casts = [
        "home_work" => 'array'
    ];

    protected $dates = [
        'date'
    ];

    public function section()
    {
        return $this->belongsTo(ClassYearSection::class, 'class_year_section_id');
    }

    public function classYearSection()
    {
        return $this->section();
    }
}
