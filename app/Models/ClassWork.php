<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property ClassYearSubject subject
 * @property array class_work
 * @property string id
 */
class ClassWork extends Model
{
    protected $fillable = ['date', 'quarter_id', 'class_work'];

    protected $casts = [
        "class_work" => 'array'
    ];

    public function subject()
    {
        return $this->belongsTo(ClassYearSubject::class, 'subject_id');
    }
}
