<?php
/**
 * User: Parvez
 * Date: 1/6/2018
 * Time: 9:28 PM
 */

namespace App\Repositories;


use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class UserRepository
{
    public static function getClassTeachers()
    {
        $currentYear = \DB::raw("(SELECT max(years.year) FROM years)");
        return (new User)->whereHas(
            'classYearSections.classYear',
            function (Builder $builder) use ($currentYear) {
                $builder->where("year", $currentYear);
            })->with(['classYearSections'])
            ->get();
    }

    public static function getSubjectTeachers()
    {
        return (new User)->join("subject_teachers", "subject_teacher_id", "users.id")
            ->join("class_year_subjects", "class_year_subject_id", "class_year_subjects.id")
            ->join("class_year_sections", "class_year_section_id", "class_year_sections.id")
            ->join("class_years", "class_year_subjects.class_year_id", "class_years.id")
            ->join("subjects", "subject_id", "subjects.id")
            ->where("year", \DB::raw("(SELECT max(years.year) FROM years)"))
            ->select("users.*", "subjects.subject", "class_years.year", "class_years.cls", "section")
            ->get();
    }
}