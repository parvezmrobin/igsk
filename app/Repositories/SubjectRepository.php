<?php
/**
 * User: Parvez
 * Date: 12/4/2017
 * Time: 10:51 PM
 */

namespace App\Repositories;


use App\Models\ClassYear;
use App\Models\User;
use App\Models\Year;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class SubjectRepository
{

    /**
     * List of taught subjects by the user
     * @param User $user
     * @return Year[]|Collection
     */
    public static function taughtSubjects($user)
    {
        return Year::query()->with(
            [
                "classYears" => function ($query) use ($user) {
                    /**
                     * @var Builder $query
                     */
                    $query->whereHas(
                        "classYearSections",
                        function ($query) use ($user) {
                            /**
                             * @var Builder $query
                             */
                            return $query->where('teacher_id', $user->id);
                        });
                },
                "classYears.classYearSubjects.subject"
            ]
        )->get();
    }

    /**
     * List of supervised subjects by the user
     * @param User $user
     * @return Collection|Year[]
     */
    public static function supervisedSubjects($user)
    {
        $classes = $user->supervisedClasses;
        $classYears = (new ClassYear)->whereIn("cls", $classes->pluck("attributes.cls")->all())->get();
        return Year::query()->with(
            [
                "classYears" => function ($query) use ($classYears) {
                    /**
                     * @var Builder $query
                     */
                    $query->whereIn('id', $classYears->pluck("attributes.id"));
                },
                "classYears.classYearSubjects.subject"
            ]
        )->get();
    }

    /**
     * @return Collection|Year[]
     */
    public static function allSubjects()
    {
        return Year::query()->with("classYears.classYearSubjects.subject")->get();
    }

    public static function subjectsOfStudent(User $user)
    {
        return Year::query()->whereHas(
            "classYears.classYearSections.students",
            function ($query) use ($user) {
                return $query->where("user_id", $user->id);
            }
        )->with([
            "classYears" => function ($query) use ($user) {
                return $query->whereHas(
                    "classYearSections.students",
                    function ($query) use ($user) {
                        return $query->where("user_id", $user->id);
                    }
                );
            },
            "classYears.classYearSubjects.subject"
        ])->get();
    }
}
