<?php
/**
 * User: Parvez
 * Date: 12/4/2017
 * Time: 11:20 PM
 */

namespace App\Repositories;


use App\Models\ClassYear;
use App\Models\User;
use App\Models\Year;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class SectionRepository
{

    /**
     * List of taught sections by the user
     * @param User $user
     * @return Collection|Year[]
     */
    public static function taughtSections($user)
    {
        return Year::query()->with(
            [
                "classYears" => function ($query) use ($user) {
                    /**
                     * @var Builder $query
                     */
                    $query->whereHas(
                        "classYearSections",
                        function ($query) use ($user) {
                            /**
                             * @var Builder $query
                             */
                            return $query->where('teacher_id', $user->id);
                        });
                },
                "classYears.classYearSections" => function ($query) use ($user) {
                    /**
                     * @var Builder $query
                     */
                    return $query->where("teacher_id", $user->id);
                }
            ]
        )->get();
    }

    /**
     * List of supervised sections by the user
     * @param User $user
     * @return Collection|Year[]
     */
    public static function supervisedSections($user)
    {
        $classes = $user->supervisedClasses;
        $classYears = (new ClassYear)->whereIn("cls", $classes->pluck("attributes.cls")->all())->get();
        return Year::query()->with(
            [
                "classYears" => function ($query) use ($classYears) {
                    /**
                     * @var Builder $query
                     */
                    $query->whereIn('id', $classYears->pluck("attributes.id"));
                },
                "classYears.classYearSections"
            ]
        )->get();
    }

    /**
     * @return Collection|Year[]
     */
    public static function allSections()
    {
        return Year::query()->with("classYears.classYearSections")->get();
    }

    public static function sectionsOfStudent(User $user){
        return Year::query()->whereHas(
                "classYears.ClassYearSections.students",
                function($query) use ($user){
                    /** @var Builder $query */
                    return $query->where("user_id",$user->id);
                }
            )->with([
                "classYears"=>function($query) use ($user){
                    /** @var Builder $query */
                    return $query->whereHas(
                        "classYearSections.students",
                        function($query) use ($user){
                            /** @var Builder $query */
                            return $query->where("user_id",$user->id);
                        }
                    );
                },
                "classYears.classYearSections"
            ])->get();
    }

    public static function sectionsOfRecentYear()
    {
        return (new Year)->where("year", DB::raw("(SELECT max(year) FROM years)"))
            ->with("classYears.classYearSections")->get();
    }
}