<?php

namespace App\Repositories;

use App\Models\ClassYear;
use App\Models\User;
use App\Models\Year;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * User: Parvez
 * Date: 12/4/2017
 * Time: 8:55 PM
 */
class StudentRepository
{
    /**
     * List of taught students by the user
     * @param User $user
     * @return Year[]|Collection
     */
    public static function taughtStudents($user)
    {
        return Year::query()->with(
            [
                "classYears" => function ($query) use ($user) {
                    /**
                     * @var Builder $query
                     */
                    $query->whereHas(
                        "classYearSections",
                        function ($query) use ($user) {
                            /**
                             * @var Builder $query
                             */
                            return $query->where('teacher_id', $user->id);
                        });
                },
                "classYears.classYearSections" => function ($query) use ($user) {
                    /**
                     * @var Builder $query
                     */
                    return $query->where("teacher_id", $user->id);
                },
                "classYears.classYearSections.students.user"
            ]
        )->get();
    }

    /**
     * List of supervised students by the user
     * @param User $user
     * @return Year[]|Collection
     */
    public static function supervisedStudents($user)
    {
        $classes = $user->supervisedClasses;
        $classYears = (new ClassYear)->whereIn("cls", $classes->pluck("attributes.cls")->all())->get();
        return Year::query()->with(
            [
                "classYears" => function ($query) use ($classYears) {
                    /**
                     * @var Builder $query
                     */
                    $query->whereIn('id', $classYears->pluck("attributes.id"));
                },
                "classYears.classYearSections.students.user"
            ]
        )->get();
    }

    /**
     * @return Collection|Year[]
     */
    public static function allStudents()
    {
        return Year::query()->with("classYears.classYearSections.students.user")->get();
    }
}