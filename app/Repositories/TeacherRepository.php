<?php
/**
 * User: Parvez
 * Date: 12/6/2017
 * Time: 2:20 AM
 */

namespace App\Repositories;


use App\Models\ClassYear;
use App\Models\User;
use App\Models\Year;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class TeacherRepository
{
    /**
     * @param User $user
     * @return Collection|Year[]
     */
    public static function subordinateTeachers(User $user)
    {
        return Year::query()->with(
            [
                "classYears" => function ($query) use ($user) {
                    /**
                     * @var Builder $query
                     */
                    $query->whereHas(
                        "classYearSections",
                        function ($query) use ($user) {
                            /**
                             * @var Builder $query
                             */
                            return $query->where('teacher_id', $user->id);
                        });
                },
                "classYears.classYearSections" => function ($query) use ($user) {
                    /**
                     * @var Builder $query
                     */
                    return $query->where("teacher_id", $user->id);
                },
                //Student.User
                "classYears.classYearSections.students.user",
                //SubjectTeacher.User
                "classYears.classYearSections.subjectTeachers.teacher",
                //SubjectTeacher.ClassYearSubject.Subject
                "classYears.classYearSections.subjectTeachers.classYearSubject.subject",
            ])->get();
    }

    /**
     * @param User $user
     * @return Collection|Year[]
     */
    public static function supervisedTeachers(User $user)
    {
        $classes = $user->supervisedClasses;
        $classYears = (new ClassYear)->whereIn("cls", $classes->pluck("attributes.cls")->all())->get();
        return Year::query()->with(
            [
                "classYears" => function ($query) use ($classYears) {
                    /**
                     * @var Builder $query
                     */
                    $query->whereIn('id', $classYears->pluck("attributes.id"));
                },
                "classYears.classYearSections",
                //Student.User
                "classYears.classYearSections.students.user",
                //SubjectTeacher.User
                "classYears.classYearSections.subjectTeachers.teacher",
                //SubjectTeacher.ClassYearSubject.Subject
                "classYears.classYearSections.subjectTeachers.classYearSubject.subject",
            ]
        )->get();
    }

    /**
     * @return Collection|Year[]
     */
    public static function allTeachers()
    {
        return Year::query()->with([
            //Student.User
            "classYears.classYearSections.students.user",
            //SubjectTeacher.User
            "classYears.classYearSections.subjectTeachers.teacher",
            //SubjectTeacher.ClassYearSubject.Subject
            "classYears.classYearSections.subjectTeachers.classYearSubject.subject",
        ])->get();
    }
}