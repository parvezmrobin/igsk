<?php

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new Post())->insert([
            "title" => "JOB ADVERTISEMENT",
            "post" => '
                <p><b style="color: #2a6496"> Position: Teacher</b><br><br></p><p>International Grammar School, Khulna (IGSK) is looking for young, inspiring, and self-motivated full-
time teachers for its campus. The very first “bag-less” school in Khulna City, besides not letting the
students to feel the load of education, wouldn’t require any house tutors for academic
achievements.</p><br><br><b>Details of the position:</b><br><br><p>The successful candidate will teach the curriculum of the subjects, e.g., Bangla, English, Sociology,
Science, and Mathematics. We welcome applications from both experienced and newly qualified
teachers.</p><br><br><b>Required qualifications:</b><br><br><ul><li>Teaching experiences will get preference for Master’s degree holders, and two years’ teaching
experiences in English medium school are required for four years’ Bachelor degree holders;</li><li>Fluency in English and Bangla languages</li><li>Candidates completed A level will get preference;</li><li>Experience to deal with children in different circumstances will get priority</li><li>Strong communication, presentation, and classroom management skills</li><li>Well versed on Microsoft office packages, basic use of internet, and printing.</li></ul><br><br><p>We will offer a competitive compensation package. If you meet above mentioned requirements,
enjoy working with children, and wish to join an enthusiastic, promising, and high standard team,
please submit applications including CV by June 14, 2018 at jobs@khulnaigs.com.</p><p></p>
           ',
            "type" => Post::NOTICE,
            "user_id" => 3,
            "created_at" => \Carbon\Carbon::create(2018, 6, 1),
            "updated_at" => \Carbon\Carbon::create(2018, 6, 1)
        ]);
        (new Post())->insert([
            "title" => "INTERVIEW SCHEDULE",
            "post" => '
                <p></p><p>Dear Candidate,</p>
<p>Thank you for applying for the teaching position at International Grammar School, Khulna. We would like to invite you for an interview session.</p>
<p><strong>Venue</strong>: Khulna Collegiate School, Sonadanga Residential Area 1st Phase</p>
<p><strong>Date</strong>: July 6, 2018</p>
<p><strong>Time</strong>: 9.00 am</p>
<p>Please call me at 01730585311-12 or email me at <a href="mailto:mailbox@khulnaigs.com" target="_blank" rel="noopener">mailbox@khulnaigs.com</a> for further query. Looking forward to meeting you. Wishing you all the best.</p>
<p><strong>NB:&nbsp;</strong><strong>IGSK reserve all rights to postpone, cancel or reschedule the interview.</strong></p><p></p>
            ',
            "type" => Post::NOTICE,
            "user_id" => 3,
            "created_at" => \Carbon\Carbon::create(2018,7,1),
            "updated_at"=> \Carbon\Carbon::create(2018,7,1)
        ]);

        (new Post())-> insert([
            "title" => "INTERVIEW RESCHEDULED",
            "post" => '<p></p><h2 style="box-sizing: border-box; margin: 0px 0px 10px; color: #5e5e5e; font-family: Roboto, Arial, Helvetica, sans-serif;">Dear Candidate,</h2>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #5e5e5e; font-family: Roboto, Arial, Helvetica, sans-serif;">Thank you for applying for the teaching position at International Grammar School, Khulna. We would like to invite you for an interview session.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #5e5e5e; font-family: Roboto, Arial, Helvetica, sans-serif;"><strong style="box-sizing: border-box;">Venue</strong>: Khulna Collegiate School, Sonadanga Residential Area 1st Phase</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #5e5e5e; font-family: Roboto, Arial, Helvetica, sans-serif;"><strong style="box-sizing: border-box;">Date</strong>: July 13, 2018</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #5e5e5e; font-family: Roboto, Arial, Helvetica, sans-serif;"><strong style="box-sizing: border-box;">Time</strong>: 9.00 am</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #5e5e5e; font-family: Roboto, Arial, Helvetica, sans-serif;">Please call me at 01730585311-12 or email me at&nbsp;<a style="box-sizing: border-box; background: transparent; color: #5e5e5e; outline: none;" href="mailto:mailbox@khulnaigs.com" target="_blank" rel="noopener">mailbox@khulnaigs.com</a>&nbsp;for further query. Looking forward to meeting you. Wishing you all the best.</p><p></p>',
            "type" => Post::NOTICE,
            "user_id" => 3,
            "created_at" => \Carbon\Carbon::create(2018,8,1),
            "updated_at" => Carbon\Carbon::create(2018,8,1)
        ]);
        (new Post)->insert([
            "title" => "OPEN SEMINAR FOR PARENTS",
            "post" => '
                <p></p><p>We are hosting this event for clarification and detailed discussion about our school and its activities with the parents.</p>
<p><strong>Date: </strong>11 August, 2018</p>
<p><strong>Time:</strong> 11.00 am</p><p></p>
            ',
            "type" => Post::EVENT,
            "user_id" => 3,
            "created_at" => \Carbon\Carbon::create(2018,8,1),
            "updated_at" => \Carbon\Carbon::create(2018,8,1)
        ]);




    }
}
