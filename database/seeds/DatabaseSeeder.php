<?php

use App\Models\ClassYearSection;
use App\Models\Cls;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->method1();
        $this->method2();
    }

    private function method2()
    {
        $classYearSections =
            ClassYearSection::query()->whereHas(
                "classYear", function ($query) {
                $query->whereNotIn('cls', [Cls::DAY_CARE, Cls::PRE_PLAY]);
            }
            )->with("students.user")->get();

        foreach ($classYearSections as $section) {
            echo $section->classYear->year, " ", $section->classYear->cls, " ", $section->section, "\n";
            for ($quarter = 1; $quarter < 4; $quarter++) {
                if ($quarter == 1) {
                    $startDate = Carbon\Carbon::create($section->classYear->year, 7, 1);
                    $endDate = Carbon\Carbon::create($section->classYear->year, 10, 1);
                } else {
                    if ($quarter == 2) {
                        $startDate = Carbon\Carbon::create($section->classYear->year, 11, 1);
                        $endDate = Carbon\Carbon::create($section->classYear->year + 1, 2, 1);
                    } else {
                        $startDate = Carbon\Carbon::create($section->classYear->year + 1, 3, 1);
                        $endDate = Carbon\Carbon::create($section->classYear->year + 1, 6, 1);
                    }
                }
                echo "Home works\n";
                for ($date = $startDate; $date->lt($endDate); $date->addDay()) {
                    if (!$date->isFriday()) {
                        factory(App\Models\HomeWork::class)->create(
                            [
                                'date' => $date,
                                'quarter_id' => $quarter,
                                'class_year_section_id' => $section->id,
                            ]
                        );
                    }
                }

                /**
                 * Seed Assignments
                 */
                echo "Assignment\n";
                for ($week = 1; $week < 15; $week++) {
                    factory(App\Models\Assignment::class)->create(
                        [
                            'week' => $week,
                            'quarter_id' => $quarter,
                            'class_year_section_id' => $section->id,
                        ]
                    );
                }
            }
        }
    }

    private function method1()
    {
        /**
         * Seed Roles
         */
        $roles = [
            ['role' => 'Student'],
            ['role' => 'Teacher'],
            ['role' => 'Supervisor'],
            ['role' => 'Admin']
        ];
        DB::table('roles')->insert($roles);

        /**
         * Seed Years
         */
        $years = [
            ['year' => 2019],
        ];
        DB::table('years')->insert($years);

        /**
         * Seed Sections
         */
        $sections = [
            ['section' => 'A'],
            ['section' => 'B'],
        ];
        DB::table('sections')->insert($sections);

        /**
         * Seed Clses
         */
        $supervisors = factory(App\Models\User::class, 3)->create(['role' => 'Supervisor']);
        $classes = [
            [
                'cls' => 'Day Care',
                'supervisor_id' => $supervisors[0]->id
            ],
            [
                'cls' => 'Pre Play',
                'supervisor_id' => $supervisors[0]->id
            ],
            [
                'cls' => 'Play',
                'supervisor_id' => $supervisors[1]->id
            ],
            [
                'cls' => 'Nursery',
                'supervisor_id' => $supervisors[1]->id
            ],
            [
                'cls' => 'KG',
                'supervisor_id' => $supervisors[1]->id
            ],
            [
                'cls' => 'One',
                'supervisor_id' => $supervisors[2]->id
            ],
            [
                'cls' => 'Two',
                'supervisor_id' => $supervisors[2]->id
            ],
            [
                'cls' => 'Three',
                'supervisor_id' => $supervisors[2]->id
            ],
        ];
        DB::table('clses')->insert($classes);

        /**
         * Seed Subjects
         */
        $subjects = [
            ['subject' => 'Bangla'],
            ['subject' => 'English'],
            ['subject' => 'Math'],
            ['subject' => 'Science']
        ];
        $temp = [];
        foreach ($subjects as $subject) {
            $temp[] = App\Models\Subject::create($subject);
        }
        $subjects = $temp;

        /**
         * Seed Quarters
         */
        $quarters = [
            ['quarter' => 'First Quarter'],
            ['quarter' => 'Second Quarter'],
            ['quarter' => 'Final Quarter'],
        ];
        DB::table('quarters')->insert($quarters);

        /**
         * Seed ClassYears
         */
        $classYears = [];

        foreach ($years as $year) {
            foreach ($classes as $cls) {
                $classYears[] = [
                    'cls' => $cls['cls'],
                    'year' => $year['year'],
                ];
            }
        }

        $temp = [];

        foreach ($classYears as $classYear) {
            $temp[] = App\Models\ClassYear::create($classYear);
        }
        $classYears = $temp;

        /**
         * Seed ClassYearSections
         */
        $classYearSections = [];

        foreach ($classYears as $classYear) {
            foreach ($sections as $section) {
                $classYearSections[] = [
                    'class_year_id' => $classYear->id,
                    'section' => $section['section'],
                ];
            }
        }

        $temp = [];
        foreach ($classYearSections as $classYearSection) {
            $cys = factory(App\Models\ClassYearSection::class)
                ->create($classYearSection);

            $students = factory(App\Models\Student::class, rand(2, 3))->create(
                [
                    'class_year_section_id' => $cys->id,
                ]
            );

            /**
             * Seed Toddlers
             */
            echo $cys->classYear->year, " ", $cys->classYear->cls, " ", $cys->section, "\n";
            if (!strcmp($cys->classYear->cls, 'Day Care')) {
                foreach ($students as $student) {
                    for ($i = 49; $i <= 60; $i++) {
                        factory(App\Models\Toddler::class)->create(
                            [
                                'student_id' => $student->id,
                                'month' => $i,
                            ]
                        );
                    }
                }
            } /**
             * Seed Moralities
             */
            elseif (strcasecmp($cys->classYear->cls, 'Pre Play')) {
                for ($quarter = 1; $quarter < 4; $quarter++) {
                    foreach ($students as $student) {
                        for ($month = 1; $month < 4; $month++) {
                            factory(App\Models\Morality::class)->create(
                                [
                                    'month' => $month,
                                    'student_id' => $student->id,
                                    'quarter_id' => $quarter
                                ]
                            );
                        }
                    }
                }
            } /**
             * Seed PrePlays
             */
            else {
                $startDate = Carbon\Carbon::create($cys->classYear->year, 7, 1);
                $endDate = Carbon\Carbon::create($cys->classYear->year + 1, 6, 1);

                for ($date = $startDate; $date->lte($endDate); $date->addDay()) {
                    factory(App\Models\PrePlay::class)->create(
                        [
                            'class_year_section_id' => $cys->id,
                            'created_at' => $date,
                        ]
                    );
                }
            }

            $temp[] = $cys;
        }
        $classYearSections = $temp;

        /**
         * Seed ClassYearSubjects
         */
        $classYearSubjects = [];

        foreach (array_slice($classYears, 2) as $classYear) {
            foreach ($subjects as $subject) {
                $classYearSubjects[] = [
                    'class_year_id' => $classYear->id,
                    'subject_id' => $subject->id,
                ];
            }
        }
        $temp = [];
        foreach ($classYearSubjects as $classYearSubject) {
            $temp[] = App\Models\ClassYearSubject::create($classYearSubject);
        }
        $classYearSubjects = $temp;

        /**
         * Seed SubjectTeachers
         */
        $subjectTeachers = [];
        foreach (array_slice($classYearSections, 4) as $section) {
            foreach ($section->subjects as $subject) {
                $subjectTeachers[] = [
                    'class_year_section_id' => $section->id,
                    'class_year_subject_id' => $subject->id,
                ];
            }
        }

        $temp = [];
        foreach ($subjectTeachers as $subjectTeacher) {
            $temp[] = factory(App\Models\SubjectTeacher::class)->create($subjectTeacher);
        }
        $subjectTeachers = $temp;

        /**
         * Seed ClassWorks
         */
        echo "Class work & Assessment\n";
        foreach ($classYearSubjects as $subject) {
            $students = $subject->classYear->students;
            echo $subject->classYear->year, " ", $subject->classYear->cls, " ", $subject->subject->subject, "\n";

            for ($quarter = 1; $quarter < 4; $quarter++) {
                if ($quarter == 1) {
                    $startDate = Carbon\Carbon::create($subject->classYear->year, 7, 1);
                    $endDate = Carbon\Carbon::create($subject->classYear->year, 10, 1);
                } else {
                    if ($quarter == 2) {
                        $startDate = Carbon\Carbon::create($subject->classYear->year, 11, 1);
                        $endDate = Carbon\Carbon::create($subject->classYear->year + 1, 2, 1);
                    } else {
                        $startDate = Carbon\Carbon::create($subject->classYear->year + 1, 3, 1);
                        $endDate = Carbon\Carbon::create($subject->classYear->year + 1, 6, 1);
                    }
                }

                for ($date = $startDate; $date->lt($endDate); $date->addDay()) {
                    if (!$date->isFriday()) {
                        factory(App\Models\ClassWork::class)->create(
                            [
                                'date' => $date,
                                'quarter_id' => $quarter,
                                'subject_id' => $subject->id,
                            ]
                        );

                        foreach ($students as $student) {
                            factory(App\Models\Assessment::class)->create(
                                [
                                    'subject_id' => $subject->id,
                                    'student_id' => $student->id,
                                    'quarter_id' => $quarter,
                                    'date' => $date,
                                ]
                            );
                        }
                    }
                }
            }
        }
    }
}

