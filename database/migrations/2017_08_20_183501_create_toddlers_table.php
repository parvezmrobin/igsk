<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToddlersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toddlers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('student_id');
            $table->unsignedTinyInteger('month');
            $table->double('height');
            $table->double('weight');
            $table->double('cognitive');
            $table->double('motor_skill');
            $table->timestamps();

            $table->unique(['student_id', 'month']);

            $table->foreign('student_id')
                ->references('id')->on('students')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toddlers');
    }
}
