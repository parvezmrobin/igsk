<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_works', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->unsignedTinyInteger('quarter_id');
            $table->text('class_work');
            $table->unsignedInteger('subject_id');
            $table->timestamps();

            $table->unique(['date', 'subject_id']);

            $table->foreign('subject_id')
                ->references('id')->on('class_year_subjects')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('quarter_id')
                ->references('id')->on('quarters')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_works');
    }
}
