<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('week');
            $table->unsignedTinyInteger('quarter_id');
            $table->text('assignment');
            $table->unsignedInteger('class_year_section_id');
            $table->timestamps();

            $table->unique(['week', 'quarter_id', 'class_year_section_id']);

            $table->foreign('class_year_section_id')
                ->references('id')->on('class_year_sections')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('quarter_id')
                ->references('id')->on('quarters')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
