<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrePlaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_plays', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->unsignedInteger('class_year_section_id');
            $table->text('homework');
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->nullable();

            $table->primary(['class_year_section_id', 'created_at']);

            $table->foreign('class_year_section_id')
                ->references('id')->on('class_year_sections')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_plays');
    }
}
