<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassYearSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_year_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('class_year_id');
            $table->unsignedInteger('teacher_id');
            $table->string('section', 31);
            $table->timestamps();

            $table->unique(['class_year_id', 'section']);

            $table->foreign('class_year_id')
                ->references('id')->on('class_years')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('teacher_id')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('section')
                ->references('section')->on('sections')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_year_sections');
    }
}
