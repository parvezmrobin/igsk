<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_years', function (Blueprint $table) {
            $table->increments('id');
            $table->string('year', 7);
            $table->string('cls', 31);
            $table->timestamps();

            $table->unique(['year', 'cls']);

            $table->foreign('year')
                ->references('year')->on('years')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('cls')
                ->references('cls')->on('clses')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_years');
    }
}
