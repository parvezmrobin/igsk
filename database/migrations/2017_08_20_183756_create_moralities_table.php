<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoralitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moralities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('month');
            $table->string('morality');
            $table->unsignedInteger('student_id');
            $table->unsignedTinyInteger('quarter_id');
            $table->timestamps();

            $table->unique(['month', 'student_id', 'quarter_id']);

            $table->foreign('student_id')
                ->references('id')->on('students')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('quarter_id')
                ->references('id')->on('quarters')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moralities');
    }
}
