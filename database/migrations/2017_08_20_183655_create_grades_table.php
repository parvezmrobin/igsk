<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('subject_id');
            $table->unsignedInteger('student_id');
            $table->unsignedTinyInteger('quarter_id');
            $table->string('grade', 5);
            $table->timestamps();

            $table->unique(['subject_id', 'student_id', 'quarter_id']);

            $table->foreign('subject_id')
                ->references('id')->on('class_year_subjects')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('student_id')
                ->references('id')->on('students')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('quarter_id')
                ->references('id')->on('quarters')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grades');
    }
}
