<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassYearSectionSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_year_subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('class_year_id');
            $table->unsignedInteger('subject_id');
            $table->timestamps();

            $table->unique(['class_year_id', 'subject_id']);

            $table->foreign('subject_id')
                ->references('id')->on('subjects')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('class_year_id')
                ->references('id')->on('class_years')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_year_subjects');
    }
}
