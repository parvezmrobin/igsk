<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->unsignedInteger('subject_id');
            $table->unsignedInteger('student_id');
            $table->unsignedTinyInteger('quarter_id');
            $table->string('comment');
            $table->unsignedTinyInteger('rating');
            $table->timestamps();

            $table->unique(['date', 'subject_id', 'student_id']);

            $table->foreign('subject_id')
                ->references('id')->on('class_year_subjects')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('student_id')
                ->references('id')->on('students')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('quarter_id')
                ->references('id')->on('quarters')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessments');
    }
}
