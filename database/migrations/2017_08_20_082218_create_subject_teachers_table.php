<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('class_year_subject_id');
            $table->unsignedInteger('class_year_section_id');
            $table->unsignedInteger('subject_teacher_id');
            $table->timestamps();

            $table->unique(['class_year_subject_id', 'class_year_section_id', 'subject_teacher_id'], 'subject_teacher_unique');

            $table->foreign('class_year_subject_id')
                ->references('id')->on('class_year_subjects')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('subject_teacher_id')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('class_year_section_id')
                ->references('id')->on('class_year_sections')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subject_teachers');
    }
}
