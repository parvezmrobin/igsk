<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_works', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->unsignedTinyInteger('quarter_id');
            $table->text('home_work');
            $table->unsignedInteger('class_year_section_id');
            $table->timestamps();

            $table->unique(['date', 'class_year_section_id']);

            $table->foreign('class_year_section_id')
                ->references('id')->on('class_year_sections')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('quarter_id')
                ->references('id')->on('quarters')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_works');
    }
}
