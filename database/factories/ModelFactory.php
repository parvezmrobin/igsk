<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'role' => 'role'
    ];
});

$factory->define(App\Models\Student::class, function () {
    return [
        'user_id' => function() {
            return factory(App\Models\User::class)->create(['role' => 'Student'])->id;
        },
        'class_year_section_id' => 0
    ];
});


$factory->define(App\Models\ClassYearSection::class, function () {
    return [
        'class_year_id' => 0,
        'teacher_id' => function() {
            return factory(App\Models\User::class)->create(['role' => 'Teacher'])->id;
        }
    ];
});


$factory->define(App\Models\SubjectTeacher::class, function () {
    return [
        'class_year_subject_id' => 0,
        'class_year_section_id' => 0,
        'subject_teacher_id' => function() {
            return factory(App\Models\User::class)->create(['role' => 'Teacher'])->id;
        }
    ];
});


$factory->define(App\Models\Assessment::class, function (Faker\Generator $faker) {
    return [
        'subject_id' => 0,
        'student_id' => 0,
        'quarter_id' => 0,
        'comment' => $faker->realText(),
        'rating' => rand(1, 5)
    ];
});

$factory->define(App\Models\Toddler::class, function (Faker\Generator $faker) {
    return [
        'student_id' => 0,
        'month' => 0,
        'height' => $faker->randomFloat(null, 70, 100),
        'weight' => $faker->randomFloat(null, 20, 25),
        'cognitive' => rand(1, 5),
        'motor_skill' => rand(1, 5)
    ];
});

$factory->define(App\Models\Morality::class, function () {
    return [
        'month' => 0,
        'morality' => [
            "attentive" => rand(1, 5),
            "polite" => rand(1, 5),
            "friendly" => rand(1, 5),
            "helpful" => rand(1, 5),
        ],
        'student_id' => 0,
        'quarter_id' => 0
    ];
});

$factory->define(App\Models\ClassWork::class, function (Faker\Generator $faker) {
    return [
        'date' => 'date',
        'quarter_id' => 0,
        'class_work' => $faker->text(1000),
        'subject_id' => 0
    ];
});

$factory->define(App\Models\HomeWork::class, function (Faker\Generator $faker) {
    return [
        'date' => 'date',
        'quarter_id' => 0,
        'home_work' => $faker->text(1000),
        'class_year_section_id' => 0
    ];
});

$factory->define(App\Models\Assignment::class, function (Faker\Generator $faker) {
    return [
        'week' => 0,
        'quarter_id' => 0,
        'assignment' => $faker->text(1000),
        'class_year_section_id' => 0
    ];
});

$factory->define(App\Models\PrePlay::class, function (Faker\Generator $faker) {
    return [
        'class_year_section_id' => 0,
        'homework' => $faker->text(1000)
    ];
});

$factory->define(App\Models\Grade::class, function () {
    $grades = ['B', 'B+', 'A-', 'A', 'A+'];

    return [
        'subject_id' => 0,
        'student_id' => 0,
        'quarter_id' => 0,
        'grade' => $grades[rand(0, 4)],
    ];
});
